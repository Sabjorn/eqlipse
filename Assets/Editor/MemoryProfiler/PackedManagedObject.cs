using System;

namespace MemoryProfilerWindow
{
    [Serializable]
    public class PackedManagedObject
    {
        public ulong address;
        public int typeIndex;
        public int size;
    }
}
