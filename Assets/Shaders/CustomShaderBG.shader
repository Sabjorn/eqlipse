﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/Background" {
	Properties{
		_ColorInner("Inner Color", Color) = (1,1,1,1)
		_ColorOuter("Outer Color", Color) = (1,1,1,1)
	}
		SubShader{
			Pass{
			CGPROGRAM

	#pragma vertex vert
	#pragma fragment frag
	#pragma target 3.0

	#include "UnityCG.cginc"

			uniform fixed4 _ColorInner;
	uniform fixed4 _ColorOuter;

		float4 vert(appdata_base v) : POSITION{
		return UnityObjectToClipPos(v.vertex);
	}

		fixed4 frag(float4 sp:VPOS) : SV_Target{
		fixed2 _sp = fixed2(sp.x / _ScreenParams.x,sp.y / _ScreenParams.y);
		fixed d = sqrt(pow(0.5 - _sp.x,2.0) + pow(0.5 - _sp.y,2.0));
		return lerp(_ColorInner,_ColorOuter,d * 2);
	}

		ENDCG
	}
	}
}