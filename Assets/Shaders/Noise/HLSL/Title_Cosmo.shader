﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "EQLIPSE/Cosmo"
{
	Properties
	{
		_Color1("Color1", Color) = (1,1,1,1)
		_Color2("Color2", Color) = (0,0,0,0)
		_WaveScale("Wave scale", float) = 1 // sliders
		_Detail("Detail", float) = 2
		_Decay("Decay", float) = 2
		_Turbulence("Time turbulence",float) = 1
		_Exponent("Exponent", int) = 4
		_Speed("Speed", float) = 1
	}

		CGINCLUDE

#include "UnityCG.cginc"

#include "SimplexNoise3D.cginc"

		v2f_img vert(appdata_base v)
	{
		v2f_img o;
		o.pos = UnityObjectToClipPos(v.vertex);
		o.uv = v.texcoord.xy;
		return o;
	}

	uniform float4 _Color1, _Color2;
	uniform float _WaveScale, _Detail, _Decay, _Turbulence;
	uniform int _Exponent;
	uniform float _Speed;

	float4 frag(v2f_img i) : SV_Target
	{
		float4 temp;
	float detail = 1.0f;
	float decay = 1.0f;
	for (int _i = 0; _i < _Exponent; _i++) {
		detail *= _Detail;
		temp += snoise(float3(i.uv*detail*_WaveScale, _i + _Time.y*(_i*_Turbulence + 1)*_Speed))*decay;
		decay /= _Decay;
	}
		return lerp(_Color1, _Color2, (temp + 1) / 2);
	}

		ENDCG
		SubShader
	{
		Pass
		{
			CGPROGRAM
			#pragma target 3.0
			#pragma vertex vert
			#pragma fragment frag
			ENDCG
		}
	}
}
