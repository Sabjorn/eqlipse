﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

namespace EQLIPSE.SuperScene
{
	[RequireComponent(typeof(AudioSource))]
	[RequireComponent(typeof(AudioLowPassFilter))]
	public class BGMManager : SingletonMonoBehaviour<BGMManager>
	{

		private AudioSource _audioSource;
		private AudioLowPassFilter _lowPassFilter;

		void Awake()
		{
			_audioSource = GetComponent<AudioSource>();
			_audioSource.loop = true;

			_lowPassFilter = GetComponent<AudioLowPassFilter>();
			_lowPassFilter.cutoffFrequency = 44100;
			_lowPassFilter.lowpassResonanceQ = 1;
		}

		public void Play(AudioClip clip)
		{
			if (!_audioSource.isPlaying || _audioSource.clip != clip)
			{
				_audioSource.volume = 1;
				_audioSource.clip = clip;
				_audioSource.Play();
			}
		}

		public void Stop(float duration = 0)
		{
			if (duration <= 0)
			{
				_audioSource.Stop();
			}
			else
			{
				Observable.EveryUpdate().AsUnitObservable()
					.Do(_ =>
					{
						_audioSource.volume -= Time.deltaTime / duration;
					})
					.TakeUntil(Observable.Timer(TimeSpan.FromSeconds(duration)))
					.Concat(Observable.ReturnUnit()
						.Do(_ =>
						{
							_audioSource.Stop();
						}))
					.Subscribe();
			}
		}

		public void LowPass(float freq, float Q, float duration)
		{
			if (duration <= 0)
			{
				_lowPassFilter.cutoffFrequency = freq;
				_lowPassFilter.lowpassResonanceQ = Q;
			}
			else
			{
				var dFreq = (freq - _lowPassFilter.cutoffFrequency);
				var dQ = (Q - _lowPassFilter.lowpassResonanceQ);
				this.UpdateAsObservable().AsUnitObservable()
					.Do(_ =>
					{
						_lowPassFilter.cutoffFrequency += dFreq * Time.deltaTime / duration;
						_lowPassFilter.lowpassResonanceQ += dQ * Time.deltaTime / duration;
					})
					.TakeUntil(Observable.Timer(TimeSpan.FromSeconds(duration)))
					.Concat(Observable.Defer(() =>
					{
						_lowPassFilter.cutoffFrequency = freq;
						_lowPassFilter.lowpassResonanceQ = Q;

						return Observable.ReturnUnit();
					}))
					.Subscribe();
			}
		}
	}
}