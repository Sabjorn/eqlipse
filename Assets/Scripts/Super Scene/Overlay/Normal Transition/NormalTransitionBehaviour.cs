﻿using System;
using UnityEngine;
using System.Collections;
using UniRx;

namespace EQLIPSE.SuperScene.Transition
{
    [RequireComponent(typeof(Animator))]
    public class NormalTransitionBehaviour : MonoBehaviour, ITransitioin
    {
	    private Animator _animator;

	    [SerializeField] private AnimationClip fadeInClip, fadeOutClip;
	    [SerializeField] private string fadeParameterName;

	    public IObservable<Unit> FadeInObservable
	    {
		    get
		    {
			    return Observable.ReturnUnit()
				    .Do(_ => { _animator.SetBool(fadeParameterName, true); })
				    .Delay(TimeSpan.FromSeconds(fadeInClip.length));
		    }
	    }
		public IObservable<Unit> FadeOutObservable
		{
			get
			{
				return Observable.ReturnUnit()
					.Do(_ => { _animator.SetBool(fadeParameterName, false); })
					.Delay(TimeSpan.FromSeconds(fadeOutClip.length));
			}
		}

		bool _finishFlag;

        void Start()
        {
            _animator = GetComponent<Animator>();
        }

        public IEnumerator FadeInCoroutine()
        {
            _animator.SetBool(fadeParameterName, true);

            while (_finishFlag == false) yield return null;
            _finishFlag = false;
        }
        public IEnumerator FadeOutCoroutine()
        {
            _animator.SetBool(fadeParameterName, false);

            while (_finishFlag == false) yield return null;
            _finishFlag = false;
        }

        public void FinishAnimation() { _finishFlag = true; }
    }
}