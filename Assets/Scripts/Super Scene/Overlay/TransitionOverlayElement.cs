﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using UniRx;

namespace EQLIPSE.SuperScene.Transition
{
    public interface ITransitioin
    {
        IEnumerator FadeInCoroutine();
        IEnumerator FadeOutCoroutine();

		IObservable<Unit> FadeInObservable { get; }
		IObservable<Unit> FadeOutObservable { get; }
	}
}