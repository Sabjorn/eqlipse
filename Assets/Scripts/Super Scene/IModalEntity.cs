﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

namespace EQLIPSE.SuperScene
{
	public interface IModalEntity<T>
	{
		CanvasGroup CanvasGroup { get; }
		IObservable<T> CompletionObservable { get; }
	}
}
