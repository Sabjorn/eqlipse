﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using EQLIPSE;
using EQLIPSE.Utility;

namespace EQLIPSE.SuperScene
{
	public class SceneDealer : SingletonMonoBehaviour<SceneDealer>
	{
		List<string> _loadedScenes;

		void Start()
		{
			_loadedScenes = new List<string>();
		}

		public IEnumerator LoadScene(string scene)
		{
			yield return UnloadAllScenes();
			yield return AddScene(scene);
		}

		public IEnumerator AddScene(string scene)
		{
			yield return SceneManager.LoadSceneAsync(scene, LoadSceneMode.Additive);
			_loadedScenes.Add(scene);
		}

		public IEnumerator UnloadAllScenes()
		{
			Pool.ReleasePools();

			foreach (var scene in _loadedScenes)
			{
				yield return SceneManager.UnloadSceneAsync(scene);
				Resources.UnloadUnusedAssets();
			}
			_loadedScenes.Clear();

			Caching.ClearCache();

			System.GC.Collect();
		}
	}
}