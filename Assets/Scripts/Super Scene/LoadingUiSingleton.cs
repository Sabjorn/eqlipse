﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace EQLIPSE.SuperScene
{
	public class LoadingUiSingleton : SingletonMonoBehaviour<LoadingUiSingleton>
	{
		public CanvasGroup CanvasGroup;
		public Text LoadingText;

		public void ShowLoading(bool show = true, string message = null)
		{
			CanvasGroup.DOFade(show ? 1 : 0, 0.2f);
			if (show)
			{
				LoadingText.text = message ?? "";
			}
		}
	}
}