﻿using UnityEngine;
using System.Collections;
using System;

namespace EQLIPSE.SuperScene
{
	[RequireComponent(typeof(AudioSource))]
	public class SEPlayer : SingletonMonoBehaviour<SEPlayer>
	{

		[SerializeField]
		AudioClip[] Clips;

		AudioSource audioSource;

		void Awake() { audioSource = GetComponent<AudioSource>(); }

		public void Play(SEs SE)
		{
			audioSource.PlayOneShot(Clips[(int)SE]);
		}
	}
}

namespace EQLIPSE
{
	[Serializable]
	public enum SEs
	{
		MiniButton, SongDecision, SongOpen, Decide, Back, Unlock
	}
}