﻿using UnityEngine;
using System.Collections;
using System.Linq;
using EQLIPSE.SuperScene.Transition;

namespace EQLIPSE.SuperScene
{
    public class TransitionOverlayBridge : SingletonMonoBehaviour<TransitionOverlayBridge>
    {
        public Component[] TransitionComponents;
        ITransitioin[] TransitionElements;
        public SuperSceneManager.Transitions Transition;

        void Start()
        {
            TransitionElements = TransitionComponents.Select(c => c != null ? c.GetComponent<ITransitioin>() : null).ToArray();
        }

        public IEnumerator FadeInCouroutine()
        {
            if ((int)Transition < TransitionElements.Length) yield return TransitionElements[(int)Transition].FadeInCoroutine();
            else yield return TransitionElements[0].FadeInCoroutine();
        }

        public IEnumerator FadeOutCouroutine()
        {
            if ((int)Transition < TransitionElements.Length) yield return TransitionElements[(int)Transition].FadeOutCoroutine();
            else yield return TransitionElements[0].FadeOutCoroutine();
        }
    }
}