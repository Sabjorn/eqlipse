﻿using UnityEngine;
using UnityEngine.UI;

namespace EQLIPSE.SuperScene
{
	public class ModalUISingleton : SingletonMonoBehaviour<ModalUISingleton>
	{
		public GraphicRaycaster Raycaster;

		[Header("Song Dialog")]
		public CanvasGroup SongCanvasGroup;
		public Text SongLabel, SongTitle, SongGenre, SongArtist;
		public RawImage SongCover;
		public Button SongOkButton;

		[Header("ローディングスクリーン")] public GameObject LoadingScreenPrefab;
		[Header("汎用メッセージダイアログ")] public GameObject InfoDialogPrefab;
		[Header("はい・いいえ選択ダイアログ")] public GameObject YesNoDialogPrefab;
		[Header("入力ダイアログ")] public GameObject InputDialogPrefab;
		[Header("スクリーンショットツイートダイアログ")] public GameObject ShareDialogPrefab;
		[Header("楽曲表示ダイアログ")] public GameObject SongDialogPrefab;
	}
}