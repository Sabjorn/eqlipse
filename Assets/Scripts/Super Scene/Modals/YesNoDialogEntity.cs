﻿using System.Collections;
using System.Collections.Generic;
using EQLIPSE.SuperScene;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace EQLIPSE.SuperScene
{
	/// <summary>
	/// はい・いいえを選択するIModalEntity。
	/// はいでtrue、いいえでfalseを発行する。
	/// </summary>
	[RequireComponent(typeof(CanvasGroup))]
	public class YesNoDialogEntity : MonoBehaviour, IModalEntity<bool>
	{
		[SerializeField] private Text messageText;
		[SerializeField] private Button yesButton, noButton;

		public CanvasGroup CanvasGroup { get { return GetComponent<CanvasGroup>(); } }

		private Subject<bool> _completionSubject;
		public IObservable<bool> CompletionObservable
		{
			get { return _completionSubject; }
		}

		public string Message { set { messageText.text = value; } }

		void Awake()
		{
			_completionSubject = new Subject<bool>();

			Message = "Yes/No メッセージ";
		}

		void Start()
		{
			yesButton.OnClickAsObservable()
				.Subscribe(_ =>
				{
					SEPlayer.Singleton.Play(SEs.Decide);
					_completionSubject.OnNext(true);
				});
			noButton.OnClickAsObservable()
				.Subscribe(_ =>
				{
					SEPlayer.Singleton.Play(SEs.Back);
					_completionSubject.OnNext(false);
				});
		}
	}
}
