﻿using System.Collections;
using System.Collections.Generic;
using EQLIPSE.SuperScene;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace EQLIPSE.SuperScene
{
	/// <summary>
	/// 楽曲表示ダイアログのIModalEntity。
	/// OKが押されるとUnitを発行する。
	/// </summary>
	[RequireComponent(typeof(CanvasGroup))]
	public class SongDialogEntity : MonoBehaviour, IModalEntity<Unit>
	{
		[SerializeField] private Text messageText;
		[SerializeField] private RawImage coverRawImage;
		[SerializeField] private Text titleText, artistText, genreText;
		[SerializeField] private Button okButton;

		public CanvasGroup CanvasGroup { get { return GetComponent<CanvasGroup>(); } }

		private Subject<Unit> _completionSubject;

		public IObservable<Unit> CompletionObservable
		{
			get { return _completionSubject; }
		}

		public string Message { set { messageText.text = value; } }

		private Song _song;
		public Song Song
		{
			get { return _song; }
			set
			{
				_song = value;

				coverRawImage.texture = value.Cover;
				titleText.text = value.Title;
				artistText.text = value.Artist;
				genreText.text = value.Genre;
			}
		}

		void Awake()
		{
			_completionSubject = new Subject<Unit>();

			Song = new Song();
			Message = "";
		}

		void Start()
		{
			SEPlayer.Singleton.Play(SEs.Unlock);

			okButton.OnClickAsObservable()
				.Subscribe(_ =>
				{
					SEPlayer.Singleton.Play(SEs.Decide);
					_completionSubject.OnNext(Unit.Default);
				});
		}
	}
}
