﻿using System.Collections;
using System.Collections.Generic;
using EQLIPSE.SuperScene;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace EQLIPSE.SuperScene
{
	/// <summary>
	/// 文字列を入力するIModalEntity。
	/// 入力された文字列を発行する。
	/// </summary>
	[RequireComponent(typeof(CanvasGroup))]
	public class　InputDialogEntity : MonoBehaviour, IModalEntity<string>
	{
		[SerializeField] private Text messageText;
		[SerializeField] private InputField inputField;
		[SerializeField] private Button okButton;

		public CanvasGroup CanvasGroup { get { return GetComponent<CanvasGroup>(); } }

		private Subject<string> _completionSubject;

		public IObservable<string> CompletionObservable
		{
			get { return _completionSubject; }
		}

		public string Message { set { messageText.text = value; } }

		public int MaxChar { set { inputField.characterLimit = value; } }

		private InputField.ContentType _contentType;
		public InputField.ContentType ContentType
		{
			get { return _contentType; }
			set
			{
				_contentType = value;

				inputField.contentType = value;
			}
		}

		void Awake()
		{
			_completionSubject = new Subject<string>();

			Message = "入力メッセージ";
			inputField.text = "";
		}

		void Start()
		{
			okButton.OnClickAsObservable()
				.Subscribe(_ =>
				{
					SEPlayer.Singleton.Play(SEs.Decide);
					_completionSubject.OnNext(inputField.text);
				});
		}
	}
}
