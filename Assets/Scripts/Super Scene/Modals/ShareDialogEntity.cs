﻿using System.Collections;
using System.Collections.Generic;
using EQLIPSE.SuperScene;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace EQLIPSE.SuperScene
{
	/// <summary>
	/// スクリーンショットのツイート画面を表示するIModalEntity。
	/// 入力された文字列を発行する。キャンセル時はnullを発行する。
	/// </summary>
	[RequireComponent(typeof(CanvasGroup))]
	public class ShareDialogEntity : MonoBehaviour, IModalEntity<string>
	{
		[SerializeField] private Text headerText,footerText;
		[SerializeField] private InputField customMessageField;
		[SerializeField] private Image screenShotImage;
		[SerializeField] private Button okButton, cancelButton;

		public CanvasGroup CanvasGroup { get { return GetComponent<CanvasGroup>(); } }

		private Subject<string> _completionSubject;
		public IObservable<string> CompletionObservable
		{
			get { return _completionSubject; }
		}

		public string Header
		{
			get { return headerText.text; }
			set { headerText.text = value; }
		}
		public string Footer
		{
			get { return footerText.text; }
			set { footerText.text = value; }
		}
		public Sprite ScreenShot
		{
			get { return screenShotImage.sprite; }
			set { screenShotImage.sprite = value; }
		}

		void Awake()
		{
			_completionSubject = new Subject<string>();

			Header = "";
			Footer = "";
			customMessageField.text = "";
		}

		void Start()
		{
			okButton.OnClickAsObservable()
				.Subscribe(_ =>
				{
					SEPlayer.Singleton.Play(SEs.Decide);
					_completionSubject.OnNext(customMessageField.text);
				});
			cancelButton.OnClickAsObservable()
				.Subscribe(_ =>
				{
					SEPlayer.Singleton.Play(SEs.Back);
					_completionSubject.OnNext(null);
				});
		}
	}
}
