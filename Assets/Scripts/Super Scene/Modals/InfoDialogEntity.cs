﻿using System.Collections;
using System.Collections.Generic;
using EQLIPSE.SuperScene;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace EQLIPSE.SuperScene
{
	/// <summary>
	/// 汎用メッセージダイアログのIModalEntity。
	/// OKが押されるとUnitを発行する。
	/// </summary>
	[RequireComponent(typeof(CanvasGroup))]
	public class InfoDialogEntity : MonoBehaviour, IModalEntity<Unit>
	{
		[SerializeField] private Text messageText;
		[SerializeField] private Button okButton;

		public CanvasGroup CanvasGroup { get { return GetComponent<CanvasGroup>(); } }

		private Subject<Unit> _completionSubject;
		public IObservable<Unit> CompletionObservable
		{
			get { return _completionSubject; }
		}

		public string Message { set { messageText.text = value; } }

		void Awake()
		{
			_completionSubject = new Subject<Unit>();

			Message = "汎用メッセージ";
		}

		void Start()
		{
			okButton.OnClickAsObservable()
				.Subscribe(_ =>
				{
					SEPlayer.Singleton.Play(SEs.Decide);
					_completionSubject.OnNext(Unit.Default);
				});
		}
	}
}
