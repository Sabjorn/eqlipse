﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using EQLIPSE.SuperScene;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace EQLIPSE.Utility
{
	public static class InputGuard
	{
		public enum ResourceTypes { ModalDialog, Await }

		private static readonly Stack<ResourceTypes> Stack = new Stack<ResourceTypes>();

		private static readonly Dictionary<GraphicRaycaster, bool> initialRaycasterDictionary = new Dictionary<GraphicRaycaster, bool>();
		private static readonly Dictionary<CanvasGroup, bool> initialCanvasGroupDictionary = new Dictionary<CanvasGroup, bool>();
		private static readonly Stack<CanvasGroup> exceptionStack = new Stack<CanvasGroup>();

		public static IDisposable LockResource(ResourceTypes resourceType, string message = null)
		{
			Push(resourceType);
			if (resourceType == ResourceTypes.Await) LoadingUiSingleton.Singleton.ShowLoading(true, message);

			if (Stack.Count > 1) Debug.LogWarning("ロックが重複しました: " + Stack.Count);

			return Disposable.Create(() =>
			{
				Pop();
				if (resourceType == ResourceTypes.Await) LoadingUiSingleton.Singleton.ShowLoading(false);
			});
		}

		private static void Push(ResourceTypes resourceType)
		{
			var raycasters = UnityEngine.Object.FindObjectsOfType<GraphicRaycaster>().Where(rc => rc != ModalUISingleton.Singleton.Raycaster);
			foreach (var raycaster in raycasters)
			{
				raycaster.enabled = false;
			}

			ModalUISingleton.Singleton.Raycaster.enabled = resourceType == ResourceTypes.ModalDialog;

			Stack.Push(resourceType);
		}

		private static void Pop()
		{
			Stack.Pop();

			var raycasters = UnityEngine.Object.FindObjectsOfType<GraphicRaycaster>().Where(rc => rc != ModalUISingleton.Singleton.Raycaster);

			if (Stack.Count == 0)
			{
				foreach (var raycaster in raycasters)
				{
					raycaster.enabled = true;
				}
				ModalUISingleton.Singleton.Raycaster.enabled = false;
				return;
			}

			foreach (var raycaster in raycasters)
			{
				raycaster.enabled = false;
			}

			ModalUISingleton.Singleton.Raycaster.enabled = Stack.Peek() == ResourceTypes.ModalDialog;
		}

		public static void LogStack()
		{
			if (Stack.Count == 0)
			{
				Debug.Log("Stack|Empty");
			}
			else
			{
				var str = Stack.ToArray().Reverse().Select(b => b == ResourceTypes.ModalDialog ? "D" : "A").Aggregate((acc, e) => acc + e);
				Debug.Log("Stack|" + str);
			}
		}

		public static IDisposable Lock(CanvasGroup exceptionCanvasGroup = null)
		{
			if (exceptionStack.Count == 0)
			{
				var initialRaycasters = UnityEngine.Object.FindObjectsOfType<GraphicRaycaster>()
					.Where(raycaster => raycaster != ModalUISingleton.Singleton.GetComponent<GraphicRaycaster>());
				var initialCanvasGroups = UnityEngine.Object.FindObjectsOfType<CanvasGroup>()
					.Where(canvasGroup => canvasGroup != ModalUISingleton.Singleton.GetComponent<CanvasGroup>());

				initialRaycasterDictionary.Clear();
				foreach (var raycaster in initialRaycasters)
				{
					initialRaycasterDictionary.Add(raycaster, raycaster.enabled);
					raycaster.enabled = false;
				}
				initialCanvasGroupDictionary.Clear();
				foreach (var canvasGroup in initialCanvasGroups)
				{
					initialCanvasGroupDictionary.Add(canvasGroup, canvasGroup.blocksRaycasts);
					canvasGroup.blocksRaycasts = false;
				}
			}

			PushException(exceptionCanvasGroup);

			return Disposable.Create(PopException);
		}

		private static void PushException(CanvasGroup canvasGroup = null)
		{
			if (exceptionStack.Count > 0 && exceptionStack.Peek() != null)
				exceptionStack.Peek().blocksRaycasts = false;

			if (canvasGroup != null) canvasGroup.blocksRaycasts = true;

			exceptionStack.Push(canvasGroup);
		}

		private static void PopException()
		{
			var canvasGroup = exceptionStack.Pop();

			if (canvasGroup != null) canvasGroup.blocksRaycasts = false;

			if (exceptionStack.Count > 0 && exceptionStack.Peek() != null)
				exceptionStack.Peek().blocksRaycasts = true;

			if (exceptionStack.Count == 0)
			{
				foreach (var entity in initialRaycasterDictionary)
				{
					if (entity.Key != null) entity.Key.enabled = entity.Value;
				}
				foreach (var entity in initialCanvasGroupDictionary)
				{
					if (entity.Key != null) entity.Key.blocksRaycasts = entity.Value;
				}
			}
		}
	}
}