﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Newtonsoft.Json.Linq;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace EQLIPSE.SuperScene
{
	[RequireComponent(typeof(Animator))]
	public class AccountBar : SingletonMonoBehaviour<AccountBar>
	{
		[SerializeField]
		private Text username, rate, wwwText, diffText;

		[SerializeField]
		private Color reachableColor, unreachableColor;

		private Animator animator;

		private float prevRate;

		void Awake()
		{
			prevRate = 0;
			animator = GetComponent<Animator>();

			diffText.text = "";
			diffText.color = new Color(1, 1, 1, 1);

			Observable.Interval(TimeSpan.FromSeconds(1d))
				.Select(_ => Application.internetReachability)
				.Subscribe(reach =>
				{
					if (reach != NetworkReachability.NotReachable)
					{
						wwwText.text = "ONLINE";
						wwwText.color = reachableColor;
					}
					else
					{
						wwwText.text = "OFFLINE";
						wwwText.color = unreachableColor;
					}
				});
		}

		public void InitRate()
		{
			prevRate = User.Rate;
		}

		public void Show(bool show = true)
		{
			if (show) Display();
			animator.SetBool("Show", show);
		}

		public IObservable<Unit> Refresh(bool fetch = true)
		{
			return Observable.Defer(() =>
			{
				Display();
				return Observable.ReturnUnit();
			});
		}

		void Display()
		{
			username.text = User.Name;
			var r = User.Rate;

			var rStr = decimal.Parse(r.ToString("#0.000"));
			var prevStr = decimal.Parse(prevRate.ToString("#0.000"));

			diffText.text = (rStr - prevStr).ToString("+#0.000;-#0.000");

			if (rStr - prevStr > 0)
				animator.SetTrigger("Arrow Up");
			else if (rStr - prevStr < 0)
				animator.SetTrigger("Arrow Down");
			else
				diffText.text = "";

			prevRate = r;

			var _rStr = User.Rate.ToString("#0.000").Split('.');
			rate.text = _rStr[0] + "<size=10>." + _rStr[1] + "</size>";
		}
	}
}