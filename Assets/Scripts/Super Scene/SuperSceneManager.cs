﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using EQLIPSE.Utility;
using Gamelogic.Extensions;
using UniRx;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace EQLIPSE.SuperScene
{
	public class SuperSceneManager : SingletonMonoBehaviour<SuperSceneManager>
	{
		public enum Transitions { Default, Lunatic }
		public enum Directions { Next, Back }

		[HideInInspector]
		public Directions Direction;
		[HideInInspector]
		public int Parameter;

		string scene;
		StateMachine<string> stateMachine;
		public string CurrentState { get { return stateMachine.CurrentState; } }

		IObservable<Unit> TransitionProcessObservable;

		[SerializeField]
		private CanvasGroup LoadingCanvasGroup;

		IEnumerator Start()
		{
			Application.targetFrameRate = 1000;
			Application.backgroundLoadingPriority = ThreadPriority.Low;
			Input.simulateMouseWithTouches = false;
			Input.gyro.enabled = false;

			Parameter = 0;
			Direction = Directions.Next;

			TransitionProcessObservable = null;

			#region Defining State Machine
			stateMachine = new StateMachine<string>();

			// 起動直後のデータ読み込み
			stateMachine.AddState("Loading", () =>
			{
				scene = "loader";
			}, () =>
			{
				stateMachine.CurrentState = "Title";
			});

			// タイトル画面
			stateMachine.AddState("Title", () =>
			{
				AccountBar.Singleton.Show(false);
				scene = "title";
			}, () =>
			{
				switch (Parameter)
				{
					case 0:
						stateMachine.CurrentState = "Menu";
						break;
					case 1:
						stateMachine.CurrentState = "Tutorial";
						break;
				}
			});

			// メニュー画面
			stateMachine.AddState("Menu", () =>
			{
				scene = "menu";

				AccountBar.Singleton.InitRate();
				AccountBar.Singleton.Show(true);

				Environment.Player.Gauge = Difficulty.Gauges.Normal;
				Environment.Player.IsChallenge = false;

				User.Records = new List<Record>();
				Environment.Selector.Infos = new Dictionary<Tuple<string, Chart.Difficulties>, Environment.Selector.InfoEntity>();

				WWWPostQueue.Load();

				// Dirty Hack
				if (User.JudgeOffset == 0)
				{
					User.JudgeOffset = 6;
				}

				BGMManager.Singleton.Play(Resources.Load<AudioClip>("メニュー"));
				BGMManager.Singleton.LowPass(44100, 1, 1);

			}, () =>
			{

				switch (Direction)
				{
					case Directions.Next:
						switch (Parameter)
						{
							case 0:
								BGMManager.Singleton.Stop(1);
								stateMachine.CurrentState = "Select";
								break;
							case 1:
								stateMachine.CurrentState = "Option";
								break;
							case 2:
								BGMManager.Singleton.Stop(1);
								stateMachine.CurrentState = "Tutorial";
								break;
							case 3:
								stateMachine.CurrentState = "Ranking";
								break;
							case 4:
								stateMachine.CurrentState = "Rival";
								break;
							case 5:
								BGMManager.Singleton.Stop(1);
								stateMachine.CurrentState = "Challenge Select";
								break;
						}
						break;
					case Directions.Back:
						BGMManager.Singleton.Stop(1);
						stateMachine.CurrentState = "Title";
						break;
				}
			});

			// チュートリアル
			stateMachine.AddState("Tutorial", () =>
			{
				scene = "player";

				Environment.Player.SelectedSong = Environment.Songs.First(s => s.Title == "Tutorial");
				Environment.Player.SelectedDifficulty = Chart.Difficulties.Easy;

				Environment.Story.Scenario = Environment.Scenarios["Tutorial"][0];
				Environment.Story.SeriphPosition = 0;

				Environment.Player.IsSimplePre = true;

				RenderSettings.fog = true;
				RenderSettings.fogMode = FogMode.Linear;
				RenderSettings.fogStartDistance = 15;
				RenderSettings.fogEndDistance = 23;

				Environment.Player.IsAutoPlay = false;

				AccountBar.Singleton.Show(false);

				Environment.EnableTouchParticle = false;
			}, () =>
			{
				Environment.EnableTouchParticle = true;
				stateMachine.CurrentState = "Menu";
			});

			// ランキング画面
			stateMachine.AddState("Ranking", () => { scene = "rate_ranking"; }, () => { stateMachine.CurrentState = "Menu"; });

			// ライバル管理画面
			stateMachine.AddState("Rival", () => { scene = "rival"; }, () => { stateMachine.CurrentState = "Menu"; });

			// 選曲画面
			stateMachine.AddState("Select", () =>
			{
				scene = "selector_alt";

				Environment.Player.IsSimplePre = false;
				Environment.Player.IsChallenge = false;

				AccountBar.Singleton.Show();

				if (Application.internetReachability != NetworkReachability.NotReachable)
				{
					// var recordSubscr = User.FetchUserDataObservable();

					var infoSubscr = Environment.Selector.FetchInfoObservable();

					var recSubscr = User.FetchRecordsObservable();

					TransitionProcessObservable = Observable.WhenAll(infoSubscr, recSubscr)
						.AsUnitObservable();
				}
				else
				{
					User.Records = new List<Record>();
					Environment.Selector.Infos = new Dictionary<Tuple<string, Chart.Difficulties>, Environment.Selector.InfoEntity>();
				}

				Environment.Selector.AvailableSongs = Environment.FreeAvailableSongs;

			}, () =>
			{
				User.Save();

				User.EnqueueUserData();
				WWWPostQueue.TryPostObservable().Subscribe();

				switch (Direction)
				{
					case Directions.Next:
						stateMachine.CurrentState = "Game";
						break;
					/*
					switch (Parameter)
					{
						case 0:
							stateMachine.CurrentState = "Game";
							break;
						case 1:
							stateMachine.CurrentState = "Challenge Game";
							break;
					}
					break;
					*/
					case Directions.Back:
						stateMachine.CurrentState = "Menu";
						break;
				}
			});

			// ゲーム画面
			stateMachine.AddState("Game", () =>
			{
				scene = "player";

				Environment.Player.Gauge = Environment.Player.IsChallenge
					? Environment.Player.SelectedSong.ChallengeGauge
					: Difficulty.Gauges.Normal;

				RenderSettings.fog = true;
				RenderSettings.fogMode = FogMode.Linear;
				RenderSettings.fogStartDistance = 15;
				RenderSettings.fogEndDistance = 23;

				Environment.Player.IsAutoPlay = false;
				Environment.EnableTouchParticle = false;

				AccountBar.Singleton.Show(false);
			}, () =>
			{
				// 画面効果処理
				RenderSettings.fog = false;
				Environment.EnableTouchParticle = true;

				// ローカルでのデータ処理

				if (User.PlayDatas.Any(e => e.Title == Environment.Player.PlayData.Title &&
											e.Difficulty == Environment.Player.PlayData.Difficulty))
				{
					var highscore = User.PlayDatas
						.Where(e => e.Title == Environment.Player.PlayData.Title &&
									e.Difficulty == Environment.Player.PlayData.Difficulty)
						.Max(e => e.Score);

					Environment.Result.IsNewRecord = Environment.Player.PlayData.Score > highscore;
				}
				else
				{
					Environment.Result.IsNewRecord = true;
				}

				User.PlayDatas.Add(Environment.Player.PlayData);

				var before = new Dictionary<string, Chart.Difficulties?>(User.UnlockedSongs);
				if (Environment.Player.IsChallenge)
				{
					if (Environment.Player.PlayData.IsClear)
					{
						if (User.UnlockedSongs.ContainsKey(Environment.Player.PlayData.Title))
							User.UnlockedSongs[Environment.Player.PlayData.Title] = Environment.Player.PlayData.Difficulty;
						else
							User.UnlockedSongs.Add(Environment.Player.PlayData.Title, Environment.Player.PlayData.Difficulty);
					}
				}
				else
					User.Evaluate(Environment.Player.PlayData);
				var after = new Dictionary<string, Chart.Difficulties?>(User.UnlockedSongs);

				Environment.Result.UnlockedSongs.Clear();
				foreach (var e in after)
				{
					if (!before.ContainsKey(e.Key))
					{
						Environment.Result.UnlockedSongs.Add(new Tuple<string, Chart.Difficulties?>(e.Key, e.Value));
					}
					else if (before[e.Key] != e.Value)
					{
						Environment.Result.UnlockedSongs.Add(new Tuple<string, Chart.Difficulties?>(e.Key, e.Value));
					}
				}

				User.Save();

				AccountBar.Singleton.Show();
				AccountBar.Singleton.Refresh().Subscribe();

				// サーバとのデータ処理
				if (Application.internetReachability != NetworkReachability.NotReachable)
				{
					Environment.Player.EnqueuePlayData();
					User.EnqueueUserData();

					WWWPostQueue.TryPostObservable().Subscribe();
				}

				// TransitionProcessObservable = WWWPostQueue.TryPostObservable();

				stateMachine.CurrentState = "Result";
			});

			// リザルト画面
			stateMachine.AddState("Result", () =>
			{
				scene = "result";
			}, () =>
			{
				stateMachine.CurrentState = "Select";
			});

			/*
			stateMachine.AddState("Scenario Select", () => { scene = "scenario_select"; }, () =>
			{
				stateMachine.CurrentState = "Scenario Play Former";
			});
			stateMachine.AddState("Scenario Play Former", () => { scene = "scenario_player"; }, () =>
			{
				stateMachine.CurrentState = "Scenario Song Select";
			});
			stateMachine.AddState("Scenario Song Select", () => { scene = "selector_alt"; }, () =>
			{
				stateMachine.CurrentState = "Scenario Game";
			});
			stateMachine.AddState("Scenario Game", () => { scene = "player"; }, () =>
			{
				stateMachine.CurrentState = "Scenario Play Latter";
			});
			stateMachine.AddState("Scenario Play Latter", () => { scene = "scenario_player"; }, () =>
			{
				stateMachine.CurrentState = "Scenario Select";
			});
			*/

			// オプション画面
			stateMachine.AddState("Option", () =>
			{
				scene = "option";
				if (Application.internetReachability != NetworkReachability.NotReachable)
					TransitionProcessObservable = User.FetchRecordsObservable()
					.Do(_ =>
						{
							BGMManager.Singleton.LowPass(1000, 1, 1);
						});
				else
					TransitionProcessObservable = Observable.ReturnUnit()
						.Do(_ =>
						{
							BGMManager.Singleton.LowPass(1000, 1, 1);
						});
			}, () =>
			{
				User.Save();
				switch (Direction)
				{
					case Directions.Back:
						stateMachine.CurrentState = "Menu";
						break;
					case Directions.Next:
						BGMManager.Singleton.Stop(1);
						stateMachine.CurrentState = "Title";
						break;
				}
			});

			stateMachine.CurrentState = "Loading";
			#endregion

			RenderSettings.fog = false;

			yield return SceneDealer.Singleton.AddScene(scene);

			yield return new WaitForSeconds(0.5f);

			SceneTransitioner.Singleton.OnTransitionEnded.Invoke();
		}

		public void NextState(Transitions transition)
		{
			TransitionOverlayBridge.Singleton.Transition = transition;
			StartCoroutine(NextCoroutine());
		}

		IEnumerator NextCoroutine()
		{
			yield return TransitionOverlayBridge.Singleton.FadeInCouroutine();

			yield return new WaitForSeconds(0.5f);

			yield return SceneDealer.Singleton.UnloadAllScenes();

			stateMachine.Update();

			if (TransitionProcessObservable != null)
			{
				yield return TransitionProcessObservable.StartAsCoroutine((System.Exception e) =>
					{
						Modal.InfoDialogObservable("エラーが発生しました。\n" + e).Subscribe();
					});

				TransitionProcessObservable = null;
			}

			Resources.UnloadUnusedAssets();

			yield return SceneDealer.Singleton.AddScene(scene);
			if (stateMachine.CurrentState == "Tutorial") yield return SceneDealer.Singleton.AddScene("conversation");


			yield return TransitionOverlayBridge.Singleton.FadeOutCouroutine();

			SceneTransitioner.Singleton.OnTransitionEnded.Invoke();
		}
	}
}