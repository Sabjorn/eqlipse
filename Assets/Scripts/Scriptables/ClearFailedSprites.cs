﻿using UnityEngine;
using System.Collections;

public class ClearFailedSprites : ScriptableObject
{
	public Sprite[] Sprites;
}
