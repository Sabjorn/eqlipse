﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/// <summary>
/// 2017.x を用いたバイナリが Android 上だとキーボードで日本語入力できない問題のワークアラウンド用.
/// InputField の子に Button を置き、その Button にアタッチして使用する.
/// </summary>
public class InputFieldButton : Button
{

	private InputField inputField
	{
		get { return inputField_ ?? (inputField_ = GetComponentInParent<InputField>()); }
	}
	private InputField inputField_;

	private TouchScreenKeyboard keyboard_ = null;
	private string originalText_ = "";
	private bool wasCanceled_ = false;

	void Start()
	{
#if UNITY_EDITOR
		gameObject.SetActive(false);
#endif
	}

	void CreateKeyboard()
	{
		if (keyboard_ != null) return;
		if (!IsInteractable()) return;

		if (!inputField.textComponent || inputField.textComponent.font == null)
			return;

		originalText_ = inputField.text;

		TouchScreenKeyboardType type = TouchScreenKeyboardType.Default;
		bool hide = false;
		bool multiLine = inputField.multiLine;
		switch (inputField.contentType)
		{
			case InputField.ContentType.Alphanumeric:
				type = TouchScreenKeyboardType.NamePhonePad;
				break;
			case InputField.ContentType.DecimalNumber:
				type = TouchScreenKeyboardType.NumbersAndPunctuation;
				break;
			case InputField.ContentType.EmailAddress:
				type = TouchScreenKeyboardType.EmailAddress;
				break;
			case InputField.ContentType.IntegerNumber:
				type = TouchScreenKeyboardType.NumberPad;
				break;
			case InputField.ContentType.Pin:
				type = TouchScreenKeyboardType.PhonePad;
				break;
			case InputField.ContentType.Password:
				hide = true;
				break;
			default:
				type = TouchScreenKeyboardType.Default;
				break;
		}

		keyboard_ = TouchScreenKeyboard.Open(inputField.text, type, true, multiLine, hide);

	}

	void DestroyKeyboard()
	{
		if (!inputField.textComponent || !inputField.IsInteractable())
			return;

		if (wasCanceled_)
		{
			inputField.text = originalText_;
			wasCanceled_ = false;
		}

		if (keyboard_ != null)
		{
			keyboard_.active = false;
			keyboard_ = null;
			originalText_ = "";
		}
	}

	void LateUpdate()
	{
		if (keyboard_ == null) return;

		inputField.text = keyboard_.text;

		if (keyboard_.wasCanceled)
		{
			wasCanceled_ = true;
			DestroyKeyboard();
		}

		if (keyboard_.done)
		{
			DestroyKeyboard();
		}
	}

	public override void OnPointerDown(UnityEngine.EventSystems.PointerEventData eventData)
	{
		CreateKeyboard();
		base.OnPointerDown(eventData);
	}

	public override void OnPointerClick(UnityEngine.EventSystems.PointerEventData eventData)
	{
		CreateKeyboard();
		base.OnPointerClick(eventData);
	}
}