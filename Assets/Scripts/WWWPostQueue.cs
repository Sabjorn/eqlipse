﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UniRx;
using UnityEngine;

namespace EQLIPSE.Utility
{
	public struct WWWPostEntity
	{
		public string Url;
		public byte[] Data;
		public Dictionary<string, string> Headers;

		public override string ToString()
		{
			return Url + ":" + Data.Select(b => b.ToString()).Aggregate((acc, c) => acc + "," + c);
		}
	}

	public static class WWWPostQueue
	{
		public enum PostStatus
		{
			Standby, Tring
		}
		private static IObservable<Unit> _tryPostObservable;

		private static Dictionary<WWWPostEntity, PostStatus> _postDictionary = new Dictionary<WWWPostEntity, PostStatus>();

		public static void Enqueue(WWWPostEntity entity)
		{
			_postDictionary.Add(entity, PostStatus.Standby);
			Debug.Log("POSTエンキュー\nURL: " + entity.Url + "\nデータ: " + Encoding.UTF8.GetString(entity.Data));
		}

		public static IObservable<Unit> TryPostObservable()
		{
			var obs = Observable.ReturnUnit()
				.SelectMany(_ => _postDictionary.Where(e => e.Value == PostStatus.Standby)
					.Select(e => Observable.Defer(() =>
						{
							_postDictionary[e.Key] = PostStatus.Tring;
							return Observable.ReturnUnit();
						})
						.SelectMany(ObservableWWW.Post(e.Key.Url, e.Key.Data, e.Key.Headers))
						.Do(r =>
						{
							Debug.Log("POST成功\nURL: " + e.Key.Url + "\nレスポンス:\n" + r);
							_postDictionary.Remove(e.Key);
						})
						.DoOnError(ex =>
						{
							Debug.LogError(ex);
							_postDictionary[e.Key] = PostStatus.Standby;
						}))
					.WhenAll())
				.DoOnCompleted(Save)
				.AsUnitObservable();
			if (_tryPostObservable == null)
			{
				Debug.Log("初トライ");
				_tryPostObservable = obs.DoOnCompleted(() => { _tryPostObservable = null; });
			}
			else
			{
				Debug.Log("セカンドトライ");
				_tryPostObservable = _tryPostObservable
					.SelectMany(_ => obs);
			}

			return _tryPostObservable;
		}

		public static void Load()
		{
			_postDictionary.Clear();
			if (File.Exists(Environment.WwwQueuePath))
			{
				using (var reader = new StreamReader(File.OpenRead(Environment.WwwQueuePath)))
				{
					var data = reader.ReadToEnd();
					Debug.Log("POSTキューデータ:\n" + data);
					try
					{
						_postDictionary = JsonConvert.DeserializeObject<Dictionary<WWWPostEntity, PostStatus>>(data);
					}
					catch (System.Exception exception)
					{
						Debug.LogWarning("POSTキュー読み込み失敗\n" + exception.Message);
						_postDictionary.Clear();
					}
				}
			}

			Debug.Log("POSTキュー読込完了: " + _postDictionary.Count + "要素");
		}

		public static void Save()
		{
			using (var writer = new StreamWriter(File.Create(Environment.WwwQueuePath)))
			{
				writer.Write(JToken.FromObject(_postDictionary).ToString(Formatting.None));
				writer.Flush();
			}
			Debug.Log("POSTキュー保存完了: " + _postDictionary.Count + "要素");
		}

		public static void Clear()
		{
			_postDictionary.Clear();
		}
	}
}