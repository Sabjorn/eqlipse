﻿using UnityEngine;
using System.Collections;

namespace EQLIPSE.Editor
{
	[RequireComponent(typeof(LineRenderer))]
	public class SlideNoteBehaviour : MonoBehaviour
	{
		[SerializeField]
		Transform TransformFrom, TransformTo;

		public Vector3[] Positions
		{
			set
			{
				lr.SetVertexCount(2);
				lr.SetPositions(value);
				TransformFrom.position = value[0];
				TransformTo.position = value[1];
			}
		}

		LineRenderer _lr;
		LineRenderer lr
		{
			get
			{
				if (_lr == null) _lr = GetComponent<LineRenderer>();
				return _lr;
			}
		}
	}
}