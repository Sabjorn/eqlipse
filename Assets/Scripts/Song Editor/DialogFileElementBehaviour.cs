﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections;
using System.IO;
using System.Linq;

namespace EQLIPSE.Editor
{
	public class DialogFileElementBehaviour : MonoBehaviour
	{
		[SerializeField]
		IconsScriptableObject Icons;

		public StringEvent PathOnSelected;

		Image _icon;
		Image icon
		{
			get
			{
				if (_icon == null) _icon = GetComponentsInChildren<Image>().Where(x => x.gameObject != gameObject).First();
				return _icon;
			}
			set { _icon = value; }
		}

		Text _text;
		Text text
		{
			get
			{
				if (_text == null) _text = GetComponentInChildren<Text>();
				return _text;
			}
			set { _text = value; }
		}

		string fullPath;
		public string FullPath
		{
			get { return fullPath; }
			set
			{
				fullPath = value;
				if (!IsDrive && IsFile)
					icon.sprite = Icons.File;
				else
					icon.sprite = Icons.Folder;

				text.text = IsDrive ? value : Path.GetFileName(value);
			}
		}

		bool isParent;
		public bool IsParent
		{
			get { return isParent; }
			set
			{
				isParent = value;
				icon.sprite = Icons.ParentFolder;

				text.text = "../";
			}
		}

		public bool IsDrive = false;
		public bool IsFile { get { return (File.GetAttributes(fullPath) & FileAttributes.Directory) != FileAttributes.Directory; } }

		public void OnPushed()
		{
			PathOnSelected.Invoke(FullPath);
		}
	}
}