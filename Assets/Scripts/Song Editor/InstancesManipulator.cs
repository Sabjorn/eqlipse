﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using EQLIPSE.Utility;
using UniRx;

namespace EQLIPSE.Editor
{
	public class InstancesManipulator : SingletonMonoBehaviour<InstancesManipulator>
	{
		public Transform LaneTransform, SlideTransform;
		public int Zoom;
		public float VisibleRange;

		[SerializeField]
		GameObject NormalNotePrefab, ChargeNotePrefab, SlideNotePrefab, TapNotePrefab, BPMPrefab;
		[SerializeField]
		GameObject MeasurePrefab;

		Dictionary<Rational, GameObject>[] NormalNotesDictionary;
		Dictionary<Tuple<Rational, Rational>, GameObject>[] ChargeNotesDictionary;
		Dictionary<SlideNote, GameObject> SlideNotesDictionary;
		Dictionary<TapNote, GameObject> TapNotesDictionary;

		Dictionary<int, GameObject> MeasureDictionary;
		GameObject[] BPMObjects;
		GameObject[] MeterObjects;

		int[] iNUB, iNLB;
		int[] iCUB, iCLB;
		int iSUB, iSLB;
		int iTUB, iTLB;

		int iMUB, iMLB;
		int iBUB, iBLB;
		int iMTUB, iMTLB;

		void Start()
		{
			InputManipulator.Singleton.Scroll
				   .Subscribe((x) =>
				   {
					   if (InputManipulator.Singleton.ModKey2.Value) Zoom += x;
				   });

			Clear();
		}

		public void Clear()
		{
			Pool.ReleasePools();

			NormalNotesDictionary = new Dictionary<Rational, GameObject>[]
			{
			new Dictionary<Rational, GameObject>(),
			new Dictionary<Rational, GameObject>(),
			new Dictionary<Rational, GameObject>(),
			new Dictionary<Rational, GameObject>()
			};
			ChargeNotesDictionary = new Dictionary<Tuple<Rational, Rational>, GameObject>[]
			{
			new Dictionary<Tuple<Rational,Rational>, GameObject>(),
			new Dictionary<Tuple<Rational,Rational>, GameObject>(),
			new Dictionary<Tuple<Rational,Rational>, GameObject>(),
			new Dictionary<Tuple<Rational,Rational>, GameObject>()
			};
			SlideNotesDictionary = new Dictionary<SlideNote, GameObject>();
			TapNotesDictionary = new Dictionary<TapNote, GameObject>();

			MeasureDictionary = new Dictionary<int, GameObject>();
			BPMObjects = new GameObject[IOManipulator.Singleton.Chart.BeatBySecond.Derivatives.Count()];

			iNLB = new int[4];
			iNUB = new int[4];
			iCLB = new int[4];
			iCUB = new int[4];
			iSUB = 0; iSLB = 0;
			iTUB = 0; iTLB = 0;

			iMUB = 0; iMLB = 0;
			iBLB = 0; iBUB = 0;
		}

		void Update()
		{
			Chart chart = IOManipulator.Singleton.Chart;
			if (chart != null)
			{
				for (int i = 0; i < 4; i++)
				{
					#region GradNormal Note Bound
					if (ChartManipulator.Singleton.NormalNotes[i].Count > 0)
					{
						iNLB[i] = Mathf.Clamp(iNLB[i], 0, ChartManipulator.Singleton.NormalNotes[i].Count - 1);
						iNUB[i] = Mathf.Clamp(iNUB[i], 0, ChartManipulator.Singleton.NormalNotes[i].Count - 1);

						while (iNLB[i] > 0 && BeatToZ(ChartManipulator.Singleton.NormalNotes[i].Values[iNLB[i] - 1]) > -VisibleRange)
							iNLB[i]--;
						while (iNLB[i] < ChartManipulator.Singleton.NormalNotes[i].Count && BeatToZ(ChartManipulator.Singleton.NormalNotes[i].Values[iNLB[i]]) < -VisibleRange)
							iNLB[i]++;
						while (iNUB[i] > 0 && BeatToZ(ChartManipulator.Singleton.NormalNotes[i].Values[iNUB[i] - 1]) > VisibleRange)
							iNUB[i]--;
						while (iNUB[i] < ChartManipulator.Singleton.NormalNotes[i].Count && BeatToZ(ChartManipulator.Singleton.NormalNotes[i].Values[iNUB[i]]) < VisibleRange)
							iNUB[i]++;
					}
					else { iNLB[i] = 0; iNUB[i] = 0; }

					foreach (var e in NormalNotesDictionary[i])
					{
						Pool.Release(e.Value);
					}
					NormalNotesDictionary[i].Clear();

					for (int j = iNLB[i]; j < iNUB[i]; j++)
					{
						NormalNotesDictionary[i].Add(ChartManipulator.Singleton.NormalNotes[i].Values[j], Pool.Instantiate(NormalNotePrefab));
					}
					#endregion
					#region Charge Note Bound
					if (ChartManipulator.Singleton.ChargeNotes[i].Count > 0)
					{
						iCLB[i] = Mathf.Clamp(iCLB[i], 0, ChartManipulator.Singleton.ChargeNotes[i].Count - 1);
						iCUB[i] = Mathf.Clamp(iCUB[i], 0, ChartManipulator.Singleton.ChargeNotes[i].Count - 1);

						while (iCLB[i] > 0 && BeatToZ(ChartManipulator.Singleton.ChargeNotes[i].Values[iCLB[i] - 1].Item2) > -VisibleRange)
							iCLB[i]--;
						while (iCLB[i] < ChartManipulator.Singleton.ChargeNotes[i].Count && BeatToZ(ChartManipulator.Singleton.ChargeNotes[i].Values[iCLB[i]].Item2) < -VisibleRange)
							iCLB[i]++;
						while (iCUB[i] > 0 && BeatToZ(ChartManipulator.Singleton.ChargeNotes[i].Values[iCUB[i] - 1].Item1) > VisibleRange)
							iCUB[i]--;
						while (iCUB[i] < ChartManipulator.Singleton.ChargeNotes[i].Count && BeatToZ(ChartManipulator.Singleton.ChargeNotes[i].Values[iCUB[i]].Item1) < VisibleRange)
							iCUB[i]++;
					}
					else { iCLB[i] = 0; iCUB[i] = 0; }

					foreach (var e in ChargeNotesDictionary[i])
					{
						Pool.Release(e.Value);
					}
					ChargeNotesDictionary[i].Clear();

					for (int j = iCLB[i]; j < iCUB[i]; j++)
					{
						ChargeNotesDictionary[i].Add(ChartManipulator.Singleton.ChargeNotes[i].Values[j], Pool.Instantiate(ChargeNotePrefab));
					}
					#endregion

					foreach (var e in NormalNotesDictionary[i])
					{
						e.Value.transform.position = LaneTransform.position
							+ Vector3.left * 1.5f + Vector3.right * i
							+ Vector3.forward * BeatToZ(e.Key);
					}
					foreach (var e in ChargeNotesDictionary[i])
					{
						e.Value.transform.position = LaneTransform.position
							+ Vector3.left * 1.5f + Vector3.right * i
							+ Vector3.forward * BeatToZ(e.Key.Item1);

						var c = e.Value.GetComponent<ChargeNoteBehaviour>();
						c.Length = BeatToZ(e.Key.Item2) - BeatToZ(e.Key.Item1);
					}
				}

				#region Measure Bound
				while (iMLB > 0 && BeatToZ(chart.MeasureToBeat(iMLB - 1)) > -VisibleRange)
					iMLB--;
				while (iMLB <= chart.BeatToMeasure(chart.BeatLength) && BeatToZ(chart.MeasureToBeat(iMLB)) < -VisibleRange)
					iMLB++;
				while (iMUB > 0 && BeatToZ(chart.MeasureToBeat(iMUB - 1)) > VisibleRange)
					iMUB--;
				while (iMUB <= chart.BeatToMeasure(chart.BeatLength) && BeatToZ(chart.MeasureToBeat(iMUB)) < VisibleRange)
					iMUB++;

				foreach (var e in MeasureDictionary)
				{
					Pool.Release(e.Value);
				}
				MeasureDictionary.Clear();

				for (int i = iMLB; i < iMUB; i++)
				{
					var g = Pool.Instantiate(MeasurePrefab);
					MeasureDictionary.Add(i, g);
					var c = g.GetComponentInChildren<TextMesh>();
					c.text = "#" + i;
				}
				#endregion
				foreach (var e in MeasureDictionary)
				{
					e.Value.transform.position = Vector3.forward * (IOManipulator.Singleton.Chart.MeasureToBeat(e.Key) - ChartManipulator.Singleton.Cursor) * Mathf.Pow(1.2f, Zoom);
				}

				#region BPM Bound
				while (iBLB > 0 && BeatToZ((float)chart.BeatBySecond.Derivatives[iBLB - 1].Item1) > -VisibleRange)
				{
					iBLB--;
					var g = Pool.Instantiate(BPMPrefab);
					BPMObjects[iBLB] = g;
				}
				while (iBUB < chart.BeatBySecond.Derivatives.Count() && BeatToZ((float)chart.BeatBySecond.Derivatives[iBUB].Item1) < VisibleRange)
				{
					var g = Pool.Instantiate(BPMPrefab);
					BPMObjects[iBUB] = g;
					iBUB++;
				}
				while (iBLB < chart.BeatBySecond.Derivatives.Count() && BeatToZ((float)chart.BeatBySecond.Derivatives[iBLB].Item1) < -VisibleRange)
				{
					Pool.Release(BPMObjects[iBLB]);
					BPMObjects[iBLB] = null;
					iBLB++;
				}
				while (iBUB > 0 && BeatToZ((float)chart.BeatBySecond.Derivatives[iBUB - 1].Item1) > VisibleRange)
				{
					iBUB--;
					Pool.Release(BPMObjects[iBUB]);
					BPMObjects[iBUB] = null;
				}

				for (int i = iBLB; i < iBUB; i++)
				{
					var g = BPMObjects[i];
					g.transform.position = Vector3.forward * BeatToZ((float)chart.BeatBySecond.Derivatives[i].Item1);
					var c = g.GetComponentInChildren<TextMesh>();
					c.text = 60.0 / chart.BeatBySecond.Derivatives[i].Item2 + " BPM";
				}
				#endregion

				#region Slide Note Bound
				if (ChartManipulator.Singleton.SlideNotes.Count > 0)
				{
					iSLB = Mathf.Clamp(iSLB, 0, ChartManipulator.Singleton.SlideNotes.Count - 1);
					iSUB = Mathf.Clamp(iSUB, 0, ChartManipulator.Singleton.SlideNotes.Count - 1);

					while (iSLB > 0 && BeatToZ(ChartManipulator.Singleton.SlideNotes.Values[iSLB - 1].BeatTo) > -VisibleRange)
						iSLB--;
					while (iSLB < ChartManipulator.Singleton.SlideNotes.Count && BeatToZ(ChartManipulator.Singleton.SlideNotes.Values[iSLB].BeatTo) < -VisibleRange)
						iSLB++;
					while (iSUB > 0 && BeatToZ(ChartManipulator.Singleton.SlideNotes.Values[iSUB - 1].BeatFrom) > VisibleRange)
						iSUB--;
					while (iSUB < ChartManipulator.Singleton.SlideNotes.Count && BeatToZ(ChartManipulator.Singleton.SlideNotes.Values[iSUB].BeatFrom) < VisibleRange)
						iSUB++;
				}
				else { iSUB = 0; iSLB = 0; }

				foreach (var e in SlideNotesDictionary)
				{
					Pool.Release(e.Value);
				}
				SlideNotesDictionary.Clear();

				for (int i = iSLB; i < iSUB; i++)
				{
					var g = Pool.Instantiate(SlideNotePrefab);
					SlideNotesDictionary.Add(ChartManipulator.Singleton.SlideNotes.Values[i], g);
				}
				#endregion

				foreach (var e in SlideNotesDictionary)
				{
					var c = e.Value.GetComponent<SlideNoteBehaviour>();

					Vector3[] positions = new Vector3[]
					{
					SlideTransform.position
					+ Vector3.left * 2f + Vector3.right * e.Key.ValueFrom * 4f
					+ Vector3.forward * BeatToZ(e.Key.BeatFrom),
					SlideTransform.position
					+ Vector3.left * 2f + Vector3.right * e.Key.ValueTo * 4f
					+ Vector3.forward * BeatToZ(e.Key.BeatTo)
					};

					c.Positions = positions;
				}

				#region Tap Note Bound
				if (ChartManipulator.Singleton.TapNotes.Count > 0)
				{
					iTLB = Mathf.Clamp(iTLB, 0, ChartManipulator.Singleton.TapNotes.Count - 1);
					iTUB = Mathf.Clamp(iTUB, 0, ChartManipulator.Singleton.TapNotes.Count - 1);

					while (iTLB > 0 && BeatToZ(ChartManipulator.Singleton.TapNotes.Values[iTLB - 1].Beat) > -VisibleRange)
						iTLB--;
					while (iTLB < ChartManipulator.Singleton.TapNotes.Count && BeatToZ(ChartManipulator.Singleton.TapNotes.Values[iTLB].Beat) < -VisibleRange)
						iTLB++;
					while (iTUB > 0 && BeatToZ(ChartManipulator.Singleton.TapNotes.Values[iTUB - 1].Beat) > VisibleRange)
						iTUB--;
					while (iTUB < ChartManipulator.Singleton.TapNotes.Count && BeatToZ(ChartManipulator.Singleton.TapNotes.Values[iTUB].Beat) < VisibleRange)
						iTUB++;
				}
				else { iTUB = 0; iTLB = 0; }

				foreach (var e in TapNotesDictionary)
				{
					Pool.Release(e.Value);
				}
				TapNotesDictionary.Clear();

				for (int i = iTLB; i < iTUB; i++)
				{
					var g = Pool.Instantiate(TapNotePrefab);
					TapNotesDictionary.Add(ChartManipulator.Singleton.TapNotes.Values[i], g);
				}
				#endregion
				foreach (var e in TapNotesDictionary)
				{
					e.Value.transform.position = SlideTransform.position
						+ Vector3.left * 2f + Vector3.right * e.Key.Value * 4f
						+ Vector3.forward * BeatToZ(e.Key.Beat);
				}

			}
		}

		float BeatToZ(float beat)
		{
			return (beat - ChartManipulator.Singleton.Cursor) * Mathf.Pow(1.2f, Zoom);
		}
	}
}