﻿using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections;
using System.Security.AccessControl;

namespace EQLIPSE.Editor
{
	public class SaveFileDialog : SingletonMonoBehaviour<SaveFileDialog>
	{
		string _currentPath;
		string CurrentPath
		{
			get { return _currentPath; }
			set
			{
				if (File.Exists(value))
				{
					if (Path.GetExtension(value) == ".eql")
					{
						OnSelected.Invoke(value);
						return;
					}
					else
					{
						Debug.LogWarning("ファイルの種類が違います");
						return;
					}
				}
				else
				{
					PathField.text = value;

					foreach (var e in entities) { Destroy(e); }
					entities.Clear();

					string[] paths = { };
					bool isDrives = false;

					if (string.IsNullOrEmpty(value))
					{
						isDrives = true;

						paths = Directory.GetLogicalDrives();
						Array.Sort(paths, (l, r) => { return string.Compare(l, r); });
					}
					else if (Directory.Exists(value))
					{
						_currentPath = value;

						paths = Directory.GetFileSystemEntries(CurrentPath)
							.Where(p => Path.GetExtension(p) == ".eql" || (File.GetAttributes(p) & FileAttributes.Directory) == FileAttributes.Directory)
							.ToArray();
						Array.Sort(paths, (l, r) =>
						{
							bool isLDir = (File.GetAttributes(l) & FileAttributes.Directory) == FileAttributes.Directory;
							bool isRDir = (File.GetAttributes(r) & FileAttributes.Directory) == FileAttributes.Directory;

							if (isLDir != isRDir) return isLDir ? -1 : 1;
							return string.Compare(l, r);
						});
					}

					foreach (var path in paths)
					{
						GameObject g = Instantiate(FileElementPrefab);
						entities.Add(g);
						g.transform.SetParent(FilesTransform, false);

						DialogFileElementBehaviour b = g.GetComponent<DialogFileElementBehaviour>();
						if (isDrives) b.IsDrive = true;
						b.FullPath = path;
						b.PathOnSelected.AddListener(_path => { CurrentPath = _path; });
					}
				}


			}
		}

		List<GameObject> entities;

		[SerializeField]
		GameObject FileElementPrefab;
		[SerializeField]
		Transform FilesTransform;
		[SerializeField]
		InputField PathField, FilenameField;

		[SerializeField]
		GameObject ConfirmationDialog;

		public StringEvent OnSelected;

		void Start()
		{
			entities = new List<GameObject>();
			CurrentPath = Application.dataPath;
		}

		public void GotoParent()
		{
			if (!string.IsNullOrEmpty(CurrentPath))
			{
				CurrentPath = Path.GetDirectoryName(CurrentPath);
			}
		}

		public void OnPathFieldTyped(string path) { SetPath(path); }

		public void SetPath(string path) { CurrentPath = path; }

		public void OnOK()
		{
			if (File.Exists(CurrentPath + "/" + FilenameField.text))
			{
				ConfirmationDialog.SetActive(true);
			}
			else { OnConfirmed(); }
		}

		public void OnConfirmed()
		{
			OnSelected.Invoke(Path.ChangeExtension(CurrentPath + "/" + FilenameField.text, ".eql"));
			gameObject.SetActive(false);
		}
	}
}