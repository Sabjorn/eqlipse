﻿using UnityEngine;
using System.Collections;

namespace EQLIPSE.Editor
{
	public class ChargeNoteBehaviour : MonoBehaviour
	{
		[SerializeField]
		Transform Begin, End, Beam;

		public float Length
		{
			set
			{
				End.localPosition = Vector3.forward * value;
				Beam.localScale = new Vector3(1, 1, value);
			}
		}
	}
}