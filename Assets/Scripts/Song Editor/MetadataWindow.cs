﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace EQLIPSE.Editor
{
	public class MetadataWindow : MonoBehaviour
	{
		[SerializeField]
		InputField TitleField, ArtistField, GenreField, LevelField, BPMField, WaveField;
		[SerializeField]
		Dropdown DifficultyDropdown;

		public void Fetch()
		{
			TitleField.text = IOManipulator.Singleton.Chart.Title;
			ArtistField.text = IOManipulator.Singleton.Chart.Artist;
			GenreField.text = IOManipulator.Singleton.Chart.Genre;
			LevelField.text = IOManipulator.Singleton.Chart.Level.ToString();
			BPMField.text = ((double)(IOManipulator.Singleton.Chart.BpmStandard)).ToString();
			WaveField.text = IOManipulator.Singleton.Chart.WaveFileName;
			DifficultyDropdown.value = (int)IOManipulator.Singleton.Chart.Difficulty;
		}
	}
}