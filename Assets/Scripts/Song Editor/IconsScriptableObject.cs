﻿using UnityEngine;
using System.Collections;

public class IconsScriptableObject : ScriptableObject {
    public Sprite File, Folder, ParentFolder;
}