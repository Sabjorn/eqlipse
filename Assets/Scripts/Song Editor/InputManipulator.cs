﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using UniRx;

namespace EQLIPSE.Editor
{
	public class InputManipulator : SingletonMonoBehaviour<InputManipulator>
	{
		public ReactiveProperty<int> Scroll;

		public ReactiveProperty<bool>[] NumericKeys;
		public ReactiveProperty<bool> ModKey1, ModKey2, ModKey3;

		void Awake()
		{
			Scroll = new ReactiveProperty<int>(0);

			NumericKeys = new ReactiveProperty<bool>[10]
			{
			new ReactiveProperty<bool>(false),
			new ReactiveProperty<bool>(false),
			new ReactiveProperty<bool>(false),
			new ReactiveProperty<bool>(false),
			new ReactiveProperty<bool>(false),
			new ReactiveProperty<bool>(false),
			new ReactiveProperty<bool>(false),
			new ReactiveProperty<bool>(false),
			new ReactiveProperty<bool>(false),
			new ReactiveProperty<bool>(false)
			};

			ModKey1 = new ReactiveProperty<bool>(false);
			ModKey2 = new ReactiveProperty<bool>(false);
			ModKey3 = new ReactiveProperty<bool>(false);
		}

		void Update()
		{
			int scr = 0;
			if (Input.GetKeyDown(KeyCode.UpArrow)) scr++;
			if (Input.GetKeyDown(KeyCode.DownArrow)) scr--;
			scr += (int)Input.mouseScrollDelta.y;
			Scroll.Value = scr;

			NumericKeys[0].Value = Input.GetKey(KeyCode.Alpha1);
			NumericKeys[1].Value = Input.GetKey(KeyCode.Alpha2);
			NumericKeys[2].Value = Input.GetKey(KeyCode.Alpha3);
			NumericKeys[3].Value = Input.GetKey(KeyCode.Alpha4);
			NumericKeys[4].Value = Input.GetKey(KeyCode.Alpha5);
			NumericKeys[5].Value = Input.GetKey(KeyCode.Alpha6);
			NumericKeys[6].Value = Input.GetKey(KeyCode.Alpha7);
			NumericKeys[7].Value = Input.GetKey(KeyCode.Alpha8);
			NumericKeys[8].Value = Input.GetKey(KeyCode.Alpha9);
			NumericKeys[9].Value = Input.GetKey(KeyCode.Alpha0);

			ModKey1.Value = Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);
			ModKey2.Value = Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl);
			ModKey3.Value = Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt);
		}
	}
}