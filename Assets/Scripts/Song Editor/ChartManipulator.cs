﻿using UnityEngine;
using System.Collections.Generic;
using UniRx;

namespace EQLIPSE.Editor
{
	public class ChartManipulator : SingletonMonoBehaviour<ChartManipulator>
	{
		public Rational Cursor, Delta;

		public SortedList<Rational, Rational>[] NormalNotes;
		public SortedList<Rational, Tuple<Rational, Rational>>[] ChargeNotes;
		public SortedList<Rational, SlideNote> SlideNotes;
		public SortedList<Rational, TapNote> TapNotes;

		void Start()
		{
			Cursor = 0; Delta = 1;

			NormalNotes = new SortedList<Rational, Rational>[4]
			{
				new SortedList<Rational, Rational>(),
				new SortedList<Rational, Rational>(),
				new SortedList<Rational, Rational>(),
				new SortedList<Rational, Rational>()
			};
			ChargeNotes = new SortedList<Rational, Tuple<Rational, Rational>>[4]
			{
				new SortedList<Rational, Tuple<Rational, Rational>>(),
				new SortedList<Rational, Tuple<Rational, Rational>>(),
				new SortedList<Rational, Tuple<Rational, Rational>>(),
				new SortedList<Rational, Tuple<Rational, Rational>>()
			};
			SlideNotes = new SortedList<Rational, SlideNote>();
			TapNotes = new SortedList<Rational, TapNote>();

			InputManipulator.Singleton.Scroll
				.Subscribe((x) =>
				{
					if (!InputManipulator.Singleton.ModKey2.Value) Cursor += x;
				}).AddTo(gameObject);

			OpenFileDialog.Singleton.OnSelected.AsObservable()
				.SelectMany((path) => IOManipulator.Singleton.LoadObservable(path))
				.Subscribe((chart) =>
				{
					for (int i = 0; i < 4; i++)
					{
						NormalNotes[i] = new SortedList<Rational, Rational>();
						foreach (var note in chart.Notes[i])
						{
							NormalNotes[i].Add(note.Beat, note.Beat);
						}

						ChargeNotes[i] = new SortedList<Rational, Tuple<Rational, Rational>>();
						foreach (var cNote in chart.ChargeNotes[i])
						{
							ChargeNotes[i].Add(cNote.Beat[0], new Tuple<Rational, Rational>(cNote.Beat[0], cNote.Beat[1]));
						}
					}

					SlideNotes = new SortedList<Rational, SlideNote>();
					foreach (var sNote in chart.SlideNotes)
					{
						SlideNotes.Add(sNote.BeatFrom, sNote);
					}
					TapNotes = new SortedList<Rational, TapNote>();
					foreach(var tNote in chart.TapNotes)
					{
						TapNotes.Add(tNote.Beat, tNote);
					}
				}).AddTo(gameObject);

			for (int i = 0; i < 4; i++)
			{
				int _i = i; // for compiler's bug.
				InputManipulator.Singleton.NumericKeys[_i]
					.DistinctUntilChanged()
					.Subscribe((k) =>
					{
						if (k)
						{
							if (InputManipulator.Singleton.ModKey1.Value)
							{

							}
							else if (InputManipulator.Singleton.ModKey2.Value)
							{

							}
							else if (InputManipulator.Singleton.ModKey3.Value)
							{

							}
							else
							{
								if (NormalNotes[_i].ContainsKey(Cursor))
								{
									NormalNotes[_i].Remove(Cursor);
								}
								else
								{
									NormalNotes[_i].Add(Cursor, Cursor);
								}
							}
						}
					}).AddTo(gameObject);
			}
		}
	}
}