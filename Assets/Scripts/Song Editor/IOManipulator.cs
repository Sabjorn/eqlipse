﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.IO;
using UniRx;
using Gamelogic.Extensions;

namespace EQLIPSE.Editor
{
	public class IOManipulator : SingletonMonoBehaviour<IOManipulator>
	{
		bool isLoading;
		public bool IsLoading
		{
			get { return isLoading; }
			private set { isLoading = value; }
		}

		public Chart Chart = new Chart();

		public UnityEvent OnLoaded;

		public IObservable<Chart> LoadObservable(string path)
		{
			return Observable.FromCoroutine<Chart>(obs => LoadObservableCoroutine(obs, path));
		}

		public void Load(string path)
		{
			StartCoroutine(LoadCoroutine(path));
		}

		IEnumerator LoadObservableCoroutine(IObserver<Chart> obs, string path)
		{
			IsLoading = true;

			WWW wwwChart = new WWW("file://" + path);
			yield return wwwChart;

			Chart = new Chart();
			Chart.Parse(wwwChart.text);

			obs.OnNext(Chart);

			IsLoading = false;
		}

		IEnumerator LoadCoroutine(string path)
		{
			IsLoading = true;

			WWW wwwChart = new WWW("file://" + path);
			yield return wwwChart;

			Chart = new Chart();
			Chart.Parse(wwwChart.text);

			IsLoading = false;
			Debug.Log("Load Complete: " + Chart.Title);

			OnLoaded.Invoke();
		}

		public void Save(string path)
		{
			using (StreamWriter sw = new StreamWriter(path, false, System.Text.Encoding.UTF8))
			{
				Chart.SaveSong(sw);
			}
		}
	}
}