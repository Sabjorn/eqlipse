﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace EQLIPSE.Selector
{
	public class TopRankerEntity : MonoBehaviour
	{
		[SerializeField] private Text RankText, NameText, ScoreText;

		public int Rank
		{
			set { RankText.text = value + "."; }
		}

		public string Name { set { NameText.text = value; } }

		public int Score { set { ScoreText.text = value.ToString("#,0"); } }
	}
}