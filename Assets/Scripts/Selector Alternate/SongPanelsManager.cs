﻿using UnityEngine;
using UnityEngine.Events;
using UniRx;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using EQLIPSE.Utility;
using UnityEngine.UI;

namespace EQLIPSE.Selector
{
	public class SongPanelsManager : SingletonMonoBehaviour<SongPanelsManager>
	{
		List<SongPanelBehaviour> _panels;

		public Transform ParentOfPanels;
		public GameObject PanelPrefab;

		public Dragger PanelsDragger;
		public SelectorScrollBar Scrollbar;

		public int SortEffectRange;

		public float DelayDuration;

		[SerializeField] private Button ChallengeButton;

		private bool _isChallenge;

		void Start()
		{
			Dragger.Singleton.IsDragging
				.Where(d => d)
				.Subscribe(d => { ShrinkAll(); }).AddTo(gameObject);

			Scrollbar.IsDragging
				.Where(d => d)
				.Subscribe(d =>
				{
					ShrinkAll();
				}).AddTo(gameObject);

			_isChallenge = false;
			Environment.Player.IsChallenge = _isChallenge;

			_panels = new List<SongPanelBehaviour>();
			ReplaceSongs(Environment.Player.IsChallenge ? Environment.ChallengeAvailableSongs : Environment.FreeAvailableSongs);
			SelectDifficulty(Environment.Player.SelectedDifficulty);

			Debug.Log("チャレンジ: " + Environment.ChallengeAvailableSongs.Count + "曲");

			ChallengeButton.gameObject.SetActive(Environment.ChallengeAvailableSongs.Count > 0);
		}

		public void ToggleMode()
		{
			_isChallenge = !_isChallenge;

			SceneTransitioner.Singleton.Parameter = _isChallenge ? 1 : 0;

			Environment.Player.IsChallenge = _isChallenge;

			ReplaceSongs(_isChallenge ? Environment.ChallengeAvailableSongs : Environment.FreeAvailableSongs, false);
		}

		public void ReplaceSongs(Dictionary<Song, bool[]> songsAvailability, bool refresh = true)
		{
			StartCoroutine(ReplaceCoroutine(songsAvailability, refresh));
		}

		IEnumerator ReplaceCoroutine(Dictionary<Song, bool[]> songsAvailability, bool refresh = true)
		{
			ShrinkAll(refresh);
			foreach (var panel in _panels)
			{
				if (panel.gameObject.activeSelf)
					panel.Out();
			}
			yield return new WaitForSeconds(0.5f);

			Pool.ReleasePools();
			_panels.Clear();

			int i = 0;
			foreach (var entity in songsAvailability)
			{
				GameObject p = Pool.Instantiate(PanelPrefab);
				p.transform.SetParent(ParentOfPanels, false);
				var behaviour = p.GetComponent<SongPanelBehaviour>();
				behaviour.Song = entity.Key;
				behaviour.AreSelectable = entity.Value;
				behaviour.Number = i;

				_panels.Add(p.GetComponent<SongPanelBehaviour>());
				p.GetComponent<SongPanelBehaviour>().In();

				i++;
			}
			yield return null;
			Scroller.Singleton.Reparent();
			Scroller.Singleton.PreferredPosition = 0;
		}

		public void SelectSong(Song song)
		{
			Environment.Player.SelectedSong = song;
			for (int i = 0; i < Enum.GetValues(typeof(Chart.Difficulties)).Length; i++)
			{
				if (!song.LevelDictionary.ContainsKey(Environment.Player.SelectedDifficulty))
				{
					Environment.Player.SelectedDifficulty = (Chart.Difficulties)((int)(Environment.Player.SelectedDifficulty + 1) % Enum.GetValues(typeof(Chart.Difficulties)).Length);
				}
			}
		}
		public void SelectDifficulty(Chart.Difficulties diff)
		{
			Environment.Player.SelectedDifficulty = diff;
			if (Environment.Player.SelectedSong != null)
			{
				for (int i = 0; i < Enum.GetValues(typeof(Chart.Difficulties)).Length; i++)
				{
					if (!Environment.Player.SelectedSong.LevelDictionary.ContainsKey(Environment.Player.SelectedDifficulty))
					{
						Environment.Player.SelectedDifficulty =
							(Chart.Difficulties)
							((int)(Environment.Player.SelectedDifficulty + 1) % Enum.GetValues(typeof(Chart.Difficulties)).Length);
					}
				}
			}
		}

		public void ShrinkAll(bool refresh = true)
		{
			for (int i = 0; i < _panels.Count; i++)
			{
				if (_panels[i].gameObject.activeSelf)
				{
					_panels[i].Shrink(refresh);
				}
			}
			CanvasAnimator.Singleton.ShowStart(false);
		}

		void OnDestroy()
		{
			Pool.ReleasePools();
		}
	}
}