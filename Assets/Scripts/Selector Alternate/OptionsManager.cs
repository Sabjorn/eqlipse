﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using EQLIPSE.SuperScene;

namespace EQLIPSE.Selector
{
	public class OptionsManager : MonoBehaviour
	{
		Animator _animator;
		Animator animator { get { return _animator ?? (_animator = GetComponent<Animator>()); } }
		public CanvasGroup AudioOffsetGroup;
		public Text TextHispeed, TextOffset, TextAudioOffset;
		public Text TextCurrentSort;

		void Start()
		{
			AudioOffsetGroup.alpha = User.AdjustAudio ? 1 : 0;
			AudioOffsetGroup.blocksRaycasts = User.AdjustAudio;
		}

		void Update()
		{
			TextHispeed.text = "× " + User.HiSpeed.ToString("#0.0");

			switch (System.Math.Sign(User.DisplayingJudgeOffset))
			{
				case 1:
					TextOffset.text = "+ ";
					break;
				case 0:
					TextOffset.text = "± ";
					break;
				case -1:
					TextOffset.text = "- ";
					break;
			}
			TextOffset.text += System.Math.Abs(User.DisplayingJudgeOffset).ToString("#0");

			switch (System.Math.Sign(User.AudioOffset))
			{
				case 1:
					TextAudioOffset.text = "+ ";
					break;
				case 0:
					TextAudioOffset.text = "± ";
					break;
				case -1:
					TextAudioOffset.text = "- ";
					break;
			}
			TextAudioOffset.text += System.Math.Abs(User.AudioOffset).ToString("#0");
		}

		public void OnHSUp()
		{
			SEPlayer.Singleton.Play(SEs.MiniButton);
			User.HiSpeed += 0.1f;
			if (User.HiSpeed > 5) User.HiSpeed = 5;
		}
		public void OnHSDown()
		{
			SEPlayer.Singleton.Play(SEs.MiniButton);
			User.HiSpeed -= 0.1f;
			if (User.HiSpeed < 0.5f) User.HiSpeed = 0.5f;
		}

		public void OnOffsetUp()
		{
			SEPlayer.Singleton.Play(SEs.MiniButton);
			User.JudgeOffset += 1;
		}
		public void OnOffsetDown()
		{
			SEPlayer.Singleton.Play(SEs.MiniButton);
			User.JudgeOffset -= 1;
		}

		public void OnAudioOffset(int delta)
		{
			SEPlayer.Singleton.Play(SEs.MiniButton);
			User.AudioOffset += delta;
		}

		public void OnNoob()
		{
			SEPlayer.Singleton.Play(SEs.MiniButton);
			Environment.Option.HSNoob();
		}
		public void OnPro()
		{
			SEPlayer.Singleton.Play(SEs.MiniButton);
			Environment.Option.HSPro();
		}

		public void ShowOptions() { animator.SetBool("Is Option Visible", true); }
		public void HideOptions() { animator.SetBool("Is Option Visible", false); }
	}
}