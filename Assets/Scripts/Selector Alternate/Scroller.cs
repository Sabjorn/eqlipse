﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using EQLIPSE.Selector;
using UniRx;

namespace EQLIPSE
{
    public class Scroller : SingletonMonoBehaviour<Scroller>
    {
        RectTransform[] rtChildren;

        Animator animator;

        int Snaps;

        float position;
        public float PreferredPosition;

        public float ShrinkRate;

        public float Interval;
        public float Separation;

        public int VisibleScope;

        public SelectorScrollBar Scrollbar;

        void Awake()
        {
            position = 0;
            PreferredPosition = 0;

            animator = GetComponent<Animator>();
        }
        void Start()
        {
            Dragger.Singleton.DeltaPositionObservable
                .Subscribe(delta => {
                    position += delta;
                    position = Mathf.Clamp(position, 0, rtChildren.Length-1);
                }).AddTo(gameObject);
            Dragger.Singleton.IsDragging
                .CombineLatest(Dragger.Singleton.IsInerting, (d, i) => new Tuple<bool, bool>(d, i))
                .Where(t => !t.Item1 && !t.Item2)
                .Subscribe(t => { PreferredPosition = Mathf.Round(position); }).AddTo(gameObject);

            Scrollbar.onValueChanged.AsObservable()
                .Subscribe(value =>
                {
                    position = value * (rtChildren.Length - 1);
                }).AddTo(gameObject);

            Scrollbar.IsDragging
                .Where(d => !d)
                .Subscribe((bool d) => { PreferredPosition = Mathf.Round(position); }).AddTo(gameObject);

			Reparent();
        }

		public void Reparent()
		{
			List<RectTransform> lsRtChildren = new List<RectTransform>();
			foreach (RectTransform childTransform in transform)
			{
				lsRtChildren.Add(childTransform);
			}
			rtChildren = lsRtChildren.ToArray();
		}

        void Update()
        {
            if (!Dragger.Singleton.IsDragging.Value && !Dragger.Singleton.IsInerting.Value && !Scrollbar.IsDragging.Value)
            {
                position = Mathf.Lerp(PreferredPosition, position, 0.9f);
            }

            for (int i = 0; i < rtChildren.Length; i++)
            {
                if (rtChildren[i] != null)
                {
                    if (Mathf.Abs(position - i) <= VisibleScope)
                    {
                        rtChildren[i].gameObject.SetActive(true);
                        rtChildren[i].anchoredPosition = new Vector2(0, PositionOf(i));
                        rtChildren[i].localScale = Vector3.one * ScaleOf(i);
                    }
                    else
                        rtChildren[i].gameObject.SetActive(false);
                }
            }

            if (rtChildren.Length > 1) Scrollbar.value = position / (rtChildren.Length - 1);
        }

        float PositionOf(int i)
        {
            return (position - i) * Interval + Mathf.Clamp(position - i, -1, 1) * Separation;
        }

        float ScaleOf(int i)
        {
            return 1 - Mathf.Abs(Mathf.Clamp(position - i, -1, 1)) * (1 - ShrinkRate);
        }

        public void Expand() { animator.SetBool("IsExpanded", true); }
        public void Shrink() { animator.SetBool("IsExpanded", false); }
    }
}