﻿using UnityEngine;
using System.Collections;

namespace EQLIPSE.Selector
{
	[RequireComponent(typeof(Animator))]
	public class CanvasAnimator : SingletonMonoBehaviour<CanvasAnimator>
	{
		Animator animator;

		// Use this for initialization
		void Awake()
		{
			animator = GetComponent<Animator>();
		}

		void Update()
		{
			animator.SetBool("Is Challenge", Environment.Player.IsChallenge);
		}

		public void ShowOption(bool visible = true)
		{
			animator.SetBool("Is Option Visible", visible);
		}
		public void ShowStart(bool visible = true)
		{
			animator.SetBool("Is Start Visible", visible);
		}
	}
}