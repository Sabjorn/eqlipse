﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UniRx;
using UnityEngine.EventSystems;

namespace EQLIPSE.Selector
{
    public class SelectorScrollBar : Scrollbar,IEndDragHandler
    {
        public ReactiveProperty<bool> IsDragging;

        protected override void Awake()
        {
            base.Awake();
            IsDragging = new ReactiveProperty<bool>();
        }

        public override void OnBeginDrag(PointerEventData eventData)
        {
            base.OnBeginDrag(eventData);
            IsDragging.Value = true;
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            IsDragging.Value = false;
        }
    }
}