﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using UniRx;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using EQLIPSE.SuperScene;
using EQLIPSE.Utility;

namespace EQLIPSE.Selector
{
	[RequireComponent(typeof(Animator))]
	public class SongPanelBehaviour : MonoBehaviour
	{
		Animator animator;

		public int Number;

		private bool[] _areSelectable = { true, true, true };
		public bool[] AreSelectable
		{
			get { return _areSelectable; }
			set
			{
				_areSelectable = value;
				RefreshSelectables();
			}
		}

		bool isExpanded;

		Sprite[] ButtonSprites;

		[SerializeField]
		Sprite[] ClearFull;

		Song _song;
		public Song Song
		{
			set
			{
				_song = value;

				ImageCover.texture = value.Cover;
				TextTitle.text = value.Title;
				TextGenre.text = "ジャンル: " + value.Genre;
				TextArtist.text = "アーティスト: " + value.Artist;

				SecretImage.color = (User.HasChartUnlock(Song.Title, Chart.Difficulties.Easy) ||
									User.HasChartUnlock(Song.Title, Chart.Difficulties.Normal) ||
									User.HasChartUnlock(Song.Title, Chart.Difficulties.Hard))
									&& !Environment.Player.IsChallenge
					? Color.white
					: Color.clear;

				RefreshSelectables();
			}
			get
			{
				return _song;
			}
		}

		public RawImage ImageCover;
		public Text TextTitle;
		public Text TextGenre;
		public Text TextArtist;
		public Text[] LevelTexts;

		public Text TextHighScore, TextRank, TextClearRate;

		public Image[] ClearedImage;

		public Image SecretImage;

		public Image[] DifficultyLabels;
		public Button[] DifficultyButtons;
		public Sprite SelectedButton;

		public Sprite ClearSprite, FullComboSprite;

		[SerializeField] private GameObject TopRankerPrefab;
		[SerializeField] private Transform TopRankersParent;

		private List<GameObject> _topRankerInstances;

		void Awake()
		{
			animator = GetComponent<Animator>();
			isExpanded = false;

			ButtonSprites = new Sprite[3];

			for (int i = 0; i < 3; i++)
			{
				ButtonSprites[i] = DifficultyButtons[i].GetComponent<Image>().sprite;
			}

			_topRankerInstances = new List<GameObject>();

			SecretImage.color = Color.clear;
		}

		public void Expand()
		{
			isExpanded = true;

			Scroller.Singleton.Expand();
			animator.SetBool("IsExpanded", true);

			SongPanelsManager.Singleton.SelectSong(Song);

			RefreshInfo();

			AudioPreviewer.Singleton.PlayPreviewSound(Song.Preview);

			SetButtonSprite(Environment.Player.SelectedDifficulty);
		}
		public void Shrink(bool refresh = true)
		{
			isExpanded = false;

			Scroller.Singleton.Shrink();
			AudioPreviewer.Singleton.StopPreviewSound();
			animator.SetBool("IsExpanded", false);

			SetButtonSprite(null);

			if (refresh)
				SecretImage.color = !Environment.Player.IsChallenge && (User.HasChartUnlock(Song.Title, Chart.Difficulties.Easy) ||
																		User.HasChartUnlock(Song.Title, Chart.Difficulties.Normal) ||
																		User.HasChartUnlock(Song.Title, Chart.Difficulties.Hard))
					? Color.white
					: Color.clear;
		}

		public void OnPanelTouched()
		{

			if (!isExpanded)
			{
				SEPlayer.Singleton.Play(SEs.SongOpen);
				SongPanelsManager.Singleton.ShrinkAll();
				CanvasAnimator.Singleton.ShowStart(true);
				Scroller.Singleton.PreferredPosition = Number;
				Expand();
			}
			else
			{
				SongPanelsManager.Singleton.ShrinkAll();
			}
		}

		public void OnButtonEasy()
		{
			if (!isExpanded)
			{
				OnPanelTouched();
			}
			else
			{
				SEPlayer.Singleton.Play(SEs.MiniButton);
			}
			SongPanelsManager.Singleton.SelectDifficulty(global::EQLIPSE.Chart.Difficulties.Easy);
			SetButtonSprite(Environment.Player.SelectedDifficulty);
			RefreshInfo();
		}
		public void OnButtonNormal()
		{
			if (!isExpanded)
			{
				OnPanelTouched();
			}
			else
			{
				SEPlayer.Singleton.Play(SEs.MiniButton);
			}
			SongPanelsManager.Singleton.SelectDifficulty(global::EQLIPSE.Chart.Difficulties.Normal);
			SetButtonSprite(Environment.Player.SelectedDifficulty);
			RefreshInfo();
		}
		public void OnButtonHard()
		{
			if (!isExpanded)
			{
				OnPanelTouched();
			}
			else
			{
				SEPlayer.Singleton.Play(SEs.MiniButton);
			}
			SongPanelsManager.Singleton.SelectDifficulty(global::EQLIPSE.Chart.Difficulties.Hard);
			SetButtonSprite(Environment.Player.SelectedDifficulty);
			RefreshInfo();
		}

		void SetButtonSprite(Chart.Difficulties? diff)
		{
			for (int i = 0; i < 3; i++)
			{
				DifficultyButtons[i].GetComponent<Image>().sprite = ButtonSprites[i];
				DifficultyButtons[i].GetComponent<Image>().color = Color.white;
			}
			if (diff != null)
			{
				DifficultyButtons[(int)diff].GetComponent<Image>().sprite = SelectedButton;
				DifficultyButtons[(int)diff].GetComponent<Image>().color = new Color(1, 1, 1, 0.625f);
			}
		}
		void RefreshInfo()
		{
			if (User.PlayDatas.Exists(r => r.Title == Song.Title && r.Difficulty == Environment.Player.SelectedDifficulty))
			{
				var highData = User.PlayDatas
					.Where(r => r.Title == Song.Title && r.Difficulty == Environment.Player.SelectedDifficulty)
					.FindMax(data => data.Score);

				TextHighScore.text = highData.Score.ToString();
				if (User.Records.Any(r => r.Title == Song.Title && r.Difficulty == Environment.Player.SelectedDifficulty))
				{
					var record =
						User.Records.First(r => r.Title == Song.Title && r.Difficulty == Environment.Player.SelectedDifficulty);
					TextRank.text = record.RankTuple.Item1 + " / " + record.RankTuple.Item2;
				}
				else
				{
					TextRank.text = "OFFLINE";
				}
			}
			else
			{
				TextHighScore.text = "NO PLAY";
				TextRank.text = "NO PLAY";
			}
			/*
			if (User.Records.Exists(r => r.Title == Song.Title && r.Difficulty == Environment.Player.SelectedDifficulty))
			{
				var record = User.Records.First(r => r.Title == Song.Title && r.Difficulty == Environment.Player.SelectedDifficulty);

				TextHighScore.text = record.Highscore.ToString();
				TextRank.text = record.RankTuple.Item1 + " / " + record.RankTuple.Item2;
			}
			else
			{
				TextHighScore.text = "NO PLAY";
				TextRank.text = "NO PLAY";
			}
			*/

			if (Environment.Selector.Infos.ContainsKey(new Tuple<string, Chart.Difficulties>(Song.Title,
					Environment.Player.SelectedDifficulty)) && Application.internetReachability != NetworkReachability.NotReachable)
			{
				var info =
					Environment.Selector.Infos[new Tuple<string, Chart.Difficulties>(Song.Title, Environment.Player.SelectedDifficulty)
					];

				foreach (var inst in _topRankerInstances)
				{
					Destroy(inst);
				}
				_topRankerInstances.Clear();

				int i = 0;
				foreach (var entity in info.TopRankers)
				{
					var go = GameObject.Instantiate(TopRankerPrefab, TopRankersParent);
					_topRankerInstances.Add(go);

					var c = go.GetComponent<TopRankerEntity>();
					c.Rank = ++i;
					c.Name = entity.Item1;
					c.Score = entity.Item2;
				}
			}
			else
			{
				TextClearRate.text = "OFFLINE";
			}

			SecretImage.color = !Environment.Player.IsChallenge && User.HasChartUnlock(Song.Title, Environment.Player.SelectedDifficulty)
				? Color.white
				: Color.clear;
		}

		void RefreshSelectables()
		{
			foreach (Chart.Difficulties difficulty in Enum.GetValues(typeof(Chart.Difficulties)))
			{
				int i = (int)difficulty;
				bool isInteractable = _song.LevelDictionary.ContainsKey(difficulty) && AreSelectable[i];
				if (isInteractable)
				{
					DifficultyButtons[i].interactable = true;
					LevelTexts[i].text = _song.LevelDictionary[difficulty].ToString();

					if (User.PlayDatas.Exists(r => r.Title == _song.Title && (int)r.Difficulty == i))
					{
						ClearedImage[i].color = Color.clear;

						if (User.PlayDatas.Any(d => d.Title == _song.Title && (int)d.Difficulty == i && d.IsFormerlyClear))
						{
							ClearedImage[i].sprite = ClearSprite;
							ClearedImage[i].color = Color.white;
						}
						if (User.PlayDatas.Any(d => d.Title == _song.Title && (int)d.Difficulty == i && d.IsFullCombo))
						{
							ClearedImage[i].sprite = FullComboSprite;
							ClearedImage[i].color = Color.white;
						}
					}
					else
					{
						ClearedImage[i].color = Color.clear;
					}
				}
				else
				{
					DifficultyButtons[i].interactable = false;
					LevelTexts[i].text = "-";
					ClearedImage[i].color = Color.clear;
				}

				DifficultyLabels[i].color = isInteractable ? Color.white : Color.clear;
				DifficultyButtons[i].GetComponent<CanvasGroup>().interactable = isInteractable;

				LevelTexts[i].color = isInteractable ? Color.white : Color.clear;
			}
		}

		public void In()
		{
			animator.SetTrigger("Panel In");
		}
		public void Out()
		{
			animator.SetTrigger("Panel Out");
		}
	}
}