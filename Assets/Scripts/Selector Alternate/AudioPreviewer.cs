﻿using UnityEngine;
using DG.Tweening;
using System.Collections;
using EQLIPSE;

namespace EQLIPSE.Selector
{
    [RequireComponent(typeof(AudioSource))]
    public class AudioPreviewer : SingletonMonoBehaviour<AudioPreviewer>
    {
        [SerializeField]
        AudioSource selectAudioSource, previewAudioSource;

        [SerializeField]
        AudioClip NormalBGM, ExtraBGM;

        bool isPreviewing;

        Tweener selectTweener, previewTweener;

        void Start()
        {
            isPreviewing = false;

            //if (Environment.Course.StageNo < 3)
            selectAudioSource.clip = NormalBGM;
            //else
            //    selectAudioSource.clip = ExtraBGM;

            selectAudioSource.Play();
            previewAudioSource.Stop();
        }

        public void PlayPreviewSound(AudioClip previewClip)
        {
            isPreviewing = true;

            if (selectTweener != null) selectTweener.Kill(false);
            if (previewTweener != null) previewTweener.Kill(false);

            previewAudioSource.clip = previewClip;
            previewAudioSource.Play();

            previewTweener = previewAudioSource.DOFade(1, 1);
            selectTweener = selectAudioSource.DOFade(0, 1);
        }
        public void StopPreviewSound()
        {
            if (isPreviewing)
            {
                Debug.Log("Stop Clip");

                if (selectTweener != null) selectTweener.Kill(false);
                if (previewTweener != null) previewTweener.Kill(false);

                previewTweener = previewAudioSource.DOFade(0, 0.5f);
                selectTweener = selectAudioSource.DOFade(1, 1.5f);
                isPreviewing = false;
            }
        }
    }
}