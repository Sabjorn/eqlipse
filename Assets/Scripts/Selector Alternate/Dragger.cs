﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using System.Collections;
using UniRx;

namespace EQLIPSE
{
    public class Dragger : SingletonMonoBehaviour<Dragger>, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        float vInertia;

        public ReactiveProperty<bool> IsDragging, IsInerting;

        Subject<float> deltaPositionSubject;
        public IObservable<float> DeltaPositionObservable
        {
            get
            {
                if (deltaPositionSubject == null) deltaPositionSubject = new Subject<float>();
                return deltaPositionSubject;
            }
        }

        public float MaxInerticVelocity;
        public float MinInerticVelocity;
        public float Friction;

        void Awake()
        {
            vInertia = 0;

            IsDragging = new ReactiveProperty<bool>();
            IsInerting = new ReactiveProperty<bool>();

            if (deltaPositionSubject == null) deltaPositionSubject = new Subject<float>();
        }

        void Update()
        {
            if (IsInerting.Value)
            {
                if (Mathf.Abs(vInertia) < MinInerticVelocity) { IsInerting.Value = false; vInertia = 0; }
                else
                {
                    deltaPositionSubject.OnNext(vInertia * Time.deltaTime);
                    vInertia += -Mathf.Sign(vInertia) * Friction * Time.deltaTime;
                }
            }
        }

        public void OnBeginDrag(PointerEventData e)
        {
            IsDragging.Value = true;
            IsInerting.Value = false;
            vInertia = 0;
        }
        public void OnDrag(PointerEventData e)
        {
            deltaPositionSubject.OnNext(e.delta.y / GetComponent<RectTransform>().rect.height);
        }
        public void OnEndDrag(PointerEventData e)
        {
            IsDragging.Value = false;

            IsInerting.Value = true;
            vInertia = Mathf.Clamp(e.delta.y / GetComponent<RectTransform>().rect.height / Time.deltaTime, -MaxInerticVelocity, MaxInerticVelocity);
        }
    }
}