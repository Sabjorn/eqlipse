﻿using UnityEngine;
using System.Collections;

namespace EQLIPSE.Result
{
    [RequireComponent(typeof(AudioSource))]
    public class ResultMusic : MonoBehaviour
    {
        [SerializeField]
        AudioClip ClearMusic, FailMusic;

        AudioSource audioSource;

        void Awake()
        {
            audioSource = GetComponent<AudioSource>();
        }

        void Start()
        {
            audioSource.clip = Environment.Player.PlayData.IsClear ? ClearMusic : FailMusic;
            audioSource.Play();
        }
    }
}