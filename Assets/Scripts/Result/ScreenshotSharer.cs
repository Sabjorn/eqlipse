﻿using System.Collections;
using System.Collections.Generic;
using EQLIPSE.Utility;
using Gamelogic.Extensions;
using UnityEngine;
using UniRx;
#if UNITY_EDITOR
using UnityEditor;
#endif
namespace EQLIPSE.Result
{
	public class ScreenshotSharer : MonoBehaviour
	{
		public void Share()
		{
			if (!User.Twitter.HasAccount)
			{
				Modal.InfoDialogObservable("Twitterアカウントが連携されていません。\n設定からアカウントを連携してください。").Subscribe();
				return;
			}

			ScreenshotHandler.Singleton.ScreenshotObservable
				.SelectMany(screenshot =>
				{
					return
						Modal.ShareDialogObservable(screenshot, HeaderString(Environment.Player.PlayData))
						.Where(msg => msg != null)
							.Select(customMessage => new Tuple<Texture2D, string>(screenshot, customMessage));
				})
				.SelectMany(tuple => Modal.YesNoDialogObservable("投稿しますか？").Where(b => b).Select(b => tuple))
				.SelectMany(tuple => ObservableEx.Using(Modal.LoadingLock("投稿中・・・"), User.Twitter.UploadStatusWithPictureObservable(HeaderString(Environment.Player.PlayData) + (string.IsNullOrEmpty(tuple.Item2) ? "" : "\n" + tuple.Item2) + "\n#EQLIPSE", System.Convert.ToBase64String(tuple.Item1.EncodeToPNG()))))
				.Subscribe(_ =>
				{
					Modal.InfoDialogObservable("投稿が完了しました。").Subscribe();
				});
		}

		public static string HeaderString(PlayData playData)
		{
			string ret = "";
			if (playData.IsFullCombo)
				ret = "FULL COMBO:";
			else if (playData.IsClear)
				ret = "CLEAR:";
			else ret = "FAILED:";
			return ret + playData.Title + " / " + playData.Difficulty.ToString()
				   + " - " + playData.Score.ToString("#,0") + " / " + playData.Rank.RankName();
		}
	}
}