﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UniRx;
using DG.Tweening;
using EQLIPSE.SuperScene;
using EQLIPSE.Utility;
using Newtonsoft.Json;

namespace EQLIPSE.Result
{
	public class ResultSequencer : MonoBehaviour
	{
		public RawImage Cover;
		public Text Title, Genre, Artist, Difficulty, Score, Combo, Brilliant, Great, Good, Miss;

		public Image Rank, ClearFailed, NewRecord;
		public RankScriptableObject RankSprites;
		public ClearFailedSprites ClearFailedSprites;
		public DifficultyColors DifficultyColors;

		// Use this for initialization
		void Start()
		{
			var result = Environment.Player.PlayData;
			var song = Environment.Player.SelectedSong;
			var chart = Environment.Player.LoadedChart;

			Cover.texture = song.Cover;
			Title.text = song.Title;
			Genre.text = song.Genre;
			Artist.text = song.Artist;
			Difficulty.text = chart.Difficulty + ": " + chart.Level;
			Difficulty.color = DifficultyColors.Colors[(int)chart.Difficulty];

			Score.text = result.Score.ToString("#,0");
			Combo.text = result.MaxCombo.ToString("#,0");

			Brilliant.text = (result.JudgeCounter[Judges.Perfect] + result.JudgeCounter[Judges.SlidePerfect]).ToString("#,0");
			Great.text = (result.JudgeCounter[Judges.Great] + result.JudgeCounter[Judges.SlideGreat]).ToString("#,0");
			Good.text = (result.JudgeCounter[Judges.Good] + result.JudgeCounter[Judges.SlideGood]).ToString("#,0");
			Miss.text = (result.JudgeCounter[Judges.Miss] + result.JudgeCounter[Judges.SlideMiss]).ToString("#,0");

			NewRecord.enabled = Environment.Result.IsNewRecord;

			if (result.IsClear)
			{
				if (result.IsFullCombo)
				{
					ClearFailed.sprite = ClearFailedSprites.Sprites[2];
				}
				else
				{
					ClearFailed.sprite = ClearFailedSprites.Sprites[0];
				}
			}
			else
			{
				ClearFailed.sprite = ClearFailedSprites.Sprites[1];
			}

			Rank.sprite = RankSprites.RankSprites[(int)result.Rank];
		}
		public void Next()
		{
			Observable.Defer(() =>
				{
					if (Environment.Result.UnlockedSongs.Any())
					{
						var newSongs = Environment.Result.UnlockedSongs.Where(e => e.Item2 == null);
						var upgSongs = Environment.Result.UnlockedSongs.Where(e => e.Item2 != null);

						var obs = Observable.ReturnUnit();
						if (upgSongs.Any())
						{
							Debug.Log("フリー解禁あり");
							foreach (var song in upgSongs.Select(t => Environment.Songs.First(s => s.Title == t.Item1)))
							{
								obs = obs.SelectMany(_ => Modal.SongDialogObservable(song, "フリー曲解禁！"));
							}
						}
						if (newSongs.Any())
						{
							Debug.Log("チャレンジ解禁あり");
							foreach (var song in newSongs.Select(t => Environment.Songs.First(s => s.Title == t.Item1)))
							{
								obs = obs.SelectMany(_ => Modal.SongDialogObservable(song, "チャレンジ曲解禁！"));
							}
						}
						return obs;
					}
					return Observable.ReturnUnit();
				})
				.Subscribe(_ =>
				{
					SceneTransitioner.Singleton.Transition = SuperSceneManager.Transitions.Default;
					SceneTransitioner.Singleton.Next();
				});
		}
	}
}