﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace EQLIPSE.Option
{
	public class RankingItemBehaviour : MonoBehaviour
	{
		public RawImage CoverImage;
		public Text[] UpperTexts, LowerTexts;
	}
}