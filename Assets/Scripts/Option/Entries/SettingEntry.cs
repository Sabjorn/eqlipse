﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using EQLIPSE.Utility;
using UniRx;

namespace EQLIPSE.Option
{
	public class SettingEntry : MonoBehaviour, IOptionEntry
	{
		[Header("判定調整器")]
		[SerializeField]
		private RectTransform Indicator;
		[SerializeField]
		private TapBehaviour Target;
		[SerializeField]
		private Text Value;
		[SerializeField]
		private Button Reset;

		[Header("マニュアル判定調整")]
		[SerializeField]
		private Text JudgeOffsetText;
		[SerializeField]
		private Button JudgeUp, JudgeDown;

		[SerializeField] private Toggle AdjustAudioToggle;
		[SerializeField] private GameObject AudioOffsetEntity;

		[SerializeField]
		private Text AudioOffsetText;
		[SerializeField]
		private Button AudioUp, AudioDown;

		[Header("レーンサイズ調整")]
		[SerializeField]
		private Text LaneSizeText;
		[SerializeField]
		private Button SizeUp, SizeDown;
		[SerializeField]
		private Image PlayerPreview;

		[Header("エフェクト簡素化")] [SerializeField] private Toggle EffectToggle;

		[Header("画面回転固定")] [SerializeField] private Dropdown OrientationDropdown;

		private CanvasGroup canvasGroup;

		private List<float> displayDeltas;

		private int ticks;

		void Awake()
		{
			canvasGroup = GetComponent<CanvasGroup>();

			displayDeltas = new List<float>();

			JudgeOffsetText.text = User.DisplayingJudgeOffset.ToString("+#;-#;±0");

			AdjustAudioToggle.isOn = User.AdjustAudio;
			AudioOffsetEntity.SetActive(User.AdjustAudio);

			AudioOffsetText.text = User.AudioOffset.ToString("+#;-#;±0");
			LaneSizeText.text = User.LaneSize.ToString("+#;-#;±0");
			Value.text = "±0.00";

			EffectToggle.isOn = User.SimpleEffect;

			OrientationDropdown.value = Screen.autorotateToLandscapeLeft && Screen.autorotateToLandscapeRight
				? 0
				: Screen.autorotateToLandscapeLeft
					? 1
					: Screen.autorotateToLandscapeRight
						? 2
						: 0;

			ticks = 0;
		}

		void Start()
		{
			Target.OnTouched.AsObservable()
				.Subscribe(_ =>
				{
					var delta = Time.time - (Mathf.Round(Time.time));
					displayDeltas.Add(delta);

					Value.text = (displayDeltas.Average() * 100f - 6).ToString("+#0.00;-#0.00;±0.00");
				});
			Reset.OnClickAsObservable()
				.Subscribe(_ =>
				{
					displayDeltas.Clear();
					Value.text = (0f).ToString("+#0.00;-#0.00;±0.00");
				});

			JudgeUp.OnClickAsObservable()
				.Subscribe(_ =>
				{
					User.JudgeOffset++;
					JudgeOffsetText.text = User.DisplayingJudgeOffset.ToString("+#;-#;±0");
				});
			JudgeDown.OnClickAsObservable()
				.Subscribe(_ =>
				{
					User.JudgeOffset--;
					JudgeOffsetText.text = User.DisplayingJudgeOffset.ToString("+#;-#;±0");
				});

			AdjustAudioToggle.OnValueChangedAsObservable()
				.Subscribe(b =>
				{
					User.AdjustAudio = b;
					AudioOffsetEntity.SetActive(b);
				});

			AudioUp.OnClickAsObservable()
				.Subscribe(_ =>
				{
					User.AudioOffset++;
					AudioOffsetText.text = User.AudioOffset.ToString("+#;-#;±0");
				});
			AudioDown.OnClickAsObservable()
				.Subscribe(_ =>
				{
					User.AudioOffset--;
					AudioOffsetText.text = User.AudioOffset.ToString("+#;-#;±0");
				});

			SizeUp.OnClickAsObservable()
				.Subscribe(_ =>
					{
						User.LaneSize++;
						LaneSizeText.text = User.LaneSize.ToString("+#;-#;±0");
					});
			SizeDown.OnClickAsObservable()
				.Subscribe(_ =>
					{
						User.LaneSize--;
						LaneSizeText.text = User.LaneSize.ToString("+#;-#;±0");
					});

			SizeUp.OnClickAsObservable().Merge(SizeDown.OnClickAsObservable())
				.Do(_ =>
				{
					PlayerPreview.transform.localScale = Vector3.one * Mathf.Pow(1.1f, User.LaneSize);
					PlayerPreview.color = new Color(1, 1, 1, 0.25f);
				}).Throttle(TimeSpan.FromSeconds(5))
				.Do(_ =>
				{
					PlayerPreview.color = Color.clear;
				}).Subscribe();

			EffectToggle.OnValueChangedAsObservable().Subscribe(b =>
			{
				User.SimpleEffect = b;
			});

			OrientationDropdown.onValueChanged.AsObservable()
				.Subscribe(selection =>
				{
					switch (selection)
					{
						case 0:
							Screen.autorotateToLandscapeRight = true;
							Screen.autorotateToLandscapeLeft = true;
							break;
						case 1:
							Screen.autorotateToLandscapeRight = false;
							Screen.autorotateToLandscapeLeft = true;
							Screen.orientation= ScreenOrientation.LandscapeLeft;
							break;
						case 2:
							Screen.autorotateToLandscapeRight = true;
							Screen.autorotateToLandscapeLeft = false;
							Screen.orientation = ScreenOrientation.LandscapeRight;
							break;
					}
				});
		}

		void Update()
		{
			var aMin = Indicator.anchorMin;
			var aMax = Indicator.anchorMax;

			aMin.y = -((Time.time + 0.5f) % 1f) + 1;
			aMax.y = -((Time.time + 0.5f) % 1f) + 1;

			Indicator.anchorMin = aMin;
			Indicator.anchorMax = aMax;
		}

		public void Show()
		{
			canvasGroup.blocksRaycasts = true;
			canvasGroup.DOFade(1, 0.5f);
		}

		public void Hide()
		{
			canvasGroup.blocksRaycasts = false;
			canvasGroup.DOFade(0, 0.5f);
		}
	}
}