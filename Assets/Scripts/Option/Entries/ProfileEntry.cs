﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design;
using EQLIPSE.Utility;
using UnityEngine;
using System.Linq;
using DG.Tweening;
using EQLIPSE.SuperScene;
using UniRx;
using UnityEngine.UI;

namespace EQLIPSE.Option
{
	public class ProfileEntry : MonoBehaviour, IOptionEntry
	{
		[SerializeField]
		private Text Name, Rate;

		[SerializeField]
		private Button EditNameButton;
		[SerializeField]
		private Button DeletionButton;

		[SerializeField]
		private GameObject ItemPrefab;
		[SerializeField]
		private Transform RankingTransform;

		private CanvasGroup canvasGroup;

		private List<GameObject> ItemList;

		void Awake()
		{
			canvasGroup = GetComponent<CanvasGroup>();
		}

		// Use this for initialization
		void Start()
		{
			ItemList = new List<GameObject>();
			foreach (var group in User.PlayDatas.GroupBy(r => r.Title))
			{
				if (Environment.Songs.Any(s => s.Title == group.Key))
				{
					var group1 = group;
					var highScores = ((Chart.Difficulties[])Enum.GetValues(typeof(Chart.Difficulties)))
						.Select(difficulty => group1.Where(data => data.Difficulty == difficulty).FindMax(data => data.Score));

					var g = Pool.Instantiate(ItemPrefab);
					g.transform.SetParent(RankingTransform, false);
					ItemList.Add(g);

					var component = g.GetComponent<RankingItemBehaviour>();

					var song = Environment.Songs.First(s => s.Title == group.Key);
					component.CoverImage.texture = song.Cover;

					for (int i = 0; i < 3; i++)
					{
						component.UpperTexts[i].text = "-";
						component.LowerTexts[i].text = "-";
					}

					foreach (var highScore in highScores)
					{
						if (highScore != null)
						{
							component.UpperTexts[(int)highScore.Difficulty].text = highScore.Score.ToString("#,0");
							if (User.Records.Any(r => r.Title == highScore.Title && r.Difficulty == highScore.Difficulty))
							{
								var record = User.Records.First(r => r.Title == highScore.Title && r.Difficulty == highScore.Difficulty);
								component.LowerTexts[(int)highScore.Difficulty].text = record.RankTuple.Item1 + "位";
							}
						}
					}
				}
			}

			Refresh();

			string oldName = "";
			EditNameButton.OnClickAsObservable()
				.Do(_ => { oldName = User.Name; })
				.SelectMany(_ => Modal.YesNoDialogObservable("名前を変更しますか？"))
				.Where(selection => selection)
				.SelectMany(_ => Modal.InputDialogObservable("新しい名前を入力してください。", InputField.ContentType.Standard, 20))
				.SelectMany(newName => Modal.YesNoDialogObservable("「" + newName + "」でよろしいですか？")
					.Select(selection => new Tuple<string, bool>(newName, selection)))
				.Where(tuple => tuple.Item2)
				.SelectMany(tuple => ObservableEx.Using(Modal.LoadingLock(),
					Observable.Defer(() =>
						{
							User.Name = tuple.Item1;
							User.EnqueueUserData();
							return WWWPostQueue.TryPostObservable();
						})
						.Do(_ =>
						{
							User.Save();
							Refresh();

							AccountBar.Singleton.Refresh().Subscribe();
						})
				))
				.Subscribe(_ =>
				{
					Modal.InfoDialogObservable("名前の変更が完了しました。").Subscribe();
				}, e =>
				{
					User.Name = oldName;
					Modal.InfoDialogObservable("名前の変更に失敗しました。変更は元に戻ります。").Subscribe();
				});

			DeletionButton.OnClickAsObservable()
				.SelectMany(_ => Modal.YesNoDialogObservable("端末上・サーバー上のすべてのデータを消去します。よろしいですか？"))
				.Where(b => b)
				.SelectMany(_ =>
				{
					if (Application.internetReachability == NetworkReachability.NotReachable)
						return Modal.YesNoDialogObservable("インターネットに接続されていません。このまま消去するとサーバーにデータが残る可能性があります。よろしいですか？");
					else return Observable.Return(true);
				})
				.Where(b => b)
				.SelectMany(_ => Modal.YesNoDialogObservable("本当によろしいですか？"))
				.Where(b => b)
				.SelectMany(_ =>
				{
					return ObservableWWW.Get(Environment.WwwApiHost + "/users/delete/" + User.Uid)
					.DoOnError(ex =>
						{
							Modal.InfoDialogObservable("サーバー上のデータの消去に失敗しました。").Subscribe();
						})
					.AsUnitObservable();
				})
				.Do(_ =>
				{
					User.Clear();
					User.Save();

					WWWPostQueue.Clear();
					WWWPostQueue.Save();

					AccountBar.Singleton.Show(false);
				})
				.SelectMany(_ => Modal.InfoDialogObservable("データの消去が完了しました。"))
				.Subscribe(_ =>
				{
					SuperSceneManager.Singleton.NextState(SuperSceneManager.Transitions.Default);
				});
		}

		void Refresh()
		{
			Name.text = User.Name;
			Rate.text = User.Rate.ToString("##0.000");
		}

		public void Show()
		{
			canvasGroup.blocksRaycasts = true;
			canvasGroup.DOFade(1, 0.5f);
		}

		public void Hide()
		{
			canvasGroup.blocksRaycasts = false;
			canvasGroup.DOFade(0, 0.5f);
		}
	}
}