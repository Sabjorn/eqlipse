﻿using System.Collections;
using System.Collections.Generic;
using EQLIPSE.Utility;
using UnityEngine;
using System.Linq;
using DG.Tweening;
using EQLIPSE.SuperScene;
using Newtonsoft.Json.Linq;
using UniRx;
using UniRx.Diagnostics;
using UnityEngine.UI;

namespace EQLIPSE.Option
{
	public class AccountEntry : MonoBehaviour, IOptionEntry
	{
		[SerializeField]
		private Button TwitterButton;
		[SerializeField]
		private Text IntegrationStatusText;

		private CanvasGroup canvasGroup;

		void Awake()
		{
			canvasGroup = GetComponent<CanvasGroup>();

			TwitterButton.OnClickAsObservable()
				.Where(_ => Application.internetReachability != NetworkReachability.NotReachable)
				.SelectMany(Observable.FromCoroutine(User.Twitter.LinkWizard))
				.Subscribe(_ =>
				{
					User.Save();
					AccountBar.Singleton.Refresh(false);
					RefreshTwitter();
					Modal.InfoDialogObservable("連携が完了しました。").Subscribe();
				}, exception =>
				{
					var wwwException = exception as WWWErrorException;
					if (wwwException != null)
					{
						Modal.InfoDialogObservable("WWWエラー: " + wwwException.StatusCode + "\n" + wwwException.Text).Subscribe();
					}
					else
					{
						Modal.InfoDialogObservable("エラー\n" + exception.Message).Subscribe();
					}
				});
			TwitterButton.OnClickAsObservable()
				.Where(_ => Application.internetReachability == NetworkReachability.NotReachable)
				.Subscribe(_ =>
				{
					Modal.InfoDialogObservable("インターネットに接続されていません。").Subscribe();
				});

			RefreshTwitter();
		}

		void RefreshTwitter()
		{
			if (Application.internetReachability != NetworkReachability.NotReachable)
			{
				if (User.Twitter.HasAccount)
					User.Twitter.FetchProfileObservable().Subscribe(_ =>
					{
						User.Save();
						IntegrationStatusText.text = "@" + User.Twitter.Name;
					});
				else
					IntegrationStatusText.text = "未連携";
			}
			else
			{
				IntegrationStatusText.text = "オフライン";
			}
		}

		public void Show()
		{
			canvasGroup.blocksRaycasts = true;
			canvasGroup.DOFade(1, 0.5f);
		}

		public void Hide()
		{
			canvasGroup.blocksRaycasts = false;
			canvasGroup.DOFade(0, 0.5f);
		}
	}
}