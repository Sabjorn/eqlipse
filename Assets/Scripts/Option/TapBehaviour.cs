﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace EQLIPSE.Option
{
	public class TapBehaviour : MonoBehaviour,IPointerDownHandler
	{
		[HideInInspector]
		public UnityEvent OnTouched;
		public void OnPointerDown(PointerEventData eventData)
		{
			OnTouched.Invoke();
		}
	}
}