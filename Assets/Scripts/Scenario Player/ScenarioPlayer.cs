﻿using UnityEngine;
using System.Collections;
using EQLIPSE;
using EQLIPSE.SuperScene;

public class ScenarioPlayer : MonoBehaviour
{
    public void Play()
    {
        StartCoroutine(PlayCoroutine());
    }

    IEnumerator PlayCoroutine()
    {
        yield return SceneDealer.Singleton.AddScene("conversation");
        ScenarioMessageBehaviour.Singleton.OnEnd.AddListener(() => { SceneTransitioner.Singleton.Next(); });
    }
}
