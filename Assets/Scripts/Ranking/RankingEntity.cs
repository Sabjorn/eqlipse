﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace EQLIPSE.Ranking
{
	public class RankingEntity : MonoBehaviour
	{
		[SerializeField]
		Text RankText, NameText, RateText;

		[SerializeField] private Color SelfColor;

		private int _rank;
		private string _name;
		private string _number;
		private string _uid;

		public string Uid
		{
			get { return _uid; }
			set
			{
				if (User.Uid == value)
				{
					NameText.color = SelfColor;
					RateText.color = SelfColor;
				}
				_uid = value;
			}
		}

		public int Rank
		{
			get { return _rank; }
			set
			{
				RankText.text = value + ".";
				_rank = value;
			}
		}

		public string Name
		{
			get { return _name; }
			set
			{
				NameText.text = value;
				_name = value;
			}
		}

		public string Number
		{
			get { return _number; }
			set
			{
				RateText.text = value;
				_number = value;
			}
		}

		void Start()
		{
			var canvasGroup = GetComponent<CanvasGroup>();
			canvasGroup.alpha = 0;
			canvasGroup.DOFade(1, 0.3f);
		}
	}
}