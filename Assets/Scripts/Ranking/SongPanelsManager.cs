﻿using System.Collections;
using System.Collections.Generic;
using EQLIPSE.SuperScene;
using UniRx;
using UnityEngine;

namespace EQLIPSE.Ranking
{
	public class SongPanelsManager : MonoBehaviour
	{
		[SerializeField] private RectTransform PanelParent;
		[SerializeField] private GameObject SongPrefab;

		private List<GameObject> _panelObjects;

		void Start()
		{
			_panelObjects = new List<GameObject>();

			foreach (var entity in Environment.FreeAvailableSongs)
			{
				var g = Instantiate(SongPrefab, PanelParent);
				var c = g.GetComponent<SongElement>();

				c.Song = entity.Key;
				c.Availabilities = entity.Value;

				for (int i = 0; i < 3; i++)
				{
					var i1 = i;
					c.Buttons[i].OnClickAsObservable()
						.Subscribe(_ =>
						{
							SEPlayer.Singleton.Play(SEs.Decide);
							RankingBehaviour.Singleton.ShowChartRanking(c.Song.Title, (Chart.Difficulties)i1);
						}).AddTo(g);
				}
			}
		}
	}
}