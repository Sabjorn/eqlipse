﻿using System.Collections;
using System.Collections.Generic;
using EQLIPSE.Utility;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

namespace EQLIPSE.Ranking
{
	public class RankingWatcher : MonoBehaviour
	{
		void Start()
		{
			this.UpdateAsObservable()
				.Select(_ => Application.internetReachability)
				.Where(r => r == NetworkReachability.NotReachable)
				.First()
				.Subscribe(_ =>
				{
					Modal.InfoDialogObservable("インターネット接続が切断されました。メインメニューに戻ります。")
						.Subscribe(__ =>
						{
							SceneTransitioner.Singleton.Back();
						});
				});
		}
	}
}