﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace EQLIPSE.Ranking
{
	public class RankingBehaviour : SingletonMonoBehaviour<RankingBehaviour>
	{
		[SerializeField] private CanvasGroup MainCanvas, RankingCanvas;
		[SerializeField] private Button RateButton, FCButton;
		[SerializeField] private Text RankingText;

		[SerializeField] private RankingFetcher Fetcher;

		private bool isShowingRanking = false;

		void Start()
		{
			MainCanvas.alpha = 1;
			MainCanvas.blocksRaycasts = true;

			RankingCanvas.alpha = 0;
			RankingCanvas.blocksRaycasts = false;

			RateButton.OnClickAsObservable()
				.Subscribe(_ =>
				{
					RankingText.text = "レートランキング";
					SwitchScreen(true);
					Fetcher.FetchRateRanking();
				});
			FCButton.OnClickAsObservable()
				.Subscribe(_ =>
				{
					RankingText.text = "フルコンボランキング";
					SwitchScreen(true);
					Fetcher.FetchFcRanking();
				});

			isShowingRanking = false;
		}

		public void SwitchScreen(bool mode)
		{
			if (!mode)
			{
				MainCanvas.blocksRaycasts = true;
				RankingCanvas.blocksRaycasts = false;

				MainCanvas.DOFade(1f, 0.2f);
				RankingCanvas.DOFade(0f, 0.2f);
			}
			else
			{
				MainCanvas.blocksRaycasts = false;
				RankingCanvas.blocksRaycasts = true;

				MainCanvas.DOFade(0f, 0.2f);
				RankingCanvas.DOFade(1f, 0.2f);
			}

			isShowingRanking = mode;
		}

		public void ShowChartRanking(string title, Chart.Difficulties difficulty)
		{
			RankingText.text = title + " / " + difficulty;
			Singleton.SwitchScreen(true);
			Fetcher.FetchChartRanking(title, difficulty);
		}

		public void OnBack()
		{
			if (isShowingRanking) SwitchScreen(false);
			else SceneTransitioner.Singleton.Back();
		}
	}
}