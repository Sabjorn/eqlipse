﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace EQLIPSE.Ranking
{
	public class SongElement : MonoBehaviour
	{
		private Song song;

		[SerializeField] private RawImage CoverImage;
		[SerializeField] private Text TitleText;
		[SerializeField] public Button[] Buttons;

		public Song Song
		{
			get
			{
				return song;
			}

			set
			{
				song = value;

				CoverImage.texture = song.Cover;
				TitleText.text = song.Title;

				for (int i = 0; i < 3; i++)
				{
					if (song.LevelDictionary.ContainsKey((Chart.Difficulties) i))
					{
						Buttons[i].GetComponentInChildren<Text>().text = song.LevelDictionary[(Chart.Difficulties) i].ToString();
					}
					else
					{
						Buttons[i].GetComponentInChildren<Text>().text = "";
						Buttons[i].interactable = false;
					}
				}
			}
		}

		public bool[] Availabilities
		{
			set
			{
				for (var i = 0; i < 3; i++)
				{

					if (song.LevelDictionary.ContainsKey((Chart.Difficulties) i))
					{
						Buttons[i].interactable = value[i];
						if (!Buttons[i].interactable) Buttons[i].GetComponentInChildren<Text>().text = "";
					}
					else
					{
						Buttons[i].GetComponentInChildren<Text>().text = "";
						Buttons[i].interactable = false;
					}
				}
			}
		}
	}
}