﻿using System;
using System.Collections;
using System.Collections.Generic;
using EQLIPSE.Utility;
using UnityEngine;
using UniRx;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace EQLIPSE.Ranking
{
	public class RankingFetcher : SingletonMonoBehaviour<RankingFetcher>
	{
		public GameObject EntityPrefab;
		public GameObject NoPlayPrefab;

		public RectTransform ContentTransform, SelfEntityTransform;

		private List<GameObject> _entities;

		void Awake()
		{
			_entities = new List<GameObject>();
		}

		public void FetchRateRanking()
		{
			foreach (var go in _entities)
			{
				Destroy(go);
			}
			_entities.Clear();

			ObservableEx.Using(Modal.LoadingLock(),
					ObservableWWW.Get(Environment.WwwApiHost + "/users/ranking/" + User.Uid))
				.Select(JToken.Parse)
				.Subscribe(response =>
				{
					Func<JToken, GameObject> f = e =>
					 {
						 var go = Instantiate(EntityPrefab, ContentTransform);
						 var c = go.GetComponent<RankingEntity>();
						 c.Rank = (int)e["rank"] + 1;
						 c.Name = e["name"].ToString();
						 c.Number = ((float)e["rate"]).ToString("#0.000");
						 c.Uid = e["uid"].ToString();
						 _entities.Add(go);
						 return go;
					 };

					foreach (var e in response["ranking"])
						f(e);
					if (response["self"] != null) f(response["self"]).transform.SetParent(SelfEntityTransform);
				});
		}

		public void FetchFcRanking()
		{
			foreach (var go in _entities)
			{
				Destroy(go);
			}
			_entities.Clear();

			ObservableEx.Using(Modal.LoadingLock(),
					ObservableWWW.Get(Environment.WwwApiHost + "/users/ranking/" + User.Uid + "?type=full_combo"))
				.Select(JToken.Parse)
				.Subscribe(response =>
				{
					Func<JToken, GameObject> f = e =>
					{
						var go = Instantiate(EntityPrefab, ContentTransform);
						var c = go.GetComponent<RankingEntity>();
						c.Rank = (int)e["rank"] + 1;
						c.Name = e["name"].ToString();
						c.Number = e["full_combo"].ToString();
						c.Uid = e["uid"].ToString();
						_entities.Add(go);
						return go;
					};

					foreach (var e in response["ranking"])
						f(e);
					if (response["self"] != null)
						f(response["self"]).transform.SetParent(SelfEntityTransform);
					else
					{
						_entities.Add(Instantiate(NoPlayPrefab, SelfEntityTransform));
					}
				});
		}

		public void FetchChartRanking(string title, Chart.Difficulties difficulty)
		{
			foreach (var go in _entities)
			{
				Destroy(go);
			}
			_entities.Clear();

			ObservableEx.Using(Modal.LoadingLock(),
					ObservableWWW.Get(Environment.WwwApiHost + "/users/ranking/" + User.Uid + "?type=chart&title=" + WWW.EscapeURL(title) +
									  "&difficulty=" + (int)difficulty))
				.Do(Debug.Log)
				.Select(JToken.Parse)
				.Subscribe(response =>
				{
					Func<JToken, GameObject> f = e =>
					{
						var go = Instantiate(EntityPrefab, ContentTransform);
						var c = go.GetComponent<RankingEntity>();
						c.Rank = (int)e["rank"] + 1;
						c.Name = e["name"].ToString();
						c.Number = ((int)e["high_score"]).ToString("#,0");
						c.Uid = e["uid"].ToString();
						_entities.Add(go);
						return go;
					};

					foreach (var e in response["ranking"])
					{
						f(e);
					}
					Debug.Log(string.IsNullOrEmpty(response["self"].ToString()));
					if (response["self"].HasValues) f(response["self"]).transform.SetParent(SelfEntityTransform);
					else
					{
						_entities.Add(Instantiate(NoPlayPrefab, SelfEntityTransform));
					}
				});
		}
	}
}