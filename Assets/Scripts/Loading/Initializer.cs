﻿using System.Collections;
using System.Collections.Generic;
using EQLIPSE.Utility;
using UniRx;
using UnityEngine;
using UnityEngine.Events;

namespace EQLIPSE.Loader
{
    public class Initializer : MonoBehaviour
    {
        public UnityEvent OnInitialized;

        public void Initialize()
        {
            StartCoroutine(InitializationCoroutine());
        }

        IEnumerator InitializationCoroutine()
        {
            yield return Environment.LoadSongs();
            yield return Environment.Story.LoadScenarios();
            yield return Environment.Story.LoadFacialImages();
            yield return Environment.Story.LoadBGImages();
	        yield return Environment.MainMenu.LoadMessages().StartAsCoroutine();
			WWWPostQueue.Load();

            OnInitialized.Invoke();
        }
    }
}