﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace EQLIPSE.MainMenu
{
	public class MainMenuWatcher : MonoBehaviour
	{
		public Button RankingButton;

		void Update()
		{
			switch (Application.internetReachability)
			{
				case NetworkReachability.NotReachable:
					RankingButton.interactable = false;
					RankingButton.GetComponent<Image>().color = new Color(1, 1, 1, 0.5f);
					break;
				default:
					RankingButton.interactable = true;
					RankingButton.GetComponent<Image>().color = Color.white;
					break;
			}
		}
	}
}