﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = System.Random;

namespace EQLIPSE.MainMenu
{
	public class MessageRandomizer : MonoBehaviour
	{
		[SerializeField]
		private Text MessageText;

		private Random random;

		void Start()
		{
			random = new Random();
			Randomize();
		}

		void Randomize()
		{
			MessageText.text = Environment.MainMenu.Messages[random.Next(Environment.MainMenu.Messages.Count)];
		}
	}
}