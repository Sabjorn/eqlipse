﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using System.Collections;
using EQLIPSE.SuperScene;
using EQLIPSE.Utility;
using UniRx;

namespace EQLIPSE
{
	public class SceneTransitioner : SingletonMonoBehaviour<SceneTransitioner>
	{
		public SuperSceneManager.Transitions Transition = SuperSceneManager.Transitions.Default;

		public UnityEvent OnTransitionEnded;

		public int Parameter
		{
			set
			{
				SuperSceneManager.Singleton.Parameter = value;
			}
		}

		public void Next()
		{
			SuperSceneManager.Singleton.Direction = SuperSceneManager.Directions.Next;
			SuperSceneManager.Singleton.NextState(Transition);
		}

		public void Back()
		{
			SuperSceneManager.Singleton.Direction = SuperSceneManager.Directions.Back;
			SuperSceneManager.Singleton.NextState(Transition);
		}

		public void ConfirmBack(string message)
		{
			Modal.YesNoDialogObservable(message)
				.Subscribe(yn =>
				{
					if (yn) Back();
				});
		}


		public void TransitScene(string scene)
		{
			SceneDealer.Singleton.StartCoroutine(SceneDealer.Singleton.LoadScene(scene));
		}
	}
}