﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using EQLIPSE.SuperScene;
using EQLIPSE.Utility;
using Newtonsoft.Json.Linq;
using UnityEngine;
using UniRx;
using UnityEngine.UI;
using UniRx;

namespace EQLIPSE.Title
{
	public class InitializeBehaviour : MonoBehaviour
	{
		[SerializeField] private InputField UidField;
		[SerializeField] private Button UidButton;

		void Start()
		{
			UidButton.OnClickAsObservable()
				.Subscribe(_ =>
				{
					if (Application.internetReachability != NetworkReachability.NotReachable)
					{
						User.Uid = UidField.text;
						ObservableEx.Using(Modal.LoadingLock(),
								User.FetchUserDataObservable()
									.SelectMany(__ => User.FetchPlayDataObservable())
									.SelectMany(WWWPostQueue.TryPostObservable())
							)
							.Subscribe(user =>
							{
								SuperSceneManager.Singleton.Parameter = 0;
								User.Save();
								SceneTransitioner.Singleton.Next();
							}, e =>
							{
								var wwwException = e as WWWErrorException;
								Modal.InfoDialogObservable("存在しないUIDです。:\n" + wwwException).Subscribe();
							});
					}
					else
					{
						Modal.InfoDialogObservable("インターネットに接続されていません。").Subscribe();
					}
				});
		}

		public void OnTouched()
		{
			// ローカルデータから読み込み
			User.Load();

			if (string.IsNullOrEmpty(User.Uid))
			{
				// 初回プレイ

				if (Application.internetReachability == NetworkReachability.NotReachable)
				{
					Modal.InfoDialogObservable("初回起動時にはインターネット接続が必要です。接続を確認してからもう一度試してください。").Subscribe();
				}
				else
				{
					Observable.FromCoroutine(NewUserEnumerator).Subscribe();
				}
			}
			else
			{
				// 既プレイ

				if (Application.internetReachability != NetworkReachability.NotReachable)
				{
					ObservableEx.Using(Modal.LoadingLock(),
							Observable.ReturnUnit()
								.SelectMany(__ =>
								{
									if (User.PlayDatas.Count == 0)
										return User.FetchPlayDataObservable(); // マージするべき
									return Observable.ReturnUnit();
								})
								.SelectMany(_ =>
								{
									User.EnqueueUserData();
									return WWWPostQueue.TryPostObservable();
								})
						)
						.Subscribe(user =>
						{
							SuperSceneManager.Singleton.Parameter = 0;
							SceneTransitioner.Singleton.Next();
						}, e =>
						{
							Modal.YesNoDialogObservable("ユーザーが見つかりませんでした。新規ユーザーとして開始しますか？")
								.Where(sel => sel)
								.SelectMany(Observable.FromCoroutine(NewUserEnumerator))
								.Subscribe();
						});
				}
				else
				{
					SuperSceneManager.Singleton.Parameter = 0;
					SceneTransitioner.Singleton.Next();
				}
			}
		}

		public IEnumerator NewUserEnumerator()
		{
			var questionInst = Modal.YesNoDialogObservable("Twitter連携済みのEQLIPSEアカウントをお持ちですか？").ToYieldInstruction();
			yield return questionInst;

			if (!questionInst.Result)
			{
				// Twitterなし、新規ユーザー登録

				// 名前入力
				var nameInst =
					Modal.InputDialogObservable("ニックネームを入力してください。", InputField.ContentType.Standard, 20).ToYieldInstruction();
				yield return nameInst;

				if (string.IsNullOrEmpty(nameInst.Result))
				{
					Modal.InfoDialogObservable("名前が入力されていません。").Subscribe();
					yield break;
				}

				// 新規ユーザーデータ送信
				var form = new WWWForm();
				form.AddField("name", nameInst.Result);

				var registerInst = ObservableEx.Using(
						Modal.LoadingLock("ユーザー登録中・・・"),
						ObservableWWW.Post(Environment.WwwApiHost + "/users/register", form))
					.ToYieldInstruction();
				yield return registerInst;

				var jResponse = JToken.Parse(registerInst.Result);

				// ローカルユーザーデータ上書き
				User.Clear();
				User.ParseJToken(jResponse);
				User.Save();

				yield return Modal.InfoDialogObservable("登録が完了しました。").ToYieldInstruction();

				SuperSceneManager.Singleton.Parameter = 1;
				SceneTransitioner.Singleton.Next();
			}
			else
			{
				// Twitterあり、データ引き継ぎ

				// Twitterログイン、アクセストークン・シークレット取得
				var inst = Observable.FromCoroutine<Unit>(User.Twitter.FetchAccessTokenWizard).ToYieldInstruction();
				yield return inst;
				if (inst.HasError)
				{
					Modal.InfoDialogObservable("エラーが発生しました。\n" + inst.Error);
				}

				using (Modal.LoadingLock("通信中・・・"))
				{
					// Twitterプロフィール取得
					yield return User.Twitter.FetchProfileObservable().ToYieldInstruction();

					// TwitterのIDからユーザーデータ取得
					inst = User.Twitter.FetchUserDataObservable().ToYieldInstruction();
					yield return inst;
					if (inst.HasError)
					{
						Modal.InfoDialogObservable("Twitterアカウントが紐付けられたEQLIPSEアカウントが存在しません。")
							.Subscribe();
						yield break;
					}

					User.Save();
				}
				using (Modal.LoadingLock("プレイデータをダウンロード中・・・"))
				{
					yield return User.FetchPlayDataObservable().ToYieldInstruction();
				}

				yield return Modal.InfoDialogObservable("引き継ぎが完了しました。").ToYieldInstruction();

				SuperSceneManager.Singleton.Parameter = 0;
				SceneTransitioner.Singleton.Next();
			}
		}
	}
}