﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace EQLIPSE.Player
{
    public class ComboscreenScript : MonoBehaviour
    {
        public int Combos;
	
        Text uiText;

        Vector3 scale;
        Color color;

        // Use this for initialization
        void Start()
        {
            uiText = GetComponent<Text>();
        }

        public void Combo(int c)
        {
            Combos = c;
            if (c == 0)
            {
                uiText.color = new Color(0, 0, 0, 0);
            }
            else
            {
                uiText.text = "" + c + " Com bo";
            }
        }
    }
}