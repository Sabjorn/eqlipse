﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;

namespace EQLIPSE.Player
{
    public class JudgeStatics : SingletonMonoBehaviour<JudgeStatics>
    {
        List<float> deltas;

        float average;
        public float Average { get { return average; } }

        float stDeviation;
        public float StandardDeviation { get { return stDeviation; } }

        void Start()
        {
            average = 0; stDeviation = 0;
            deltas = new List<float>();

            SongJudger.Singleton.JudgeObservable
                .Subscribe(judge =>
                {
                    if (judge.DeltaTime != 0) deltas.Add(judge.DeltaTime);
                }).AddTo(gameObject);

            Observable.Interval(new System.TimeSpan(0, 0, 0, 0, 500))
                .Subscribe(_ =>
                {
                    if (deltas.Count > 0)
                    {
                        average = deltas.Average();
                        stDeviation = Mathf.Sqrt((float)deltas.Variance());
                    }
                }).AddTo(gameObject);
        }
    }
}