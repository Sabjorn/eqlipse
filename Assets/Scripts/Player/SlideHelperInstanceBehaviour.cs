﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using DG.Tweening;
using EQLIPSE;
using EQLIPSE.Utility;

namespace EQLIPSE.Player
{
	public class SlideHelperInstanceBehaviour : MonoBehaviour, IPoolant
	{
		Animator animator;

		public float Duration
		{
			set
			{
				animator.speed = 1 / value;
			}
		}

		public void OnPooled()
		{
			animator = GetComponent<Animator>();
			animator.SetTrigger("Initialize");
		}

		public void OnReleased() { }

		public void Finish()
		{
			animator.speed = 1;
			animator.SetTrigger("Disappear");
		}
	}
}