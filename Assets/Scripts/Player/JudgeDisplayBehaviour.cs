﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using EQLIPSE;
using EQLIPSE.Player;
using UniRx;

[RequireComponent(typeof(Animator))]
public class JudgeDisplayBehaviour : MonoBehaviour
{
	Animator animator;

	public Image ImageJudge;
	public Text TextCombo;
	public Text TextFastSlow;

	public Sprite[] SpriteJudges;

	public Color[] ColorFastSlow;

	void Awake()
	{
		animator = GetComponent<Animator>();
	}

	void Start()
	{
		SongJudger.Singleton.JudgeObservable
			.Subscribe(judgeInfo =>
			{
				int i = 0;
				switch (judgeInfo.Judge)
				{
					case Judges.Perfect: i = 0; break;
					case Judges.Great: i = 1; break;
					case Judges.Good: i = 2; break;
					case Judges.Miss: i = 3; break;
					case Judges.SlidePerfect: i = 0; break;
					case Judges.SlideGreat: i = 1; break;
					case Judges.SlideGood: i = 2; break;
					case Judges.SlideMiss: i = 3; break;
				}
				ImageJudge.sprite = SpriteJudges[i];

				TextCombo.text = ComboManager.Singleton.Combo > 0 ? "" + ComboManager.Singleton.Combo + " combo!" : "";

				if (judgeInfo.Judge == Judges.Perfect || judgeInfo.Judge == Judges.Miss || judgeInfo.Judge == Judges.SlidePerfect || judgeInfo.Judge == Judges.SlideMiss || judgeInfo.NoteType == JudgeInfo.NoteTypes.Slide)
				{
					TextFastSlow.text = "";
					TextFastSlow.color = Color.white;
				}
				else
				{
					if (judgeInfo.DeltaTime < 0)
					{
						TextFastSlow.text = "FAST";
						TextFastSlow.color = ColorFastSlow[0];
					}
					else
					{
						TextFastSlow.text = "SLOW";
						TextFastSlow.color = ColorFastSlow[1];
					}
				}
				//TextFastSlow.text = (judgeInfo.DeltaTime*1000).ToString("#0")+" ms";

				animator.SetTrigger("Judge");
			});
	}
}
