﻿using UnityEngine;
using System.Collections;

namespace EQLIPSE.Player
{
    public class ChargeNoteInstanceBehaviour : MonoBehaviour
    {
        [SerializeField]
        private Transform TipStart, Beam, TipEnd;
        [SerializeField]
        private Renderer NoteStartRenderer, NoteEndRenderer, BeamRenderer;
        [SerializeField]
        private Material NoteNormal, NoteBright, NoteDark;
        [SerializeField]
        private Material BeamNormal, BeamBright, BeamDark;

        public enum BeamStatuses
        {
            Normal,
            Bright,
            Dark
        }

        public BeamStatuses BeamStatus
        {
            set
            {
                switch (value)
                {
                    case BeamStatuses.Normal:
                        NoteStartRenderer.material = NoteNormal;
                        NoteEndRenderer.material = NoteNormal;
                        BeamRenderer.material = BeamNormal;
                        break;
                    case BeamStatuses.Bright:
                        NoteStartRenderer.material = NoteBright;
                        NoteEndRenderer.material = NoteBright;
                        BeamRenderer.material = BeamBright;
                        break;
                    case BeamStatuses.Dark:
                        NoteStartRenderer.material = NoteDark;
                        NoteEndRenderer.material = NoteDark;
                        BeamRenderer.material = BeamDark;
                        break;
                }
            }
        }

        public float Length
        {
            set
            {
                TipEnd.localPosition = Vector3.forward * value;
                Beam.localScale = new Vector3(1, value, 1);
            }
        }
    }
}