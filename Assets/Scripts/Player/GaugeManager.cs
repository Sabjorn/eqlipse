﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UniRx;

namespace EQLIPSE.Player
{
	public class GaugeManager : SingletonMonoBehaviour<GaugeManager>
	{
		public float Gauge;

		public Image Hider;

		public FloatEvent GaugeOnUpdate;

		bool isFailed;

		Subject<Unit> failSubject;
		public IObservable<Unit> FailObservable { get { return failSubject; } }

		void Awake()
		{
			isFailed = false;
			failSubject = new Subject<Unit>();

			switch (Environment.Player.Gauge)
			{
				case Difficulty.Gauges.Normal:
					Hider.enabled = false;
					Gauge = 0.0f;
					break;
				default:
					Hider.enabled = true;
					Gauge = 1.0f;
					break;
			}
		}

		// Use this for initialization
		void Start()
		{
			SongJudger.Singleton.JudgeObservable
				.Subscribe(judgeInfo =>
				{
					var tuple = Difficulty.GaugeDictionaries[Environment.Player.Gauge][judgeInfo.Judge];
					if (tuple.Item2)
					{
						Gauge += tuple.Item1;
					}
					else
					{
						Gauge += tuple.Item1 / SongLoader.Singleton.ChartData.NumAllNotes;
					}

					Gauge = Mathf.Clamp01(Gauge);
				});
		}

		// Update is called once per frame
		void Update()
		{
			GaugeOnUpdate.Invoke(Gauge);

			if (Environment.Player.Gauge != Difficulty.Gauges.Normal && Gauge == 0 && !isFailed)
			{
				isFailed = true;
				failSubject.OnNext(Unit.Default);
				failSubject.OnCompleted();
			}
		}
	}
}