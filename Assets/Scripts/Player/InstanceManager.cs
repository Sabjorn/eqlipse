﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using EQLIPSE;
using EQLIPSE.Utility;
using UniRx;

namespace EQLIPSE.Player
{
	public class InstanceManager : SingletonMonoBehaviour<InstanceManager>
	{
		bool isInitialized;

		Chart LoadedChart;

		int[] iNotesVisible, iNotesInvFuture;
		Dictionary<NormalNote, GameObject>[] noteObjectTable;
		int[] iNotesFingered, iNotesFingering;
		Dictionary<NormalNote, GameObject>[] noteFingerTable;

		int[] iChargeNotesVisible, iChargeNotesInvFuture;
		Dictionary<ChargeNote, GameObject>[] chargeNoteObjectTable;
		int[] iChargeNotesFingering, iChargeNotesFingered;
		Dictionary<ChargeNote, GameObject>[] chargeNoteFingerTable;

		int iTapNoteVisible, iTapNoteInvFuture, iTapNoteBeforeHelped, iTapNoteBeingHelped;
		Dictionary<TapNote, GameObject> tapNoteObjectTable;
		int iTapNoteFingered, iTapNoteFingering;
		Dictionary<TapNote, GameObject> tapNoteFingerTable;

		int iSlideNoteVisible, iSlideNoteInvFuture;
		Dictionary<SlideNote, GameObject> slideNoteObjectTable;
		List<SlideNote> slideNotesSorted;
		List<SlideNote> slideNotesBeforeHelped, slideNotesBeingHelped, slideNotesAfterHelped;
		List<SlideNote> slideNotesBeforeTraced;
		int iSlideNoteFingering, iSlideNoteFingered;
		Dictionary<SlideNote, GameObject> slideNoteFingerTable;

		Dictionary<SlideNote, GameObject> slideHelperDictionary;

		Dictionary<SlideNote, SliderTraceBehaviour> slideTraceDictionary;

		Dictionary<TapNote, GameObject> tapHelperDictionary;

		int measureVisible, measureBeyond;
		Dictionary<int, GameObject> measureDictionary;

		Dictionary<GameObject, Rational> supportLineDictionary;

		public GameObject NotePrefab, ChargeNotePrefab, TapNotePrefab, SlideNotePrefab, SlideHelperPrefab, SlideTracePrefab, TapHelperPrefab, BarPrefab, SupportPrefab, FingerPrefab;

		public Transform[] LanesOrigins;
		public Transform SliderPivot;

		public float VisibleDistance;
		public float HelperDuration, TraceInterval;

		void Start()
		{
			isInitialized = false;

			LoadedChart = null;

			iNotesInvFuture = new int[] { 0, 0, 0, 0 }; iNotesVisible = new int[] { 0, 0, 0, 0 };
			noteObjectTable = new Dictionary<NormalNote, GameObject>[4]{
				new Dictionary<NormalNote, GameObject>(),
				new Dictionary<NormalNote, GameObject>(),
				new Dictionary<NormalNote, GameObject>(),
				new Dictionary<NormalNote, GameObject>()
			};
			iNotesFingering = new int[4] { 0, 0, 0, 0 }; iNotesFingered = new int[4] { 0, 0, 0, 0 };
			noteFingerTable = new Dictionary<NormalNote, GameObject>[4]
			{
				new Dictionary<NormalNote, GameObject>(),
				new Dictionary<NormalNote, GameObject>(),
				new Dictionary<NormalNote, GameObject>(),
				new Dictionary<NormalNote, GameObject>()
			};

			iChargeNotesInvFuture = new int[] { 0, 0, 0, 0 }; iChargeNotesVisible = new int[] { 0, 0, 0, 0 };
			chargeNoteObjectTable = new Dictionary<ChargeNote, GameObject>[4]{
				new Dictionary<ChargeNote, GameObject>(),
				new Dictionary<ChargeNote, GameObject>(),
				new Dictionary<ChargeNote, GameObject>(),
				new Dictionary<ChargeNote, GameObject>()
			};
			iChargeNotesFingering = new int[4] { 0, 0, 0, 0 }; iChargeNotesFingered = new int[4] { 0, 0, 0, 0 };
			chargeNoteFingerTable = new Dictionary<ChargeNote, GameObject>[4]
			{
				new Dictionary<ChargeNote, GameObject>(),
				new Dictionary<ChargeNote, GameObject>(),
				new Dictionary<ChargeNote, GameObject>(),
				new Dictionary<ChargeNote, GameObject>()
			};

			iTapNoteVisible = 0; iTapNoteInvFuture = 0;
			iTapNoteBeforeHelped = 0;
			tapNoteObjectTable = new Dictionary<TapNote, GameObject>();
			iTapNoteFingering = 0; iTapNoteFingered = 0;
			tapNoteFingerTable = new Dictionary<TapNote, GameObject>();

			tapHelperDictionary = new Dictionary<TapNote, GameObject>();

			iSlideNoteVisible = 0; iSlideNoteInvFuture = 0;
			slideNoteObjectTable = new Dictionary<SlideNote, GameObject>();
			iSlideNoteFingering = 0; iSlideNoteFingered = 0;
			slideNoteFingerTable = new Dictionary<SlideNote, GameObject>();

			slideNotesBeforeHelped = new List<SlideNote>();
			slideNotesBeingHelped = new List<SlideNote>();
			slideNotesAfterHelped = new List<SlideNote>();

			slideNotesBeforeTraced = new List<SlideNote>();

			slideHelperDictionary = new Dictionary<SlideNote, GameObject>();

			slideTraceDictionary = new Dictionary<SlideNote, SliderTraceBehaviour>();

			measureVisible = 0; measureBeyond = 0;
			measureDictionary = new Dictionary<int, GameObject>();

			supportLineDictionary = new Dictionary<GameObject, Rational>();
		}

		public void Init()
		{
			LoadedChart = SongLoader.Singleton.ChartData;

			#region Slide Notes
			slideNotesSorted = new List<SlideNote>(SongLoader.Singleton.ChartData.SlideNotes);
			slideNotesSorted.Sort((SlideNote l, SlideNote r) => l.BeatFrom.CompareTo(r.BeatFrom));

			slideNotesBeforeHelped = new List<SlideNote>(slideNotesSorted);

			slideNotesBeforeTraced = new List<SlideNote>(slideNotesSorted);
			#endregion

			isInitialized = true;
		}

		void Update()
		{
			if (isInitialized)
			{
				List<Pair<Rational, int>> tp = new List<Pair<Rational, int>>();

				for (int i = 0; i < 4; i++)
				{
					#region Note
					// Selecting Visible Notes
					while (iNotesInvFuture[i] < LoadedChart.Notes[i].Count && BeatToZ(LoadedChart.Notes[i][iNotesInvFuture[i]].Beat) <= VisibleDistance)
					{
						noteObjectTable[i][LoadedChart.Notes[i][iNotesInvFuture[i]]] = Pool.Instantiate(NotePrefab);
						tp.Add(new Pair<Rational, int>(LoadedChart.Notes[i][iNotesInvFuture[i]].Beat, i));
						iNotesInvFuture[i]++;
					}

					while (iNotesVisible[i] < LoadedChart.Notes[i].Count && SongRhythmer.BeatToTime(LoadedChart.Notes[i][iNotesVisible[i]].Beat) < SongPlayer.Singleton.Time - SongJudger.Singleton.CurrentDifficulty.TimeJudgeDictionary[Judges.Good])
					{
						if (noteObjectTable[i].ContainsKey(LoadedChart.Notes[i][iNotesVisible[i]]))
						{
							Pool.Release(noteObjectTable[i][LoadedChart.Notes[i][iNotesVisible[i]]]);
							noteObjectTable[i].Remove(LoadedChart.Notes[i][iNotesVisible[i]]);
						}
						iNotesVisible[i]++;
					}

					// Moving Visible Notes

					List<KeyValuePair<NormalNote, GameObject>> disabled_Normal = new List<KeyValuePair<NormalNote, GameObject>>();
					foreach (var e in noteObjectTable[i])
					{
						if (SongJudger.Singleton.JudgeRecord[e.Key] == Judges.None)
							e.Value.transform.position = LanesOrigins[i].transform.position + Vector3.forward * BeatToZ(e.Key.Beat);
						else
						{
							disabled_Normal.Add(e);
						}
					}
					foreach (var e in disabled_Normal)
					{
						noteObjectTable[i].Remove(e.Key);
						Pool.Release(e.Value);
					}

					#endregion

					#region Note Finger
					while (iNotesFingering[i] < LoadedChart.Notes[i].Count && SongRhythmer.BeatToTime(LoadedChart.Notes[i][iNotesFingering[i]].Beat) - SongPlayer.Singleton.Time <= 0.25f)
					{
						if (Environment.Player.IsAutoPlay)
						{
							GameObject go = Pool.Instantiate(FingerPrefab);
							go.GetComponent<TutorFingerBehaviour>().Prefab = FingerPrefab;
							go.transform.position = LanesOrigins[i].position;
							noteFingerTable[i][LoadedChart.Notes[i][iNotesFingering[i]]] = go;
						}
						iNotesFingering[i]++;
					}

					while (iNotesFingered[i] < LoadedChart.Notes[i].Count && SongRhythmer.BeatToTime(LoadedChart.Notes[i][iNotesFingered[i]].Beat) <= SongPlayer.Singleton.Time)
					{
						if (Environment.Player.IsAutoPlay)
						{
							if (noteFingerTable[i].ContainsKey(LoadedChart.Notes[i][iNotesFingered[i]]))
							{
								GameObject go = noteFingerTable[i][LoadedChart.Notes[i][iNotesFingered[i]]];
								go.GetComponent<TutorFingerBehaviour>().Out();
								noteFingerTable[i].Remove(LoadedChart.Notes[i][iNotesFingered[i]]);
							}
						}
						iNotesFingered[i]++;
					}
					#endregion


					#region Charge Note
					// Selecting Visible Notes
					while (iChargeNotesInvFuture[i] < LoadedChart.ChargeNotes[i].Count && BeatToZ(LoadedChart.ChargeNotes[i][iChargeNotesInvFuture[i]].Beat[0]) <= VisibleDistance)
					{
						chargeNoteObjectTable[i][LoadedChart.ChargeNotes[i][iChargeNotesInvFuture[i]]] = Pool.Instantiate(ChargeNotePrefab);
						tp.Add(new Pair<Rational, int>(LoadedChart.ChargeNotes[i][iChargeNotesInvFuture[i]].Beat[0], i));
						iChargeNotesInvFuture[i]++;
					}
					while (iChargeNotesVisible[i] < LoadedChart.ChargeNotes[i].Count && LoadedChart.ChargeNotes[i][iChargeNotesVisible[i]].Beat[1] < SongRhythmer.Singleton.Beat)
					{
						Pool.Release(chargeNoteObjectTable[i][LoadedChart.ChargeNotes[i][iChargeNotesVisible[i]]]);
						chargeNoteObjectTable[i].Remove(LoadedChart.ChargeNotes[i][iChargeNotesVisible[i]]);
						iChargeNotesVisible[i]++;
					}

					// Moving Visible Notes
					foreach (var e in chargeNoteObjectTable[i])
					{
						e.Value.transform.position = LanesOrigins[i].transform.position + Vector3.forward * BeatToZ(e.Key.Beat[0]);
						e.Value.GetComponent<ChargeNoteInstanceBehaviour>().Length = Mathf.Min(BeatToZ(e.Key.Beat[1]) - BeatToZ(e.Key.Beat[0]), BeatToZ(e.Key.Beat[1]));
						if (SongJudger.Singleton.JudgeRecord[e.Key] != Judges.None)
						{
							e.Value.GetComponent<ChargeNoteInstanceBehaviour>().BeamStatus = ChargeNoteInstanceBehaviour.BeamStatuses.Dark;
						}
						else
						{
							if (SongJudger.Singleton.ChargeNotesJudging[i] == e.Key)
								e.Value.GetComponent<ChargeNoteInstanceBehaviour>().BeamStatus = ChargeNoteInstanceBehaviour.BeamStatuses.Bright;
							else
								e.Value.GetComponent<ChargeNoteInstanceBehaviour>().BeamStatus = ChargeNoteInstanceBehaviour.BeamStatuses.Normal;
						}
					}
					#endregion

					#region Charge Note Finger
					while (iChargeNotesFingering[i] < LoadedChart.ChargeNotes[i].Count && SongRhythmer.BeatToTime(LoadedChart.ChargeNotes[i][iChargeNotesFingering[i]].Beat[0]) - SongPlayer.Singleton.Time <= 0.25f)
					{
						if (Environment.Player.IsAutoPlay)
						{
							GameObject go = Pool.Instantiate(FingerPrefab);
							go.GetComponent<TutorFingerBehaviour>().Prefab = FingerPrefab;
							go.transform.position = LanesOrigins[i].position;
							chargeNoteFingerTable[i][LoadedChart.ChargeNotes[i][iChargeNotesFingering[i]]] = go;
						}
						iChargeNotesFingering[i]++;
					}

					while (iChargeNotesFingered[i] < LoadedChart.ChargeNotes[i].Count && SongRhythmer.BeatToTime(LoadedChart.ChargeNotes[i][iChargeNotesFingered[i]].Beat[1]) <= SongPlayer.Singleton.Time)
					{
						if (Environment.Player.IsAutoPlay)
						{
							if (chargeNoteFingerTable[i].ContainsKey(LoadedChart.ChargeNotes[i][iChargeNotesFingered[i]]))
							{
								GameObject go = chargeNoteFingerTable[i][LoadedChart.ChargeNotes[i][iChargeNotesFingered[i]]];
								go.GetComponent<TutorFingerBehaviour>().Out();
								chargeNoteFingerTable[i].Remove(LoadedChart.ChargeNotes[i][iChargeNotesFingered[i]]);
							}
						}
						iChargeNotesFingered[i]++;
					}
					#endregion

				}

				#region Tap Note
				// Selecting Visible Notes
				while (iTapNoteInvFuture < LoadedChart.TapNotes.Count && BeatToZ(LoadedChart.TapNotes[iTapNoteInvFuture].Beat) <= VisibleDistance)
				{
					tapNoteObjectTable[LoadedChart.TapNotes[iTapNoteInvFuture]] = Pool.Instantiate(TapNotePrefab);
					if (tp.Exists(_t => _t.Item1 == LoadedChart.TapNotes[iTapNoteInvFuture].Beat))
					{
						List<Pair<Rational, int>> ts = tp.FindAll(_t => _t.Item1 == LoadedChart.TapNotes[iTapNoteInvFuture].Beat);
						foreach (var t in ts)
						{
							GameObject g = Pool.Instantiate(SupportPrefab);
							supportLineDictionary.Add(g, LoadedChart.TapNotes[iTapNoteInvFuture].Beat);
							LineRenderer lr = g.GetComponent<LineRenderer>();
							lr.useWorldSpace = false;
							lr.positionCount = 2;
							lr.SetPositions(new Vector3[] { PositionOfValue(LoadedChart.TapNotes[iTapNoteInvFuture].Value), LanesOrigins[t.Item2].position });
						}
					}
					iTapNoteInvFuture++;
				}

				while (iTapNoteVisible < LoadedChart.TapNotes.Count && SongRhythmer.BeatToTime(LoadedChart.TapNotes[iTapNoteVisible].Beat) < SongRhythmer.Singleton.Time - SongJudger.Singleton.CurrentDifficulty.TimeJudgeDictionary[Judges.Good])
				{
					if (tapNoteObjectTable.ContainsKey(LoadedChart.TapNotes[iTapNoteVisible]))
					{
						tapNoteObjectTable[LoadedChart.TapNotes[iTapNoteVisible]].GetComponent<TapNoteBehaviour>().Finish();
						tapNoteObjectTable.Remove(LoadedChart.TapNotes[iTapNoteVisible]);
					}
					iTapNoteVisible++;
				}


				// Moving Visible Notes
				List<KeyValuePair<TapNote, GameObject>> disabled_Tap = new List<KeyValuePair<TapNote, GameObject>>();
				foreach (var e in tapNoteObjectTable)
				{
					e.Value.transform.position = PositionOfValue(e.Key.Value) + Vector3.forward * BeatToZ(e.Key.Beat);

					if (SongJudger.Singleton.JudgeRecord[e.Key] != Judges.None)
						disabled_Tap.Add(e);
				}
				foreach (var e in disabled_Tap)
				{
					tapNoteObjectTable.Remove(e.Key);
					e.Value.GetComponent<TapNoteBehaviour>().Finish();
				}
				#endregion

				#region Tap Note Finger
				while (iTapNoteFingering < LoadedChart.TapNotes.Count && SongRhythmer.BeatToTime(LoadedChart.TapNotes[iTapNoteFingering].Beat) - SongPlayer.Singleton.Time <= 0.25f)
				{
					if (Environment.Player.IsAutoPlay)
					{
						GameObject go = Pool.Instantiate(FingerPrefab);
						go.GetComponent<TutorFingerBehaviour>().Prefab = FingerPrefab;
						go.transform.position = PositionOfValue(LoadedChart.TapNotes[iTapNoteFingering].Value);
						tapNoteFingerTable[LoadedChart.TapNotes[iTapNoteFingering]] = go;
					}
					iTapNoteFingering++;
				}

				while (iTapNoteFingered < LoadedChart.TapNotes.Count && SongRhythmer.BeatToTime(LoadedChart.TapNotes[iTapNoteFingered].Beat) <= SongPlayer.Singleton.Time)
				{
					if (Environment.Player.IsAutoPlay)
					{
						if (tapNoteFingerTable.ContainsKey(LoadedChart.TapNotes[iTapNoteFingered]))
						{
							GameObject go = tapNoteFingerTable[LoadedChart.TapNotes[iTapNoteFingered]];
							go.GetComponent<TutorFingerBehaviour>().Out();
							tapNoteFingerTable.Remove(LoadedChart.TapNotes[iTapNoteFingered]);
						}
					}
					iTapNoteFingered++;
				}
				#endregion

				#region Slide Note
				while (iSlideNoteInvFuture < LoadedChart.SlideNotes.Count && BeatToZ(LoadedChart.SlideNotes[iSlideNoteInvFuture].BeatFrom) <= VisibleDistance)
				{
					slideNoteObjectTable[LoadedChart.SlideNotes[iSlideNoteInvFuture]] = Pool.Instantiate(SlideNotePrefab);
					slideNoteObjectTable[LoadedChart.SlideNotes[iSlideNoteInvFuture]].GetComponent<SlideNoteInstanceBehaviour>().AssignedSlideNote = LoadedChart.SlideNotes[iSlideNoteInvFuture];
					if (tp.Exists(_t => _t.Item1 == LoadedChart.SlideNotes[iSlideNoteInvFuture].BeatFrom))
					{
						List<Pair<Rational, int>> ts = tp.FindAll(_t => _t.Item1 == LoadedChart.SlideNotes[iSlideNoteInvFuture].BeatFrom);
						foreach (var t in ts)
						{
							GameObject g = Pool.Instantiate(SupportPrefab);
							supportLineDictionary.Add(g, LoadedChart.SlideNotes[iSlideNoteInvFuture].BeatFrom);

							LineRenderer lr = g.GetComponent<LineRenderer>();
							lr.useWorldSpace = false;
							lr.positionCount = 2;
							lr.SetPositions(new Vector3[] { PositionOfValue(LoadedChart.SlideNotes[iSlideNoteInvFuture].ValueFrom), LanesOrigins[t.Item2].position });
						}
					}
					iSlideNoteInvFuture++;
				}

				while (iSlideNoteVisible < LoadedChart.SlideNotes.Count && SongRhythmer.BeatToTime(LoadedChart.SlideNotes[iSlideNoteVisible].BeatTo) < SongRhythmer.Singleton.Time)
				{
					if (slideNoteObjectTable.ContainsKey(LoadedChart.SlideNotes[iSlideNoteVisible]))
					{
						slideNoteObjectTable[LoadedChart.SlideNotes[iSlideNoteVisible]].GetComponent<SlideNoteInstanceBehaviour>().Finish();
						slideNoteObjectTable.Remove(LoadedChart.SlideNotes[iSlideNoteVisible]);
					}
					iSlideNoteVisible++;
				}
				#endregion

				#region Slide Note Finger
				while (iSlideNoteFingering < LoadedChart.SlideNotes.Count && SongRhythmer.BeatToTime(LoadedChart.SlideNotes[iSlideNoteFingering].BeatFrom) - SongPlayer.Singleton.Time <= 0.25f)
				{
					if (Environment.Player.IsAutoPlay)
					{
						GameObject go = Pool.Instantiate(FingerPrefab);
						go.GetComponent<TutorFingerBehaviour>().Prefab = FingerPrefab;
						go.transform.position = PositionOfValue(LoadedChart.SlideNotes[iSlideNoteFingering].ValueFrom);
						slideNoteFingerTable[LoadedChart.SlideNotes[iSlideNoteFingering]] = go;
					}
					iSlideNoteFingering++;
				}

				while (iSlideNoteFingered < LoadedChart.SlideNotes.Count && SongRhythmer.BeatToTime(LoadedChart.SlideNotes[iSlideNoteFingered].BeatTo) <= SongPlayer.Singleton.Time)
				{
					if (Environment.Player.IsAutoPlay)
					{
						if (slideNoteFingerTable.ContainsKey(LoadedChart.SlideNotes[iSlideNoteFingered]))
						{
							GameObject go = slideNoteFingerTable[LoadedChart.SlideNotes[iSlideNoteFingered]];
							go.GetComponent<TutorFingerBehaviour>().Out();
							slideNoteFingerTable.Remove(LoadedChart.SlideNotes[iSlideNoteFingered]);
						}
					}
					iSlideNoteFingered++;
				}
				foreach (var entity in slideNoteFingerTable)
				{
					SlideNote sn = entity.Key;
					float v = Mathf.Lerp(
						sn.ValueFrom,
						sn.ValueTo,
						Mathf.Clamp01(
							Mathf.InverseLerp(
								(float)sn.BeatFrom,
								(float)sn.BeatTo,
								(float)SongRhythmer.Singleton.Beat
							)
						)
					);
					entity.Value.transform.position = PositionOfValue(v);
				}
				#endregion

				#region Same Support
				List<GameObject> removeList = new List<GameObject>();
				foreach (var e in supportLineDictionary)
				{
					if (SongRhythmer.BeatToTime(e.Value) <= SongPlayer.Singleton.Time)
					{
						removeList.Add(e.Key);
						Pool.Release(e.Key);
					}
				}
				foreach (var e in removeList)
					supportLineDictionary.Remove(e);

				foreach (var e in supportLineDictionary)
				{
					e.Key.transform.position = new Vector3(0, 0, BeatToZ(e.Value));
				}
				#endregion

				#region Tap Helper
				while (iTapNoteBeforeHelped < LoadedChart.TapNotes.Count)
				{
					TapNote tn = LoadedChart.TapNotes[iTapNoteBeforeHelped];
					if (SongRhythmer.BeatToTime(tn.Beat) <= SongPlayer.Singleton.Time + HelperDuration)
					{
						GameObject instance = Pool.Instantiate(TapHelperPrefab);
						instance.GetComponent<SlideHelperInstanceBehaviour>().Duration = HelperDuration;
						instance.transform.position = PositionOfValue(tn.Value);
						tapHelperDictionary[tn] = instance;

						iTapNoteBeforeHelped++;
					}
					else break;
				}
				while (iTapNoteBeingHelped < LoadedChart.TapNotes.Count)
				{
					TapNote tn = LoadedChart.TapNotes[iTapNoteBeingHelped];
					if (SongRhythmer.BeatToTime(tn.Beat) <= SongPlayer.Singleton.Time)
					{
						if (tapHelperDictionary.ContainsKey(tn))
						{
							Pool.Release(tapHelperDictionary[tn]);
							tapHelperDictionary.Remove(tn);
						}

						iTapNoteBeingHelped++;
					}
					else break;
				}
				#endregion

				#region Slide Helper
				for (int j = 0; j < slideNotesBeforeHelped.Count; j++)
				{
					SlideNote tn = slideNotesBeforeHelped.First();
					if (SongRhythmer.BeatToTime(tn.BeatFrom) <= SongPlayer.Singleton.Time + HelperDuration)
					{
						GameObject instance = Pool.Instantiate(SlideHelperPrefab);
						instance.GetComponent<SlideHelperInstanceBehaviour>().Duration = HelperDuration;
						instance.transform.position = PositionOfValue(tn.ValueFrom);
						slideHelperDictionary[tn] = instance;

						slideNotesBeingHelped.Add(tn);
						slideNotesBeforeHelped.RemoveAt(0);
					}
					else break;
				}
				for (int j = 0; j < slideNotesBeingHelped.Count; j++)
				{
					SlideNote tn = slideNotesBeingHelped.First();
					if (SongRhythmer.BeatToTime(tn.BeatFrom) <= SongPlayer.Singleton.Time)
					{
						if (slideHelperDictionary.ContainsKey(tn))
						{
							Pool.Release(slideHelperDictionary[tn]);
							slideHelperDictionary.Remove(tn);
						}

						slideNotesAfterHelped.Add(tn);
						slideNotesBeingHelped.RemoveAt(0);
					}
					else break;
				}
				#endregion

				#region Slide Trace

				for (int j = 0; j < slideNotesBeforeTraced.Count; j++)
				{
					SlideNote tn = slideNotesBeforeTraced.First();
					if (SongRhythmer.BeatToTime(tn.BeatFrom) <= SongPlayer.Singleton.Time + TraceInterval)
					{
						if (Mathf.Abs(tn.ValueFrom - tn.ValueTo) >= 0.01f)
						{
							var inst = Pool.Instantiate(SlideTracePrefab);
							inst.transform.position = new Vector3(0, 0, -0.01f);
							var behav = inst.GetComponent<SliderTraceBehaviour>();

							behav.From = tn.ValueFrom;
							behav.To = tn.ValueTo;
							behav.Init();

							slideTraceDictionary[tn] = behav;
						}

						slideNotesBeforeTraced.RemoveAt(0);
					}
					else break;
				}
				foreach (var e in slideTraceDictionary)
				{
					if (e.Value == null) slideTraceDictionary.Remove(e.Key);
				}

				#endregion

				#region Measures
				while (BeatToZ(SongRhythmer.MeasureToBeat(measureBeyond)) < VisibleDistance)
				{
					measureDictionary[measureBeyond] = Pool.Instantiate(BarPrefab);
					measureBeyond++;
				}
				while (SongRhythmer.MeasureToBeat(measureVisible) < SongRhythmer.Singleton.Beat)
				{
					Pool.Release(measureDictionary[measureVisible]);
					measureDictionary.Remove(measureVisible);
					measureVisible++;
				}
				foreach (var e in measureDictionary)
				{
					e.Value.transform.position = Vector3.forward * BeatToZ(SongRhythmer.MeasureToBeat(e.Key));
				}
				#endregion
			}
		}

		public float BeatToZ(double beat)
		{
			double BPMStandard = SongLoader.Singleton.ChartData.BpmStandard;

			return Math.Max(0, (float)(beat - SongRhythmer.Singleton.Beat) * User.HiSpeed * (480 / (float)BPMStandard));
		}
		public Vector3 PositionOfValue(float value)
		{
			return SliderPivot.position + Quaternion.AngleAxis(value * 180f, Vector3.back) * Vector3.left * 2f;
		}

		public void OnDestroy()
		{
			Pool.ReleasePools();
		}
	}
}