﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace EQLIPSE.Player
{
    public class PlayerDebugInfo : MonoBehaviour
    {
        Text uiText;
        public AudioSource SongSource;
        public RectTransform Indicator;
        void Start()
        {
            uiText = GetComponentInChildren<Text>();
        }

        void Update()
        {
            if (SongLoader.Singleton.IsLoaded)
            {
                float t = SongPlayer.Singleton.Time;
                float b = SongRhythmer.Singleton.Beat;
                float m = (float)SongRhythmer.BeatToMeasure(b);
                float mt = (float)SongRhythmer.MeterAt(m);
                float BPM = (float)SongRhythmer.BPMAt(b);

                uiText.text =
                    "実時間: " + t.ToString("#0.00")
                    + "\n音ズレ: " + ((SongSource.time - t) * 1000f).ToString("#0.0") + "ms"
                    + "\n拍時間: " + b.ToString("#0.00")
                    + "\n小　節: " + m.ToString("#0.00")
                    + "\n拍　子: " + mt.ToString("#0.00")
                    + "\nBPM: " + BPM.ToString("#0.0")
                    + "\n平　均: " + (JudgeStatics.Singleton.Average * 1000).ToString("#0.0") + "ms"
                    + "\n偏　差: " + (JudgeStatics.Singleton.StandardDeviation * 1000).ToString("#0.0") + "ms";

                //Indicator.localScale = new Vector3(1, Mathf.Clamp(SongPlayer.Singleton.DelayCompensation * 10f / 0.3f, -1, 1), 1);
            }
            else
            {
                uiText.text = "Loading...";
            }
        }
    }
}