﻿using UnityEngine;
using UnityEngine.UI;
using UniRx;
using System.Collections;

namespace EQLIPSE.Player
{
	public class PauseMenuBehaviour : MonoBehaviour
	{
		[SerializeField]
		Toggle AUTOToggle;

		[SerializeField] private ButtonHoldBehaviour PauseButton;

		void Start()
		{
			SongPlayer.Singleton.OnFinished.AsObservable().Subscribe(_ =>
			{
				PauseButton.enabled = false;
			});
		}

		public void Open()
		{
			transform.GetChild(0).gameObject.SetActive(true);
			Time.timeScale = 0;
		}
		public void Close()
		{
			transform.GetChild(0).gameObject.SetActive(false);
			Time.timeScale = 1;
		}

		public void OnResume()
		{
			SongPlayer.Singleton.Resume();
			//OptionBarBehaviour.Singleton.OnDismiss();
			Close();
		}

		public void OnAbort()
		{
			Time.timeScale = 1;
			ResultWriter.Singleton.WriteResult();
			Environment.Player.PlayData.Gauge = 0;
			SceneTransitioner.Singleton.Next();
		}

		public void ToggleAUTO(bool isAuto)
		{
			Environment.Player.IsAutoPlay = isAuto;
		}
	}
}