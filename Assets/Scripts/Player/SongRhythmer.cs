﻿using UnityEngine;
using System.Collections;

namespace EQLIPSE.Player
{
    public class SongRhythmer : SingletonMonoBehaviour<SongRhythmer>
    {
        public float Time
        {
            get { return SongPlayer.Singleton.Time; }
        }

        float _beat = 0;
        public float Beat
        {
            get { return _beat; }
            private set { _beat = value; }
        }

        void Start()
        {
            _beat = 0;
        }

        void Update()
        {
            if (SongLoader.Singleton.ChartData != null) _beat = (float)SongRhythmer.TimeToBeat(Time);
        }

        public static double TimeToBeat(double time)
        {
            return SongLoader.Singleton.ChartData.ToBeat(time);
        }

        public static double BeatToTime(double beat)
        {
            return SongLoader.Singleton.ChartData.ToTime(beat);
        }

        public static double BPMAt(double beat)
        {
            return 60.0 / SongLoader.Singleton.ChartData.BeatBySecond.DYDXAt(beat);
        }

        public static double MeterAt(double measure)
        {
            return SongLoader.Singleton.ChartData.MeasureByBeat.DYDXAt(measure);
        }

        public static double BeatToMeasure(double beat)
        {
            return SongLoader.Singleton.ChartData.BeatToMeasure(beat);
        }
        public static double MeasureToBeat(int measure)
        {
            return SongLoader.Singleton.ChartData.MeasureToBeat(measure);
        }
    }
}