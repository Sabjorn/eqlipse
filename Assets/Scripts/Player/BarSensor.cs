﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

namespace EQLIPSE.Player
{
    public class BarSensor : MonoBehaviour
    {
        public List<float> Touches
        {
            get
            {
                List<float> ret = new List<float>(touches);
                ret.AddRange(touchesVirtual);

                return ret;
            }
        }
        List<float> touches;
        List<float> touchesVirtual;

        [System.Serializable]
        public class UnityFloatEvent : UnityEvent<float> { }

        [System.Serializable]
        public class SliderTouchesEvent : UnityEvent<List<float>> { }

        public ParticleSystem HitParticle;

        public bool Pressing;
        public float Position;

        public UnityFloatEvent OnTouched;

        // Use this for initialization
        void Start()
        {
            Pressing = false;
            Position = 0f;

            touches = new List<float>();
            touchesVirtual = new List<float>();
        }

        // Update is called once per frame
        void Update()
        {
            Pressing = false;

            touches.Clear();
            touchesVirtual.Clear();

            if (!Environment.Player.IsAutoPlay)
            {
                var tchs = Input.touches;
                for (int i = 0; i < tchs.Length; i++)
                {
                    Touch t = tchs[i];

                    RaycastHit rh = new RaycastHit();
                    if (GetComponent<Collider>().Raycast(Camera.main.ScreenPointToRay(t.position), out rh, 100))
                    {
                        Vector3 vpoint = rh.point - transform.position;
                        Position = Mathf.Clamp01(Mathf.Atan2(vpoint.x, vpoint.y) / Mathf.PI + 0.5f);
                        touches.Add(Position);

                        if (t.phase == TouchPhase.Began)
                        {
                            OnTouched.Invoke(Position);
                        }
                    }
                }
#if UNITY_EDITOR
                if (Input.GetMouseButtonDown(0))
                {
                    RaycastHit rh = new RaycastHit();
                    if (GetComponent<Collider>().Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out rh, 100))
                    {
                        Vector3 vpoint = rh.point - transform.position;
                        Position = Mathf.Clamp01(Mathf.Atan2(vpoint.x, vpoint.y) / Mathf.PI + 0.5f);
                        OnTouched.Invoke(Position);
                    }
                }
                if (Input.GetMouseButton(0))
                {
                    RaycastHit rh = new RaycastHit();
                    if (GetComponent<Collider>().Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out rh, 100))
                    {
                        Vector3 vpoint = rh.point - transform.position;
                        Position = Mathf.Clamp01(Mathf.Atan2(vpoint.x, vpoint.y) / Mathf.PI + 0.5f);
                        touches.Add(Position);
                    }
                }
#endif
            }
        }

        public void Tap(float position)
        {
            OnTouched.Invoke(position);
        }

        public void Hold(float position)
        {
            touchesVirtual.Add(position);
        }
    }
}