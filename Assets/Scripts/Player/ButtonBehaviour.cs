﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using UniRx;

namespace EQLIPSE.Player
{
	public class ButtonBehaviour : MonoBehaviour
	{
		bool isPressing, isPressingVirtual;
		new Renderer renderer;

		public int LaneNo;
		public Collider AttachedCollider;

		public KeyCode AlternateKey;

		BoolReactiveProperty _rState;
		public BoolReactiveProperty RState
		{
			get
			{
				if (_rState == null) _rState = new BoolReactiveProperty(false);
				return _rState;
			}
			set
			{
				_rState = value;
			}
		}

		void Start()
		{
			renderer = GetComponent<Renderer>();
		}

		void Update()
		{
			isPressing = false;

			if (!Environment.Player.IsAutoPlay)
			{
				var touches = Input.touches;
				for (int i = 0; i < touches.Length; i++)  // For Touch
				{
					Touch touch = touches[i];

					RaycastHit hit;
					isPressing |= AttachedCollider.Raycast(Camera.main.ScreenPointToRay(new Vector3(touch.position.x, touch.position.y, 0)), out hit, 30);

					if (isPressing) break;
				}
			}
			isPressing |= isPressingVirtual || Input.GetKey(AlternateKey);
			RState.Value = isPressing;

			renderer.material.color = new Color(1, 1, 1, isPressing ? 1 : 0);
		}

		public void Tap()
		{
			StartCoroutine(TapCoroutine());
		}

		IEnumerator TapCoroutine()
		{
			isPressingVirtual = true;
			yield return null;
			isPressingVirtual = false;
		}

		public void Press()
		{
			isPressingVirtual = true;
		}

		public void Release()
		{
			isPressingVirtual = false;
		}
	}
}