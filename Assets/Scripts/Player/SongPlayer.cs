﻿using UnityEngine;
using UnityEngine.Events;
using UniRx;
using System;
using System.Collections;
using System.Linq;
using DG.Tweening;
using EQLIPSE.Utility;

namespace EQLIPSE.Player
{
	[RequireComponent(typeof(AudioSource))]
	public class SongPlayer : SingletonMonoBehaviour<SongPlayer>
	{
		AudioSource audioSource;

		float timeLength;

		public AudioClip Failed;

		public bool IsPlaying;

		public UnityEvent OnFinished;

		public float Time
		{
			get
			{
				//return UnityEngine.Time.time - TimeStarted + delayCompensation;
				return audioSourceExtension.Time + User.AudioOffset * 0.01f;
			}
		}

		private AudioSourceExtension audioSourceExtension;

		void Awake()
		{
			audioSourceExtension = GetComponent<AudioSourceExtension>();
		}

		void Start()
		{
			audioSource = GetComponent<AudioSource>();
			audioSource.time = 0;

			timeLength = 0;

			IsPlaying = false;

			GaugeManager.Singleton.FailObservable
				.Do(_ =>
				{
					GaugeManager.Singleton.Gauge = 0;
					ResultWriter.Singleton.WriteResult();

					Pause();
					audioSource.Stop();
					audioSource.PlayOneShot(Failed, 1f);
				})
				.Delay(new TimeSpan(0, 0, 0, 2))
				.Subscribe(_ =>
				{
					SceneTransitioner.Singleton.Next();
				});
		}

		public void OnLoaded()
		{
			AudioClip clip = SongLoader.Singleton.ChartData.Audio;
			if (clip != null) { audioSource.clip = clip; }
			timeLength = clip != null ? audioSource.clip.length : (float)SongLoader.Singleton.ChartData.TimeLength;

			Play();
		}

		public void Play()
		{
			IsPlaying = true;

			audioSourceExtension.PlayAfter(2f);

			Observable.Timer(new TimeSpan(0, 0, 0, Mathf.CeilToInt(audioSource.clip.length + 2f)))
				.Subscribe(_ =>
				{
					OnFinished.Invoke();
				});
		}

		public void Pause()
		{
			IsPlaying = false;
			audioSourceExtension.Pause();
		}

		public void Resume()
		{
			IsPlaying = true;
			audioSourceExtension.Unpause();
		}
	}
}