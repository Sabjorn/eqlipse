﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace EQLIPSE.Player
{
    [RequireComponent(typeof(Text))]
    public class ScoreToText : MonoBehaviour {

        Text textComponent;

        // Use this for initialization
        void Start () {
            textComponent = GetComponent<Text>();
        }
	
        public void InputScore(int score)
        {
            textComponent.text = score.ToString("0000000");
        }
    }
}