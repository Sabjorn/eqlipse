﻿using UnityEngine;
using System.Collections;
using UniRx;
using UnityEngine.Events;

namespace EQLIPSE.Player
{
    public class ComboManager : SingletonMonoBehaviour<ComboManager>
    {
        int combo, maxCombo;

        Subject<Unit> fullcomboSubject;
        public IObservable<Unit> FullComboObservable { get { return fullcomboSubject; } }

        public int Combo { get { return combo; } }
        public int IdealCombo;
        public int MaxCombo
        {
            get
            {
                return maxCombo;
            }
        }

        void Awake()
        {
            combo = 0;
            maxCombo = 0;
            IdealCombo = 0;

            fullcomboSubject = new Subject<Unit>();
        }

        void Start()
        {
            SongJudger.Singleton.JudgeObservable
                .Subscribe(judgeInfo =>
                {
                    if (judgeInfo.Judge != Judges.Miss && judgeInfo.Judge != Judges.SlideMiss)
                    {
                        combo++;
                        maxCombo = combo > maxCombo ? combo : maxCombo;
                    }
                    else
                    {
                        combo = 0;
                    }

                    if (IdealCombo != 0 && Combo == IdealCombo) fullcomboSubject.OnNext(Unit.Default);
                });
        }
    }
}