﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using EQLIPSE;
using EQLIPSE.Utility;
using UniRx;

namespace EQLIPSE.Player
{
    public class SliderTraceBehaviour : MonoBehaviour, IPoolant
    {
        public GameObject ArrowPrefab;
        public int Nums;
        public float From, To, Width, Duration;

        List<GameObject> arrows = new List<GameObject>();

        public void Init()
        {
            int dir = (int)Mathf.Sign(To - From);

            for (int i = 0; i < 2; i++)
            {
                arrows.Add(Pool.Instantiate(ArrowPrefab));
                float v = From + 0.04f * (i + 1) * dir;

                arrows[i].transform.position = InstanceManager.Singleton.PositionOfValue(v) - Vector3.back * 0.02f;
                arrows[i].transform.rotation = Quaternion.identity;
                arrows[i].transform.Rotate(new Vector3(0, 0, 90 - v * 180 + (dir == 1 ? 180 : 0)));
            }

            Observable.Timer(new TimeSpan(0, 0, 0, 0, 500))
                .Subscribe(_ => { Pool.Release(gameObject); });
        }

        public void OnPooled() { }

        public void OnReleased()
        {
            foreach (GameObject g in arrows) { Pool.Release(g); }
            arrows.Clear();
        }
    }
}