﻿using UnityEngine;
using System.Collections;
using UniRx;

namespace EQLIPSE.Player
{
    public class PlayerCanvasAnimator : SingletonMonoBehaviour<PlayerCanvasAnimator>
    {

        // Use this for initialization
        void Start()
        {
            GaugeManager.Singleton.FailObservable
                .Subscribe(_ =>
                {
                    GetComponent<Animator>().SetTrigger("Fail");
                });
        }
    }
}