﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;

namespace EQLIPSE.Player
{
    public class ScoreManager : SingletonMonoBehaviour<ScoreManager>
    {
        Dictionary<Judges, int> counter;
        public Dictionary<Judges, int> Counter { get { return counter; } }

        public IntEvent ScoreOnJudged;

        public int Score
        {
            get
            {
                return SongLoader.Singleton.IsLoaded ? Scoring.ScoreFromCounter(counter, SongLoader.Singleton.ChartData.NumMaxCombo) : 0;
            }
        }

        void Awake()
        {
            counter = new Dictionary<Judges, int>()
			{
				{Judges.None,0 },
				{Judges.Perfect,0 },
                {Judges.Great,0 },
                {Judges.Good,0 },
                {Judges.Miss,0 },
                {Judges.SlidePerfect,0 },
                {Judges.SlideGreat,0 },
                {Judges.SlideGood,0 },
                {Judges.SlideMiss,0 },
            };
        }

        void Start()
        {
            SongJudger.Singleton.JudgeObservable
                .Subscribe(judgeInfo =>
	            {
		            counter[Judges.None]--;
                    counter[judgeInfo.Judge]++;
                    ScoreOnJudged.Invoke(Score);
                });

	        SongLoader.Singleton.ProgressObservable
		        .Subscribe(_ => { }, Init);
        }

	    public void Init()
	    {
		    counter[Judges.None] = SongLoader.Singleton.ChartData.NumMaxCombo;
	    }
    }
}