﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

namespace EQLIPSE.Player
{
    public class SliderHelper : MonoBehaviour
    {
        Transform Outer, Inner;
        MeshRenderer mrO, mrI;

        // Use this for initialization
        void Start()
        {
            Outer = transform.Find("Outer");
            Inner = transform.Find("Inner");

            mrO = Outer.GetComponentInChildren<MeshRenderer>();
            mrI = Inner.GetComponentInChildren<MeshRenderer>();

            StartCoroutine(BeginCoroutine());
        }

        IEnumerator BeginCoroutine()
        {
            mrI.material.color = new Color(1, 1, 1, 0);
            mrO.material.color = new Color(1, 1, 1, 0);

            Inner.localScale = Vector3.zero;

            Tweener t = mrI.material.DOFade(1f, 1f);
            mrO.material.DOFade(1f, 1f);
            Inner.DOScale(1f, 1f).SetEase(Ease.Linear);

            while (!t.IsComplete()) yield return null;

            Inner.gameObject.SetActive(false);
        }

        public void End()
        {
            StartCoroutine(EndCoroutine());
        }

        IEnumerator EndCoroutine()
        {
            Tweener t = Outer.DOScale(1.5f, 0.2f);
            mrO.material.DOFade(0f, 0.2f);

            while (!t.IsComplete())
            {
                yield return null;
            }

            Destroy(gameObject);
        }
    }
}