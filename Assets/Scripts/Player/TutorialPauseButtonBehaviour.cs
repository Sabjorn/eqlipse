﻿using UnityEngine;
using UnityEngine.UI;
using UniRx;
using System.Collections;
using EQLIPSE.SuperScene;
using EQLIPSE.Utility;

namespace EQLIPSE.Player
{
	[RequireComponent(typeof(Button))]
	public class TutorialPauseButtonBehaviour : MonoBehaviour
	{
		Button button;

		// Use this for initialization
		void Start()
		{
			gameObject.SetActive(SuperSceneManager.Singleton.CurrentState == "Tutorial");

			button = GetComponent<Button>();

			button.OnClickAsObservable()
				.Subscribe(_ =>
				{
					SongPlayer.Singleton.Pause();
					Time.timeScale = 0;
					Modal.YesNoDialogObservable("チュートリアルをスキップしますか？")
					.Subscribe(b =>
					{
						Time.timeScale = 1;
						if (b)
						{
							SceneTransitioner.Singleton.Next();
						}
						else
						{
							SongPlayer.Singleton.Resume();
						}
					});
				});
		}
	}
}