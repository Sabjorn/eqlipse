﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UniRx;
using System.Collections;

namespace EQLIPSE.Player
{
	public class PreplayManager : MonoBehaviour
	{
		[SerializeField]
		Animator CanvasAnimator;

		[SerializeField]
		Text Stage, Title, Artist, Genre, Difficulty;

		[SerializeField]
		Text[] GetReadys;

		[SerializeField]
		RawImage Cover;

		[SerializeField]
		Image Gauge;

		public UnityEvent OnPlay;

		// Use this for initialization
		void Start()
		{
			Stage.text = Environment.Player.IsChallenge ? "CHALLENGE" : "FREE PLAY";

			Title.text = Environment.Player.SelectedSong.Title;
			Artist.text = Environment.Player.SelectedSong.Artist;
			Genre.text = Environment.Player.SelectedSong.Genre;

			Cover.texture = Environment.Player.SelectedSong.Cover;

			Difficulty.text = Environment.Player.SelectedDifficulty + ": " + Environment.Player.SelectedSong.LevelDictionary[Environment.Player.SelectedDifficulty];

			GetReadys[0].text = "GET READY...";
			GetReadys[1].text = "GET READY...";
		}

		public void OnEndTransition()
		{
			CanvasAnimator.SetBool("Pre Simple", Environment.Player.IsSimplePre);
			CanvasAnimator.SetTrigger("Pre In");

			Observable.TimerFrame(50)
				.SelectMany(_ =>
				{
					CanvasAnimator.enabled = false;
					SongLoader.Singleton.Load();
					return SongLoader.Singleton.ProgressObservable;
				})
				.DelayFrame(3)
				.Subscribe(p =>
				{
					Gauge.fillAmount = p;
				}, () =>
				{
					GetReadys[0].text = "GO!!";
					GetReadys[1].text = "GO!!";

					CanvasAnimator.enabled = true;
					CanvasAnimator.SetTrigger("Pre Out");
					Observable.TimerFrame(40)
						.Subscribe(__ => { OnPlay.Invoke(); }).AddTo(gameObject);
				}).AddTo(gameObject);

		}
	}
}