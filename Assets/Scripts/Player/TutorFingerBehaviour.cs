﻿using UnityEngine;
using System.Collections;
using EQLIPSE;
using EQLIPSE.Utility;

namespace EQLIPSE.Player
{
    public class TutorFingerBehaviour : MonoBehaviour
    {
        public GameObject Prefab;
        public void Out() { StartCoroutine(OutCoroutine()); }
        IEnumerator OutCoroutine()
        {
            GetComponent<Animator>().SetTrigger("Out");

            yield return new WaitForSeconds(0.5f);

            Pool.Release(gameObject);
        }
    }
}