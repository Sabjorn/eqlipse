﻿using UnityEngine;
using System.Collections;
using UniRx;

namespace EQLIPSE.Player
{
	public class ParticleManager : SingletonMonoBehaviour<ParticleManager>
	{
		public ParticleSystem[] SlideParticles, TapParticles;

		public ParticleSystem[] BgParticles;

		void Start()
		{
			SongJudger.Singleton.JudgeObservable
				.Subscribe(judgeInfo =>
				{
					switch (judgeInfo.NoteType)
					{
						case JudgeInfo.NoteTypes.Normal:
							break;
						case JudgeInfo.NoteTypes.Charge:
							break;
						case JudgeInfo.NoteTypes.Slide:
							if (judgeInfo.Judge != Judges.SlideMiss)
							{
								foreach (var p in SlideParticles)
								{
									p.transform.position = InstanceManager.Singleton.PositionOfValue(judgeInfo.Position);
									p.Play();
								}
							}
							break;
						case JudgeInfo.NoteTypes.Tap:
							if (judgeInfo.Judge != Judges.SlideMiss)
							{
								foreach (var p in TapParticles)
								{
									p.transform.position = InstanceManager.Singleton.PositionOfValue((judgeInfo.Note as TapNote).Value);
									p.Play();
								}
							}
							break;
					}
				});

			foreach (var particle in BgParticles)
			{
				particle.gameObject.SetActive(!User.SimpleEffect);
			}
		}
	}
}