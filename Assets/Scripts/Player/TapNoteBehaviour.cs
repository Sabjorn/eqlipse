﻿using System;
using System.Collections;
using System.Collections.Generic;
using EQLIPSE.Utility;
using UnityEngine;
using DG.Tweening;
using UniRx;
using UniRx.Triggers;

namespace EQLIPSE.Player
{
	public class TapNoteBehaviour : MonoBehaviour, IPoolant
	{
		[SerializeField]
		GameObject ParticlePrefab;
		GameObject _particleGameObject;

		void Update()
		{
			if (_particleGameObject != null) _particleGameObject.transform.position = transform.position;
		}

		public void Finish()
		{
			transform.DOScale(1.2f, 0.2f);
			GetComponentInChildren<Renderer>().material.DOFade(0, 0.2f);

			if (_particleGameObject != null)
			{
				var em = _particleGameObject.GetComponent<ParticleSystem>().emission;
				em.rateOverTime = 0;
				_particleGameObject.UpdateAsObservable()
					.Where(_ => _particleGameObject != null && _particleGameObject.GetComponent<ParticleSystem>().particleCount == 0)
					.Subscribe(_ => Destroy(_particleGameObject))
					.AddTo(_particleGameObject);
			}
			Observable.Timer(new TimeSpan(0, 0, 0, 0, 200))
				.Subscribe(x =>
				{
					Pool.Release(gameObject);
				});
		}

		public void OnPooled()
		{
			transform.localScale = Vector3.one;
			GetComponentInChildren<Renderer>().material.color = Color.white;
			if (!User.SimpleEffect) _particleGameObject = Instantiate(ParticlePrefab);
		}

		public void OnReleased()
		{
			if (!User.SimpleEffect) Destroy(_particleGameObject);
		}
	}
}