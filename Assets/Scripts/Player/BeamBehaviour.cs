﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UniRx;

namespace EQLIPSE.Player
{
    public class BeamBehaviour : MonoBehaviour
    {
        Tweener In, Out;

        public ButtonBehaviour Button;

        // Use this for initialization
        void Start()
        {
            transform.localScale = new Vector3(2, 1, 0);
        
            Button.RState.Subscribe(b =>
            {
                if (b) Pushed(); else Released();
            }).AddTo(gameObject);
        }

        public void Pushed()
        {
            Out.Complete();
            transform.localScale = new Vector3(2, 1, 0);
            In = transform.DOScale(new Vector3(2, 1, 2), 0.1f);
        }

        public void Released()
        {
            In.Complete();
            Out = transform.DOScale(new Vector3(0, 1, 2), 0.1f);
        }
    }
}