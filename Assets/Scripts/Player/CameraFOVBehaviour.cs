﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EQLIPSE.Player
{
	public class CameraFOVBehaviour : MonoBehaviour
	{
		public float DefaultFOV;

		void Awake(){
			var camera = GetComponent<Camera> ();

			var fov = Mathf.Atan (1f / (Mathf.Pow(1.1f,User.LaneSize) * Mathf.Tan ((90f - DefaultFOV / 2f) * Mathf.Deg2Rad))) * 2f*Mathf.Rad2Deg;
			camera.fieldOfView = fov;
		}
	}
}