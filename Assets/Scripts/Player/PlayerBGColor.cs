﻿using UnityEngine;
using System.Collections;

namespace EQLIPSE.Player
{
    public class PlayerBGColor : MonoBehaviour
    {
        public Color Enough, Lack;

        // Update is called once per frame
        void Update()
        {
            var cam = GetComponent<Camera>();
            switch (Environment.Player.Gauge)
            {
                case Difficulty.Gauges.Normal:
                    if (GaugeManager.Singleton.Gauge > 0.7f)
                    {
                        cam.backgroundColor = Color.Lerp(cam.backgroundColor, Enough, 0.1f);
                    }
                    else
                    {
                        cam.backgroundColor = Color.Lerp(cam.backgroundColor, Lack, 0.1f);
                    }
                    break;
                case Difficulty.Gauges.Hard:
                    if (GaugeManager.Singleton.Gauge > 0.3f)
                    {
                        cam.backgroundColor = Color.Lerp(cam.backgroundColor, Enough, 0.1f);
                    }
                    else
                    {
                        cam.backgroundColor = Color.Lerp(cam.backgroundColor, Lack, 0.1f);
                    }
                    break;
            }
        }
    }
}