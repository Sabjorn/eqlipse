﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.IO;
using UniRx;

namespace EQLIPSE.Player
{
	public class SongLoader : SingletonMonoBehaviour<SongLoader>
	{
		bool isLoaded;
		public bool IsLoaded { get { return isLoaded; } }

		Chart chartData;
		public Chart ChartData { get { return chartData; } }

		Subject<float> progressSubject;
		public IObservable<float> ProgressObservable { get { return progressSubject; } }

		void Awake()
		{
			progressSubject = new Subject<float>();
			isLoaded = false;
		}

		public void Load()
		{
			StartCoroutine(LoadSongEnumerator());
		}

		IEnumerator LoadSongEnumerator()
		{
			chartData = new Chart();

			WWW wwwChart = new WWW(Environment.SongsUrl + "/" + Environment.Player.SelectedSong.Folder + "/" + Environment.DifficultyFilenameDictionary[Environment.Player.SelectedDifficulty] + ".eql") { threadPriority = ThreadPriority.Low };
			yield return wwwChart;

			for (int i = 0; i < 20; i++)
			{
				progressSubject.OnNext(Mathf.Lerp(0, 0.3f, i / 20f));
				yield return null;
			}

			if (string.IsNullOrEmpty(wwwChart.error))
			{
				chartData = new Chart();
				chartData.Song = Environment.Player.SelectedSong;
				chartData.Difficulty = Environment.Player.SelectedDifficulty;

				using (StreamReader reader = new StreamReader(new MemoryStream(wwwChart.bytes)))
				{
					chartData.Parse(reader.ReadToEnd());
				}
			}
			wwwChart.Dispose();

			for (int i = 0; i < 20; i++)
			{
				progressSubject.OnNext(Mathf.Lerp(0.3f, 0.5f, i / 20f));
				yield return null;
			}

			string clipURL = Environment.SongsUrl + "/" + Environment.Player.SelectedSong.Folder + "/" + chartData.WaveFileName;
			clipURL = Path.ChangeExtension(clipURL, ".mp3");

			WWW wwwWave = new WWW(clipURL) { threadPriority = ThreadPriority.Low };
			yield return wwwWave;

			var clip = wwwWave.GetAudioClip(false, false);
			if (clip == null)
			{
				clipURL = Path.ChangeExtension(clipURL, ".ogg");
				wwwWave = new WWW(clipURL) { threadPriority = ThreadPriority.Low };
				yield return wwwWave;
				clip = wwwWave.GetAudioClip(false, false);
			}

			for (int i = 0; i < 20; i++)
			{
				progressSubject.OnNext(Mathf.Lerp(0.5f, 0.8f, i / 20f));
				yield return null;
			}

			if (string.IsNullOrEmpty(wwwWave.error))
			{
				chartData.Audio = clip;
			}
			wwwWave.Dispose();

			Environment.Player.LoadedChart = chartData;

			for (int i = 0; i < 20; i++)
			{
				progressSubject.OnNext(Mathf.Lerp(0.8f, 1f, i / 20f));
				yield return null;
			}

			progressSubject.OnCompleted();

			isLoaded = true;
		}

	}
}