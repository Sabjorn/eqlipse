﻿using System;
using UnityEngine;
using System.Collections;
using System.Linq;
using EQLIPSE;
using DG.Tweening;
using EQLIPSE.Utility;
using UniRx;

namespace EQLIPSE.Player
{
    [RequireComponent(typeof(LineRenderer))]
    public class SlideNoteInstanceBehaviour : MonoBehaviour, IPoolant
    {
        int resolution;

        LineRenderer lineRenderer;

        [SerializeField]
        GameObject ParticlePrefab;

        GameObject particle;

        SlideNote assignedNote;
        public SlideNote AssignedSlideNote
        {
            get
            {
                return assignedNote;
            }
            set
            {
                var dx = Mathf.Abs(value.ValueTo - value.ValueTo) * 3;
                var dy = Mathf.Abs(value.BeatFrom - value.BeatTo) * 4;

                resolution = (int)Mathf.Max(2, Mathf.Ceil(Mathf.Sqrt(Mathf.Pow(dx, 2) + Mathf.Pow(dy, 2))));

                lineRenderer.positionCount = resolution;

                assignedNote = value;
            }
        }

        public Transform CapStart, CapCurrent, CapEnd;

        public Color ColorNormal;

        void Awake()
        {
            lineRenderer = GetComponent<LineRenderer>();
        }

        void LateUpdate()
        {
            if (AssignedSlideNote != null)
            {
                float v = Mathf.Lerp(
                    AssignedSlideNote.ValueFrom,
                    AssignedSlideNote.ValueTo,
                    Mathf.Clamp01(
                        Mathf.InverseLerp(
                            (float)AssignedSlideNote.BeatFrom,
                            (float)AssignedSlideNote.BeatTo,
                            (float)SongRhythmer.Singleton.Beat
                        )
                    )
                );
                Vector3 vStart = Quaternion.AngleAxis(AssignedSlideNote.ValueFrom * 180f, Vector3.back) * Vector3.left * 2f;
                Vector3 vEnd = Quaternion.AngleAxis(AssignedSlideNote.ValueTo * 180f, Vector3.back) * Vector3.left * 2f;

                CapStart.position = InstanceManager.Singleton.SliderPivot.position
                                    + vStart
                                    + Vector3.forward * InstanceManager.Singleton.BeatToZ(AssignedSlideNote.BeatFrom);
                CapCurrent.position = InstanceManager.Singleton.PositionOfValue(v)
                                      + Vector3.forward * InstanceManager.Singleton.BeatToZ(AssignedSlideNote.BeatFrom);
                CapEnd.position = InstanceManager.Singleton.SliderPivot.position
                                  + vEnd
                                  + Vector3.forward * InstanceManager.Singleton.BeatToZ(AssignedSlideNote.BeatTo);

                Vector3[] vs = Enumerable.Range(0, resolution)
                    .Select(i => (float)i / (resolution - 1))
                    .Select(t => InstanceManager.Singleton.SliderPivot.position
                                 + Vector3.Slerp(vStart, vEnd, t)
                                 + Vector3.forward * InstanceManager.Singleton.BeatToZ(Mathf.Lerp((float)AssignedSlideNote.BeatFrom, (float)AssignedSlideNote.BeatTo, t)))
                    .ToArray();
                lineRenderer.SetPositions(vs);

                if(particle!=null)particle.transform.position = CapCurrent.position;
            }
        }

        public void Finish()
        {
            lineRenderer.DOColor(new Color2(Color.white, Color.white), new Color2(new Color(1, 1, 1, 0), new Color(1, 1, 1, 0)), 0.2f);

            //CapEnd.GetComponent<Renderer>().material.DOFade(0, 0.2f);
            CapCurrent.DOScale(1.2f, 0.2f);
            CapCurrent.GetComponentInChildren<Renderer>().material.DOFade(0, 0.2f);

	        if (particle != null)
	        {
		        var emission = particle.GetComponent<ParticleSystem>().emission;
		        emission.rateOverTime = 0;
	        }

	        Observable.Timer(new TimeSpan(0, 0, 0, 0, 200))
                .Subscribe(x => { Pool.Release(gameObject); });
        }

        public void OnPooled()
        {
            if(!User.SimpleEffect)particle = Instantiate<GameObject>(ParticlePrefab);

            lineRenderer.colorGradient = new Gradient()
            {
                colorKeys = new[] { new GradientColorKey(ColorNormal, 0) }
            };
            //CapEnd.GetComponent<Renderer>().material.color = Color.white;
            CapCurrent.transform.localScale = Vector3.one;
            CapCurrent.GetComponentInChildren<Renderer>().material.color = Color.white;

            LateUpdate();
        }

        public void OnReleased()
        {
			if (!User.SimpleEffect) Destroy(particle);
        }
    }
}