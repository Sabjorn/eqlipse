﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UniRx;
using System;
using System.Collections;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace EQLIPSE
{
#if UNITY_EDITOR
    [CustomEditor(typeof(ButtonHoldBehaviour), true)]
    public class ButtonHoldEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
        }
    }
#endif

    [RequireComponent(typeof(Button))]
    public class ButtonHoldBehaviour : Button
    {
        bool isDown;
        float counter;

        public float HoldTime;

        public UnityEvent OnInvoked;
        public FloatEvent Progress;

        protected override void Start()
        {
            base.Start();
            isDown = false;
            counter = 0;
        }

        void Update()
        {
            if (isDown)
            {
                counter += Time.deltaTime;

                if (counter >= HoldTime)
                {
                    isDown = false;
                    OnInvoked.Invoke();
                    counter = 0;
                }

                Progress.Invoke(counter / HoldTime);
            }
            else
            {
                Progress.Invoke(0);
            }
        }

        public override void OnPointerDown(PointerEventData eventData)
        {
            isDown = true;
        }
        public override void OnPointerUp(PointerEventData eventData)
        {
            isDown = false;
            counter = 0;
        }
    }
}