﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace EQLIPSE.Player
{
    [RequireComponent(typeof(Image))]
    public class GaugeUIController : MonoBehaviour
    {
        Image imageComponent;

        public Sprite SpriteNotEnough, SpriteEnough, SpriteHard;

        void Start()
        {
            imageComponent = GetComponent<Image>();
        }
        void Update()
        {
            if (Environment.Player.Gauge != Difficulty.Gauges.Normal)
            {
                if (GaugeManager.Singleton.Gauge < 0.3f)
                {
                    imageComponent.color = new Color(1, 1, 1, Mathf.Sin(Time.time * 5 * 2 * Mathf.PI) >= 0 ? 1 : 0);
                }
                else
                {
                    imageComponent.color = Color.white;
                }
            }
        }

        public void InputGauge(float gauge)
        {
            imageComponent.fillAmount = gauge;

            switch (Environment.Player.Gauge)
            {
                case Difficulty.Gauges.Normal:
                    imageComponent.sprite = gauge >= 0.7f ? SpriteEnough : SpriteNotEnough;
                    break;
                default:
                    imageComponent.sprite = SpriteHard;
                    break;
            }
        }
    }
}