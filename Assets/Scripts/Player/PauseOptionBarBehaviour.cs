﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace EQLIPSE.Player
{
    public class PauseOptionBarBehaviour : MonoBehaviour
    {
        Animator animator;
        public Text HSDisplay;

        void Start()
        {
            animator = GetComponent<Animator>();
        }

        void Update()
        {
            HSDisplay.text = User.HiSpeed.ToString("#0.0");
        }

        public void In()
        {
            animator.SetBool("isIn", true);
        }

        public void Out()
        {
            animator.SetBool("isIn", false);
        }

        public void HSUp() { User.HiSpeed += 0.1f; }
        public void HSDown() { User.HiSpeed -= 0.1f; }

        public void HSNoob() { Environment.Option.HSNoob(); }
        public void HSPro() { Environment.Option.HSPro(); }
    }
}