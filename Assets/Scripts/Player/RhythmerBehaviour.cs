﻿using UnityEngine;
using System.Collections;

namespace EQLIPSE.Player
{
    [RequireComponent(typeof(Renderer))]
    public class RhythmerBehaviour : MonoBehaviour
    {
        public AnimationCurve IntensityCurve;

        new Renderer renderer;
        void Start()
        {
            renderer = GetComponent<Renderer>();
            renderer.material.color = new Color(1, 1, 1, 0);
        }

        void Update()
        {
            if (SongLoader.Singleton.IsLoaded)
            {
                renderer.material.color = new Color(1, 1, 1, IntensityCurve.Evaluate(SongRhythmer.Singleton.Beat >= 0 ? Mathf.Clamp01(SongRhythmer.Singleton.Beat % 1f) : 0));
            }
            else
            {
                renderer.material.color = new Color(1, 1, 1, 0);
            }
        }
    }
}