﻿using UnityEngine;
using System.Collections;

namespace EQLIPSE.Player
{
    [RequireComponent(typeof(AudioSource))]
    public class Clapper : SingletonMonoBehaviour<Clapper>
    {
        AudioSource audioSource;

        public bool Enabled;

        // Use this for initialization
        void Start()
        {
            audioSource = GetComponent<AudioSource>();
        }

        public void Clap()
        {
            if (Enabled) audioSource.Play();
        }
    }
}