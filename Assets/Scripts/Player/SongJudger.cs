﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using EQLIPSE;
using UniRx;
using UnityEngine;
using UnityEngine.Events;

namespace EQLIPSE.Player
{
	public class SongJudger : SingletonMonoBehaviour<SongJudger>
	{
		class SlideTiming : JudgeElement
		{
			public Rational Beat;
			public float Position;
			public SlideNote Parent;
		}
		Chart LoadedChart;

		bool isInitialized;

		public Dictionary<JudgeElement, Judges> JudgeRecord { get; private set; }

		int[] iNoteReach, iNoteBeyond;

		int[] iNotePerfect; // For Clap

		int iTapNoteReach, iTapNoteBeyond;

		int iTapNotePerfect;    //ForClap

		SlideTiming[] slideTimings;
		int iSlideTimingBeyond;

		int[] iChargeNoteReach, iChargeNoteBeyond;

		int[] iChargeNotePerfect;   // For Clap

		int iAutoCommand;

		public ChargeNote[] ChargeNotesJudging { get; private set; }
		Judges?[] chargeNotesJudge;
		float?[] deltaChargeNotePushing;

		public ButtonBehaviour[] Buttons;
		public BarSensor Bar;

		public Difficulty CurrentDifficulty;

		float JudgeOffset { get { return User.JudgeOffset * 0.01f; } }

		Subject<JudgeInfo> judgeSubject;
		public IObservable<JudgeInfo> JudgeObservable { get { return judgeSubject; } }

		Subject<Note> notePerfectSubject;
		public IObservable<Note> NotePerfectObservable { get { return notePerfectSubject; } }

		private IDisposable[] _chargeMarginDisposables = new IDisposable[4];
		void Awake()
		{
			isInitialized = false;

			JudgeRecord = new Dictionary<JudgeElement, Judges>();

			ChargeNotesJudging = new ChargeNote[4] { null, null, null, null };
			chargeNotesJudge = new Judges?[4] { null, null, null, null };
			deltaChargeNotePushing = new float?[4] { null, null, null, null };

			CurrentDifficulty = Difficulty.Normal;

			iNoteReach = new int[4] { 0, 0, 0, 0 };
			iNoteBeyond = new int[4] { 0, 0, 0, 0 };

			iNotePerfect = new int[4] { 0, 0, 0, 0 };

			iChargeNoteReach = new int[4] { 0, 0, 0, 0 };
			iChargeNoteBeyond = new int[4] { 0, 0, 0, 0 };

			iChargeNotePerfect = new int[4] { 0, 0, 0, 0 };

			iTapNoteReach = 0; iTapNoteBeyond = 0;

			iTapNotePerfect = 0;

			iSlideTimingBeyond = 0;

			iAutoCommand = 0;

			judgeSubject = new Subject<JudgeInfo>();
			notePerfectSubject = new Subject<Note>();
		}

		public void Init()
		{
			LoadedChart = SongLoader.Singleton.ChartData;

			// Subscription
			for (int i = 0; i < 4; i++)
			{
				int _i = i; //それぞれに変数を生成

				Buttons[_i].RState
					.Subscribe(b =>
					{
						if (b) OnLaneTouched(_i); else OnLaneReleased(_i);
					})
					.AddTo(this.gameObject);
			}

			#region Slide Note
			slideTimings = LoadedChart.SlideNotes.SelectMany(sn =>
			{
				List<SlideTiming> ret = new List<SlideTiming>();

				for (Rational beat = sn.BeatFrom + new Rational(1, 4); beat <= sn.BeatTo; beat += new Rational(1, 2))
				{
					float position = Mathf.Lerp(sn.ValueFrom, sn.ValueTo, Mathf.InverseLerp(sn.BeatFrom, sn.BeatTo, beat));

					ret.Add(new SlideTiming { Beat = beat, Position = position, Parent = sn });
				}

				return ret;
			}).ToArray();
			#endregion

			JudgeRecord.Clear();

			int idealCombo = 0;

			for (int i = 0; i < 4; i++)
			{
				foreach (var note in LoadedChart.Notes[i])
				{
					JudgeRecord.Add(note, Judges.None);
					idealCombo++;
				}
				foreach (var cNote in LoadedChart.ChargeNotes[i])
				{
					JudgeRecord.Add(cNote, Judges.None);
					idealCombo++;
				}
			}

			foreach (var tNote in LoadedChart.TapNotes)
			{
				JudgeRecord.Add(tNote, Judges.None);
				idealCombo++;
			}
			foreach (var sElem in slideTimings)
			{
				JudgeRecord.Add(sElem, Judges.None);
				idealCombo++;
			}

			ComboManager.Singleton.IdealCombo = idealCombo;


			if (Environment.Player.IsAutoPlay) User.JudgeOffset = 0;

			isInitialized = true;
		}

		void Update()
		{
			if (isInitialized && SongPlayer.Singleton.IsPlaying)
			{
				#region Auto Command
				while (iAutoCommand < LoadedChart.AutoplayCommands.Count && SongRhythmer.BeatToTime(LoadedChart.AutoplayCommands[iAutoCommand].Item1) + JudgeOffset < SongRhythmer.Singleton.Time)
				{
					Environment.Player.IsAutoPlay = LoadedChart.AutoplayCommands[iAutoCommand].Item2;

					iAutoCommand++;
				}
				#endregion

				for (int i = 0; i < 4; i++) //On Each Lane
				{
					#region Note
					while (iNoteReach[i] < LoadedChart.Notes[i].Count
						   && SongPlayer.Singleton.Time - (SongRhythmer.BeatToTime(LoadedChart.Notes[i][iNoteReach[i]].Beat) + JudgeOffset) >= CurrentDifficulty.TimeJudgeDictionary[Judges.Good])
					{
						if (JudgeRecord[LoadedChart.Notes[i][iNoteReach[i]]] == Judges.None)
						{
							SetNoteJudging(LoadedChart.Notes[i][iNoteReach[i]], 0, true);
						}
						iNoteReach[i]++;
					}
					while (iNoteBeyond[i] < LoadedChart.Notes[i].Count
						   && SongRhythmer.BeatToTime(LoadedChart.Notes[i][iNoteBeyond[i]].Beat) + JudgeOffset - SongPlayer.Singleton.Time < CurrentDifficulty.TimeJudgeDictionary[Judges.Good])
					{
						iNoteBeyond[i]++;
					}
					while (iNotePerfect[i] < LoadedChart.Notes[i].Count
						   && SongRhythmer.BeatToTime(LoadedChart.Notes[i][iNotePerfect[i]].Beat) + JudgeOffset - SongPlayer.Singleton.Time < 0)
					{
						if (Environment.Player.IsAutoPlay)
							Buttons[i].Tap();
						//Clapper.Singleton.Clap();
						notePerfectSubject.OnNext(LoadedChart.Notes[i][iNotePerfect[i]]);
						iNotePerfect[i]++;
					}
					#endregion

					#region Charge Note
					while (iChargeNoteReach[i] < LoadedChart.ChargeNotes[i].Count
						   && SongPlayer.Singleton.Time - (SongRhythmer.BeatToTime(LoadedChart.ChargeNotes[i][iChargeNoteReach[i]].Beat[0]) + JudgeOffset) >= CurrentDifficulty.TimeJudgeDictionary[Judges.Good])
					{
						if (JudgeRecord[LoadedChart.ChargeNotes[i][iChargeNoteReach[i]]] == Judges.None && LoadedChart.ChargeNotes[i][iChargeNoteReach[i]] != ChargeNotesJudging[i])
							SetChargeNoteJudging(LoadedChart.ChargeNotes[i][iChargeNoteReach[i]], 0, true);
						iChargeNoteReach[i]++;
					}
					while (iChargeNoteBeyond[i] < LoadedChart.ChargeNotes[i].Count
						   && SongRhythmer.BeatToTime(LoadedChart.ChargeNotes[i][iChargeNoteBeyond[i]].Beat[0]) + JudgeOffset - SongPlayer.Singleton.Time < CurrentDifficulty.TimeJudgeDictionary[Judges.Good])
					{
						iChargeNoteBeyond[i]++;
					}
					while (iChargeNotePerfect[i] < LoadedChart.ChargeNotes[i].Count
						   && SongRhythmer.BeatToTime(LoadedChart.ChargeNotes[i][iChargeNotePerfect[i]].Beat[0]) + JudgeOffset - SongPlayer.Singleton.Time < 0)
					{
						if (Environment.Player.IsAutoPlay)
							Buttons[i].Press();
						//Clapper.Singleton.Clap();
						notePerfectSubject.OnNext(LoadedChart.ChargeNotes[i][iChargeNotePerfect[i]]);
						iChargeNotePerfect[i]++;
					}

					if (ChargeNotesJudging[i] != null && SongRhythmer.BeatToTime(ChargeNotesJudging[i].Beat[1]) + JudgeOffset < SongPlayer.Singleton.Time)
					{
						if (Environment.Player.IsAutoPlay)
							Buttons[i].Release();
						SetChargeNoteJudging(ChargeNotesJudging[i], (float)deltaChargeNotePushing[i], false);
						ChargeNotesJudging[i] = null;
						deltaChargeNotePushing[i] = null;
					}
					#endregion
				}

				#region Slide Note

				while (iSlideTimingBeyond < slideTimings.Length && SongRhythmer.BeatToTime(slideTimings[iSlideTimingBeyond].Beat) + JudgeOffset < SongPlayer.Singleton.Time)
				{
					if (Environment.Player.IsAutoPlay)
						Bar.Hold(slideTimings[iSlideTimingBeyond].Position);
					OnSliderTimingArrive(slideTimings[iSlideTimingBeyond]);
					//Clapper.Singleton.Clap();
					notePerfectSubject.OnNext(slideTimings[iSlideTimingBeyond].Parent);
					iSlideTimingBeyond++;
				}
				#endregion

				#region Tap Note
				while (iTapNoteReach < LoadedChart.TapNotes.Count && SongPlayer.Singleton.Time - (SongRhythmer.BeatToTime(LoadedChart.TapNotes[iTapNoteReach].Beat) + JudgeOffset) >= CurrentDifficulty.TimeJudgeDictionary[Judges.Good])
				{
					if (JudgeRecord[LoadedChart.TapNotes[iTapNoteReach]] == Judges.None)
						SetTapNoteJudging(LoadedChart.TapNotes[iTapNoteReach], 0, 0, true);
					iTapNoteReach++;
				}
				while (iTapNoteBeyond < LoadedChart.TapNotes.Count && SongRhythmer.BeatToTime(LoadedChart.TapNotes[iTapNoteBeyond].Beat) + JudgeOffset - SongPlayer.Singleton.Time < CurrentDifficulty.TimeJudgeDictionary[Judges.Good])
				{
					iTapNoteBeyond++;
				}
				while (iTapNotePerfect < LoadedChart.TapNotes.Count && SongRhythmer.BeatToTime(LoadedChart.TapNotes[iTapNotePerfect].Beat) + JudgeOffset - SongPlayer.Singleton.Time < 0)
				{
					if (Environment.Player.IsAutoPlay)
						Bar.Tap(LoadedChart.TapNotes[iTapNotePerfect].Value);
					//Clapper.Singleton.Clap();
					notePerfectSubject.OnNext(LoadedChart.TapNotes[iTapNotePerfect]);
					iTapNotePerfect++;
				}
				#endregion
			}
		}

		void SetNoteJudging(NormalNote note, float delta, bool isMiss)
		{
			Judges j = isMiss ? Judges.Miss : TimeDeltaToJudge(delta);
			JudgeRecord[note] = j;

			var judgeInfo = new JudgeInfo()
			{
				Judge = j,
				Note = note,
				NoteType = JudgeInfo.NoteTypes.Normal,
				DeltaTime = delta
			};

			judgeSubject.OnNext(judgeInfo);
		}
		void SetChargeNoteJudging(ChargeNote chargeNote, float delta, bool isMiss)
		{
			Judges j = isMiss ? Judges.Miss : TimeDeltaToJudge(delta);
			JudgeRecord[chargeNote] = j;

			var judgeInfo = new JudgeInfo()
			{
				Judge = j,
				Note = chargeNote,
				NoteType = JudgeInfo.NoteTypes.Charge,
				DeltaTime = delta
			};

			judgeSubject.OnNext(judgeInfo);
		}
		void SetSlideTimingJudging(SlideTiming timing, float deltaPosition, bool isMiss)
		{
			Judges j = isMiss ? Judges.SlideMiss : SlideDeltaToJudge(deltaPosition);
			if (!JudgeRecord.ContainsKey(timing)) JudgeRecord[timing] = j;
			JudgeRecord[timing] = j;

			var judgeInfo = new JudgeInfo()
			{
				Judge = j,
				Note = timing.Parent,
				NoteType = JudgeInfo.NoteTypes.Slide,
				Position = timing.Position,
				DeltaPosition = deltaPosition
			};

			judgeSubject.OnNext(judgeInfo);
		}

		void SetTapNoteJudging(TapNote tapNote, float delta, float position, bool isMiss)
		{
			Judges j = isMiss ? Judges.SlideMiss : TimeDeltaToJudge(delta, true);
			JudgeRecord[tapNote] = j;

			var judgeInfo = new JudgeInfo()
			{
				Judge = j,
				Note = tapNote,
				NoteType = JudgeInfo.NoteTypes.Tap,
				DeltaTime = delta
			};

			judgeSubject.OnNext(judgeInfo);
		}

		public Judges TimeDeltaToJudge(float delta, bool isSlide = false)
		{
			float deltaAbs = Math.Abs(delta);

			if (deltaAbs <= CurrentDifficulty.TimeJudgeDictionary[Judges.Perfect])
			{
				return isSlide ? Judges.SlidePerfect : Judges.Perfect;
			}
			else if (deltaAbs <= CurrentDifficulty.TimeJudgeDictionary[Judges.Great])
			{
				return isSlide ? Judges.SlideGreat : Judges.Great;
			}
			else if (deltaAbs <= CurrentDifficulty.TimeJudgeDictionary[Judges.Good])
			{
				return isSlide ? Judges.SlideGood : Judges.Good;
			}
			else
			{
				return isSlide ? Judges.SlideMiss : Judges.Miss;
			}
		}

		public Judges SlideDeltaToJudge(float delta)
		{
			float deltaAbs = Math.Abs(delta);

			if (deltaAbs <= CurrentDifficulty.SliderJudgeDictionary[Judges.Perfect])
			{
				return Judges.SlidePerfect;
			}
			else if (deltaAbs <= CurrentDifficulty.SliderJudgeDictionary[Judges.Great])
			{
				return Judges.SlideGreat;
			}
			else if (deltaAbs <= CurrentDifficulty.SliderJudgeDictionary[Judges.Good])
			{
				return Judges.SlideGood;
			}
			else
			{
				return Judges.SlideMiss;
			}
		}

		public void OnLaneTouched(int i)
		{
			if (isInitialized && SongPlayer.Singleton.IsPlaying)
			{
				NormalNote[] notesCandidate = LoadedChart.Notes[i]
					.Skip(iNoteReach[i])
					.Take(iNoteBeyond[i] - iNoteReach[i])
					.Where(n => JudgeRecord[n] == Judges.None)
					.ToArray();

				ChargeNote[] chargeNotesCandidate = LoadedChart.ChargeNotes[i]
					.Skip(iChargeNoteReach[i])
					.Take(iChargeNoteBeyond[i] - iChargeNoteReach[i])
					.Where(cn => JudgeRecord[cn] == Judges.None)
					.ToArray();

				NormalNote noteNearest = notesCandidate.Length > 0
					? notesCandidate.Nearest(SongPlayer.Singleton.Time, n => SongRhythmer.BeatToTime(n.Beat) + JudgeOffset)
					: null;
				ChargeNote chargeNoteNearest = chargeNotesCandidate.Length > 0
					? chargeNotesCandidate.Nearest(SongPlayer.Singleton.Time, cn => SongRhythmer.BeatToTime(cn.Beat[0]) + JudgeOffset)
					: null;

				if (!(noteNearest == null && chargeNoteNearest == null))
				{
					double dNote = noteNearest != null ? Math.Abs(SongPlayer.Singleton.Time - (SongRhythmer.BeatToTime(noteNearest.Beat) + JudgeOffset)) : 1;
					double dChargeNote = chargeNoteNearest != null ? Math.Abs(SongPlayer.Singleton.Time - (SongRhythmer.BeatToTime(chargeNoteNearest.Beat[0]) + JudgeOffset)) : 1;

					if (dNote <= dChargeNote)
					{
						float delta = (float)(SongPlayer.Singleton.Time - (SongRhythmer.BeatToTime(noteNearest.Beat) + JudgeOffset));
						SetNoteJudging(noteNearest, delta, false);
					}
					else
					{
						float delta = SongPlayer.Singleton.Time - (float)(SongRhythmer.BeatToTime(chargeNoteNearest.Beat[0]) + JudgeOffset);
						ChargeNotesJudging[i] = chargeNoteNearest;
						chargeNotesJudge[i] = TimeDeltaToJudge(delta);
						deltaChargeNotePushing[i] = delta;
					}
				}

				if (_chargeMarginDisposables[i] != null)
				{
					_chargeMarginDisposables[i].Dispose();
					_chargeMarginDisposables[i] = null;
				}
			}
		}

		public void OnLaneReleased(int i)
		{
			if (isInitialized && SongPlayer.Singleton.IsPlaying)
			{
				if (ChargeNotesJudging[i] != null)
				{
					_chargeMarginDisposables[i] = Observable.TimerFrame(2)
						.Subscribe(_ =>
						{
							if (ChargeNotesJudging[i] != null)
							{
								ChargeNote cn = ChargeNotesJudging[i];
								if (SongRhythmer.BeatToTime(cn.Beat[1]) + JudgeOffset - SongPlayer.Singleton.Time < CurrentDifficulty.TimeJudgeDictionary[Judges.Great])
								{
									SetChargeNoteJudging(ChargeNotesJudging[i], (float)deltaChargeNotePushing[i], false);
								}
								else
								{
									SetChargeNoteJudging(ChargeNotesJudging[i], 0, true);
								}

								ChargeNotesJudging[i] = null;
								chargeNotesJudge[i] = null;
								deltaChargeNotePushing[i] = null;
							}
							_chargeMarginDisposables[i] = null;
						});
				}
			}
		}

		public void OnSliderTouched(float position)
		{
			if (isInitialized && SongPlayer.Singleton.IsPlaying)
			{
				TapNote[] tapNotesCandidate = LoadedChart.TapNotes
					.Skip(iTapNoteReach)
					.Take(iTapNoteBeyond - iTapNoteReach)
					.Where(tn => JudgeRecord[tn] == Judges.None)
					.ToArray();
				if (tapNotesCandidate.Length > 0)
				{
					float nearestBeat = tapNotesCandidate.Nearest(SongPlayer.Singleton.Time, tn => SongRhythmer.BeatToTime(tn.Beat) + JudgeOffset).Beat;
					TapNote tapNoteNearest = tapNotesCandidate
						.Where(t => t.Beat == nearestBeat)
						.Nearest(position, tn => tn.Value);

					if (Math.Abs(tapNoteNearest.Value - position) <= 0.2f)
					{
						float delta = (float)(SongPlayer.Singleton.Time - (SongRhythmer.BeatToTime(tapNoteNearest.Beat) + JudgeOffset));
						SetTapNoteJudging(tapNoteNearest, delta, position, false);
					}
				}
			}
		}

		void OnSliderTimingArrive(SlideTiming timing)
		{
			if (isInitialized && SongPlayer.Singleton.IsPlaying)
			{
				if (Bar.Touches.Count > 0)
				{
					float nearestTouch = Bar.Touches.Nearest(timing.Position, v => v);

					SetSlideTimingJudging(timing, timing.Position - nearestTouch, false);
				}
				else { SetSlideTimingJudging(timing, 0, true); }
			}
		}
	}

	public interface JudgeElement { }
}