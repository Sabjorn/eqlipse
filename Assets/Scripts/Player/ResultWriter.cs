﻿using UnityEngine;
using System.Collections;

namespace EQLIPSE.Player
{
	public class ResultWriter : SingletonMonoBehaviour<ResultWriter>
	{
		public void WriteResult()
		{
			Environment.Player.PlayData = new PlayData()
			{
				Title = SongLoader.Singleton.ChartData.Title,
				Difficulty = SongLoader.Singleton.ChartData.Difficulty,
				JudgeCounter = ScoreManager.Singleton.Counter,
				MaxCombo = ComboManager.Singleton.MaxCombo,
				TheoreticalMaxCombo = SongLoader.Singleton.ChartData.NumMaxCombo,
				Gauge = GaugeManager.Singleton.Gauge,
				GaugeType = Environment.Player.Gauge
			};
		}
	}
}