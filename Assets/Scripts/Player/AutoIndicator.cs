﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace EQLIPSE.Player
{
    public class AutoIndicator : MonoBehaviour
    {
        void Update()
        {
            GetComponent<Text>().enabled = Environment.Player.IsAutoPlay;
            GetComponent<Text>().color = new Color(1, 1, 1, Mathf.Abs(Mathf.Sin(Mathf.PI * Time.time)));
        }
    }
}