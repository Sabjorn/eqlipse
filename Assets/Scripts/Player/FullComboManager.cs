﻿using UnityEngine;
using UniRx;
using System.Collections;

namespace EQLIPSE.Player
{
    public class FullComboManager : SingletonMonoBehaviour<FullComboManager>
    {
        [SerializeField]
        ParticleSystem Flare, Bloom, Flash;

        [SerializeField]
        Animator CanvasAnimator;

        void Start()
        {
            ComboManager.Singleton.FullComboObservable
                .Subscribe(_ =>
                {
                    CanvasAnimator.SetTrigger("Full Combo");

                    Flash.Emit(5);
                    Bloom.Emit(50);
                    Flare.Emit(50);
                });
        }
    }
}