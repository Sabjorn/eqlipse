﻿using UnityEngine;
using System.Collections;
using EQLIPSE;
using EQLIPSE.SuperScene;

public class SEBridge : MonoBehaviour
{
    public void Play(int SE)
    {
        SEPlayer.Singleton.Play((SEs)SE);
	}
	public void Play(SEs SE)
	{
		SEPlayer.Singleton.Play(SE);
	}
}
