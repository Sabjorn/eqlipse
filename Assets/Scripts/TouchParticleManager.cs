﻿using UnityEngine;
using System.Collections;
using EQLIPSE;

public class TouchParticleManager : SingletonMonoBehaviour<TouchParticleManager>
{
	[SerializeField]
	private Camera OverlayCamera;
	[SerializeField]
	private ParticleSystem[] TouchParticles;

	void Update()
	{
		if (Environment.EnableTouchParticle)
		{
			foreach (Touch t in Input.touches)
			{
				if (t.phase == TouchPhase.Began)
				{
					foreach (var p in TouchParticles)
					{
						var tp = t.position;
						tp.x /= OverlayCamera.pixelWidth;
						tp.y /= OverlayCamera.pixelHeight;
						var pos = OverlayCamera.ViewportToWorldPoint(tp);
						pos.z = 2;
						p.transform.position = pos;
						p.Play();
					}
				}
			}

			if (Input.GetMouseButtonDown(0))
			{
				foreach (var p in TouchParticles)
				{
					var mp = Input.mousePosition;
					mp.x /= OverlayCamera.pixelWidth;
					mp.y /= OverlayCamera.pixelHeight;
					var pos = OverlayCamera.ViewportToWorldPoint(mp);
					pos.z = 2;
					p.transform.position = pos;
					p.Play();
				}
			}
		}
	}
}
