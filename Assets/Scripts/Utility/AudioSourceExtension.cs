﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UniRx;

namespace EQLIPSE.Utility
{
	[RequireComponent(typeof(AudioSource))]
	public class AudioSourceExtension : MonoBehaviour
	{
		[SerializeField]
		private int Buffers;

		public float Time { get; private set; }

		private AudioSource audioSource;

		private Subject<Tuple<float, float>> timeSubject;

		private bool _isPlaying;
		private bool _hasStarted;
		private float _timeInternal;

		void Awake()
		{
			audioSource = GetComponent<AudioSource>();

			timeSubject = new Subject<Tuple<float, float>>();
			timeSubject
				.Subscribe(t =>
				{
					Time = t.Item2;
				});
				/*
				.Buffer(Buffers, 1)
				.Subscribe(times =>
				{
					double sumprod = 0, sumx = 0, sumy = 0, sumx2 = 0;
					foreach (var t in times)
					{
						sumprod += t.Item1 * t.Item2;
						sumx += t.Item1;
						sumy += t.Item2;
						sumx2 += t.Item1 * t.Item1;
					}

					double slope = (sumprod * times.Count - sumx * sumy) / (sumx2 * times.Count - sumx * sumx);
					double intercept = (sumx2 * sumy - sumprod * sumx) / (sumx2 * times.Count - sumx * sumx);

					//Time = times.Average() + audioSource.pitch * (Buffers * 0.003f);
					Time = (float
					)(intercept + slope * UnityEngine.Time.time);
				});
				*/
			Time = -100;

			_isPlaying = false;
			_hasStarted = false;
			_timeInternal = 0;
		}

		void Update()
		{
			if (_isPlaying)
			{
				if (_timeInternal < 0)
				{
					_timeInternal += UnityEngine.Time.deltaTime * audioSource.pitch;
					timeSubject.OnNext(new Tuple<float, float>(UnityEngine.Time.time, _timeInternal));
				}
				else
				{
					if (!audioSource.isPlaying && !_hasStarted)
					{
						_hasStarted = true;
						audioSource.Play();
					}
					timeSubject.OnNext(new Tuple<float, float>(UnityEngine.Time.time, (float)audioSource.timeSamples / audioSource.clip.frequency));
				}
			}
		}

		public void Unpause()
		{
			_isPlaying = true;
			audioSource.UnPause();
		}

		public void Pause()
		{
			_isPlaying = false;
			audioSource.Pause();
		}

		public void PlayAfter(float time)
		{
			_isPlaying = true;
			_timeInternal = -time;
		}
	}
}