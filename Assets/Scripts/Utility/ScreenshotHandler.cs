﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

namespace EQLIPSE.Utility
{
	public class ScreenshotHandler : SingletonMonoBehaviour<ScreenshotHandler>
	{
		public IObservable<Texture2D> ScreenshotObservable
		{
			get
			{
				return Observable.EveryEndOfFrame().First()
					.Do((_) =>
					{
						var exc = GameObject.FindGameObjectsWithTag("Excluded from Screenshot");
						foreach (var g in exc)
						{
							g.GetComponent<Renderer>().enabled = false;
						}
					})
					.DelayFrame(1, FrameCountType.EndOfFrame)
					.Select(_ =>
					{
						Texture2D screenTexture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
						screenTexture.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
						screenTexture.Apply();

						var exc = GameObject.FindGameObjectsWithTag("Excluded from Screenshot");
						foreach (var g in exc)
						{
							g.GetComponent<Renderer>().enabled = true;
						}

						return screenTexture;
					});
			}
		}
	}
}