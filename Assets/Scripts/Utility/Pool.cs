﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using EQLIPSE.Exception;

namespace EQLIPSE
{
	namespace Exception
	{
		public class NotPoolantException : System.Exception
		{
			public NotPoolantException() : base() { }
			public NotPoolantException(string message) : base(message) { }
			public NotPoolantException(string message, System.Exception inner) : base(message) { }
		}
	}

	namespace Utility
	{
		public static class Pool
		{
			static Dictionary<GameObject, List<GameObject>> activePools = new Dictionary<GameObject, List<GameObject>>();
			static Dictionary<GameObject, List<GameObject>> inactivePools = new Dictionary<GameObject, List<GameObject>>();

			static Dictionary<GameObject, GameObject> prefabDictionary = new Dictionary<GameObject, GameObject>();

			public static GameObject Instantiate(GameObject prefab)
			{
				if (activePools.ContainsKey(prefab))
				{
					if (inactivePools[prefab].Count > 0)
					{
						// Take One GameObject From Inactive Pool.

						GameObject ret = inactivePools[prefab].First();

						inactivePools[prefab].Remove(ret);
						activePools[prefab].Add(ret);

						prefabDictionary[ret] = prefab;

						ret.SetActive(true);

						ExecuteEvents.Execute<IPoolant>(ret, null, (x, y) => { x.OnPooled(); });

						return ret;
					}
					else
					{
						// Newly Instantiate GameObject.

						GameObject ret = GameObject.Instantiate(prefab);
						activePools[prefab].Add(ret);

						prefabDictionary[ret] = prefab;

						ExecuteEvents.Execute<IPoolant>(ret, null, (x, y) => { x.OnPooled(); });

						return ret;
					}
				}
				else
				{
					// Create New Pool for the Prefab.

					activePools[prefab] = new List<GameObject>();
					inactivePools[prefab] = new List<GameObject>();

					GameObject ret = GameObject.Instantiate(prefab);
					activePools[prefab].Add(ret);

					prefabDictionary[ret] = prefab;

					ExecuteEvents.Execute<IPoolant>(ret, null, (x, y) => { x.OnPooled(); });

					return ret;
				}
			}

			public static void Release(GameObject gameObject)
			{
				if (gameObject != null)
				{
					activePools[prefabDictionary[gameObject]].Remove(gameObject);
					inactivePools[prefabDictionary[gameObject]].Add(gameObject);

					ExecuteEvents.Execute<IPoolant>(gameObject, null, (x, y) => x.OnReleased());
					gameObject.SetActive(false);
				}
			}

			public static void ReleasePools()
			{
				var prefabs = activePools.Keys.ToList();
				foreach (var prefab in prefabs)
				{
					var instances = activePools[prefab].ToList();
					foreach (var instance in instances) { Release(instance); }
				}

				activePools.Clear();

				foreach (var e in inactivePools)
				{
					foreach (var g in e.Value) { Object.Destroy(g); }
				}

				inactivePools.Clear();

				prefabDictionary.Clear();
			}
		}

		public interface IPoolant : IEventSystemHandler
		{
			void OnPooled();
			void OnReleased();
		}
	}
}