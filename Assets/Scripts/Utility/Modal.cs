﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using EQLIPSE.SuperScene;
using UnityEngine;
using UniRx;
using UniRx.Triggers;
using UnityEngine.UI;
using Object = UnityEngine.Object;

namespace EQLIPSE.Utility
{
	public static class Modal
	{
		/// <summary>
		/// 非同期処理中の入力保護・画面表示を行う。
		/// </summary>
		/// <param name="message"></param>
		/// <returns>IDisposable。Disposeで保護を解除する。</returns>
		public static IDisposable LoadingLock(string message = null)
		{
			LoadingUiSingleton.Singleton.ShowLoading(true, message);

			return new CompositeDisposable(
				InputGuard.Lock(),
				Disposable.Create(() =>
				{
					LoadingUiSingleton.Singleton.ShowLoading(false);
				})
			);
		}

		/// <summary>
		/// 汎用メッセージダイアログを表示するObservable。
		/// </summary>
		/// <param name="message">表示するメッセージ。</param>
		/// <returns>IObservable&lt;Unit&gt;。</returns>
		public static IObservable<Unit> InfoDialogObservable(string message)
		{
			var modal = ModalUISingleton.Singleton;

			return Observable.Defer(() =>
			{
				var go = Object.Instantiate(modal.InfoDialogPrefab);

				var entity = go.GetComponent<InfoDialogEntity>();
				entity.Message = message;

				return BaseDialogObservable(go, entity);
			});
		}

		/// <summary>
		/// はい・いいえを選択するダイアログを表示するObservable。
		/// </summary>
		/// <param name="message">表示するメッセージ。</param>
		/// <returns></returns>
		public static IObservable<bool> YesNoDialogObservable(string message)
		{
			var modal = ModalUISingleton.Singleton;

			return Observable.Defer(() =>
			{
				var go = Object.Instantiate(modal.YesNoDialogPrefab);

				var entity = go.GetComponent<YesNoDialogEntity>();
				entity.Message = message;

				return BaseDialogObservable(go, entity);
			});
		}

		/// <summary>
		/// 文字列を入力するダイアログを表示するObservable。
		/// </summary>
		/// <param name="message">表示するメッセージ。</param>
		/// <param name="contentType">入力できる文字列の種類。</param>
		/// <returns></returns>
		public static IObservable<string> InputDialogObservable(string message, InputField.ContentType contentType, int maxChar = 0)
		{
			var modal = ModalUISingleton.Singleton;

			return Observable.Defer(() =>
			{
				var go = Object.Instantiate(modal.InputDialogPrefab);

				var entity = go.GetComponent<InputDialogEntity>();
				entity.Message = message;
				entity.ContentType = contentType;
				entity.MaxChar = maxChar;

				return BaseDialogObservable(go, entity);
			});
		}

		/// <summary>
		/// スクリーンショットをツイートするダイアログを表示するObservable。
		/// </summary>
		/// <param name="screenshot">表示するスクリーンショット。</param>
		/// <param name="header">表示するヘッダ。</param>
		/// <returns></returns>
		public static IObservable<string> ShareDialogObservable(Texture2D screenshot, string header)
		{
			var modal = ModalUISingleton.Singleton;

			return Observable.Defer(() =>
			{
				var go = Object.Instantiate(modal.ShareDialogPrefab);

				var entity = go.GetComponent<ShareDialogEntity>();
				entity.Header = header;
				entity.Footer = "#EQLIPSE";
				entity.ScreenShot = Sprite.Create(screenshot, new Rect(0, 0, screenshot.width, screenshot.height), Vector2.zero);

				return BaseDialogObservable(go, entity);
			});
		}

		/// <summary>
		/// 楽曲を現すダイアログを表示するObservable。
		/// </summary>
		/// <param name="song">表示する楽曲。</param>
		/// <param name="message">表示するメッセージ。</param>
		/// <returns></returns>
		public static IObservable<Unit> SongDialogObservable(Song song, string message)
		{
			var modal = ModalUISingleton.Singleton;

			return Observable.Defer(() =>
			{
				var go = Object.Instantiate(modal.SongDialogPrefab);

				var entity = go.GetComponent<SongDialogEntity>();
				entity.Message = message;
				entity.Song = song;

				return BaseDialogObservable(go, entity);
			});

		}

		/// <summary>
		/// ダイアログの基礎となるObservable。
		/// ModalObservableに既存GameObjectの設定・破棄の機能が加わる。
		/// </summary>
		/// <param name="dialogObject">ダイアログのGameObject。</param>
		/// <param name="entity">ダイアログが持つIModalEntity。</param>
		/// <returns></returns>
		private static IObservable<T> BaseDialogObservable<T>(GameObject dialogObject, IModalEntity<T> entity)
		{
			var modalTransform = ModalUISingleton.Singleton.transform;

			dialogObject.transform.SetParent(modalTransform, false);

			return ObservableEx.Using(Disposable.Create(() =>
			{
				Object.Destroy(dialogObject);
			}), ModalObservable(entity));
		}

		/// <summary>
		/// モーダルウィンドウを表示するObservable。
		/// IModalEntityのCompletionObservableを入力保護と画面効果でラッピングする。
		/// </summary>
		/// <param name="entity">ウィンドウが持つIModalEntity。</param>
		/// <returns></returns>
		private static IObservable<T> ModalObservable<T>(IModalEntity<T> entity)
		{
			return ObservableEx.Using(InputGuard.Lock(entity.CanvasGroup),
				Observable.Defer(() =>
					{
						entity.CanvasGroup.alpha = 0;
						entity.CanvasGroup.DOFade(1f, 0.2f).SetUpdate(true);

						return entity.CompletionObservable;
					})
					.First()
					.Do(_ =>
					{
						entity.CanvasGroup.DOFade(0f, 0.2f).SetUpdate(true);
					})
					.Delay(TimeSpan.FromSeconds(0.2), Scheduler.MainThreadIgnoreTimeScale));
		}
	}
}