﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

class TextMeshHelper
{
    static Mesh DuplicateMesh(Mesh mesh)
    {
        Mesh newmesh = new Mesh();
        newmesh.vertices = (Vector3[])mesh.vertices.Clone();
        newmesh.normals = (Vector3[])mesh.normals.Clone();
        newmesh.tangents = (Vector4[])mesh.tangents.Clone();
        newmesh.uv = (Vector2[])mesh.uv.Clone();
        newmesh.uv2 = (Vector2[])mesh.uv2.Clone();
        newmesh.colors32 = (Color32[])mesh.colors32.Clone();
        newmesh.subMeshCount = mesh.subMeshCount;
        for (int i = 0; i < mesh.subMeshCount; ++i)
            newmesh.SetTriangles(mesh.GetTriangles(i), i);
        newmesh.boneWeights = (BoneWeight[])mesh.boneWeights.Clone();
        newmesh.bindposes = (Matrix4x4[])mesh.bindposes.Clone();
        return newmesh;
    }

    Mesh _mesh, _processedMesh;
    public Mesh Mesh
    {
        get { return _mesh; }
        set
        {
            _mesh = value;

            _processedMesh = DuplicateMesh(_mesh);

            Scales = new float[CharCount];
            for (int i = 0; i < CharCount; i++)
                Scales[i] = 1.0f;

            Colors = new Color[CharCount];
            for (int i = 0; i < CharCount; i++)
                Colors[i] = Color.white;

            Offset = new Vector3[CharCount];
        }
    }
    public Mesh ProcessedMesh { get { return _processedMesh; } }

    public int CharCount { get { return _mesh.vertexCount / 4; } }
    public Vector3[] Offset;
    public float[] Scales;
    public Color[] Colors;

    public TextMeshHelper()
    {
        Mesh = new Mesh();
    }
    public TextMeshHelper(Mesh mesh)
    {
        Mesh = mesh;
    }

    public void Update()
    {
        Vector3[] vertices = _mesh.vertices;
        Color32[] colors = _mesh.colors32;
        for (int i = 0; i < CharCount; i++)
        {
            Vector3 center = (vertices[i * 4] + vertices[i * 4 + 1] + vertices[i * 4 + 2] + vertices[i * 4 + 3]) / 4;
            for (int j = 0; j < 4; j++)
            {
                vertices[i * 4 + j] = (vertices[i * 4 + j] - center) * Scales[i] + center + Offset[i];
                colors[i * 4 + j] = Colors[i];
            }
        }
        _processedMesh.vertices = vertices;
        _processedMesh.colors32 = colors;
    }
}

[RequireComponent(typeof(Text))]
public class TextAnimator : MonoBehaviour, IMeshModifier
{
    public float Position;
    public float Speed;
    public float AnimateWords;

    public AnimationCurve ScaleCurve;
    public AnimationCurve XCurve, YCurve;
    public AnimationCurve AlphaCurve;

    CanvasRenderer canvasRenderer;
    Text text;
    TextMeshHelper tHelper;

    // Use this for initialization
    void Start()
    {
        canvasRenderer = GetComponent<CanvasRenderer>();
        text = GetComponent<Text>();
        tHelper = new TextMeshHelper();
    }

    // Update is called once per frame
    void LateUpdate()
    {
        tHelper.Update();
        canvasRenderer.SetMesh(tHelper.ProcessedMesh);
    }

    [ContextMenu("Play")]
    public void Play(string str)
    {
        StopAllCoroutines();
        StartCoroutine(PlayCoroutine(str));
    }

    IEnumerator PlayCoroutine(string str)
    {
        float cue = Time.time;

        Color[] initColors = (Color[])tHelper.Colors.Clone();
        while(Time.time < cue + 0.5f)
        {
            float t = Mathf.InverseLerp(cue, cue + 0.5f, Time.time);

            for(int i = 0; i < tHelper.CharCount; i++)
            {
                tHelper.Colors[i] = Color.Lerp(initColors[i], new Color(1, 1, 1, 0), AlphaCurve.Evaluate(t));
            }

            yield return null;
        }

        text.text = str;

        Position = 0;
        while (Position < tHelper.CharCount + AnimateWords)
        {
            Position += Speed * Time.deltaTime;

            for (int i = 0; i < tHelper.CharCount; i++)
            {
                float t = Mathf.Clamp01(Mathf.InverseLerp(i, i + AnimateWords, Position));
                tHelper.Scales[i] = ScaleCurve.Evaluate(t);
                tHelper.Offset[i] = new Vector3(XCurve.Evaluate(t), YCurve.Evaluate(t), 0);

                tHelper.Colors[i] = new Color(1, 1, 1, AlphaCurve.Evaluate(t));
            }

            yield return null;
        }
    }

    public void ModifyMesh(Mesh mesh)
    {
    }
    public void ModifyMesh(VertexHelper vHelper)
    {
        if (tHelper != null)
        {
            Mesh m = new Mesh();
            vHelper.FillMesh(m);
            tHelper.Mesh = m;

            vHelper.Clear();
        }
    }
}
