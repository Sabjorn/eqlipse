﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System;
using System.Collections;
using System.Linq;
using System.Text;
using DG.Tweening;
using EQLIPSE;
using EQLIPSE.Player;
using EQLIPSE.SuperScene;
using Environment = EQLIPSE.Environment;

public class ScenarioMessageBehaviour : SingletonMonoBehaviour<ScenarioMessageBehaviour>
{
	[SerializeField]
	MessageViewAnimator MessageViewAnimator;
	[SerializeField]
	Image Background, Background_Back;
	[SerializeField]
	FaceAnimator FaceAnimatorLeft, FaceAnimatorRight;
	[SerializeField]
	Text Name;
	[SerializeField]
	TextAnimator BodyTextAnimator;

	public UnityEvent OnEnd;

	bool isSleeping, isStarted;

	string FaceLeft, FaceRight;

	private Seriph Seriph
	{
		set
		{
			if (!string.IsNullOrEmpty(value.Song))
			{
				Debug.Log("Go to Selector.");
				Environment.Selector.AvailableSongs = Environment.Songs.Where(s => s.Title == value.Song).ToDictionary(s => s, s => new[] { true, true, true });
				SceneTransitioner.Singleton.Next();
			}
			else
			{

				if (value.Name == "NULL") { MessageViewAnimator.Active = false; }
				else
				{
					MessageViewAnimator.Active = true;
					if (!string.IsNullOrEmpty(value.Name)) { Name.text = value.Name; }
				}

				BodyTextAnimator.Play(value.Body);

				if (value.Background == "NULL")
				{
					Background_Back.color = Color.clear;
					Background.DOFade(0, 0.5f);
				}
				else
				{
					if (Environment.Story.BGImagesDictionary.ContainsKey(value.Background))
					{
						Background_Back.sprite = Background.sprite;
						if (Background_Back.sprite != null) Background_Back.color = Color.white;
						Background.sprite = Environment.Story.BGImagesDictionary[value.Background];
						Background.color = new Color(1, 1, 1, 0);
						Background.DOFade(1, 0.5f);
					}
				}

				if (!string.IsNullOrEmpty(value.FaceLeft))
				{
					FaceAnimatorLeft.Face = value.FaceLeft;
				}
				if (value.EmotionLeft != null)
				{
					FaceAnimatorLeft.Emotion = (Seriph.Emotions)value.EmotionLeft;
				}

				if (!string.IsNullOrEmpty(value.FaceRight))
				{
					FaceAnimatorRight.Face = value.FaceRight;
				}
				if (value.EmotionRight != null)
				{
					FaceAnimatorRight.Emotion = (Seriph.Emotions)value.EmotionRight;
				}

				if (value.Position != null)
				{
					switch (value.Position)
					{
						case Seriph.Positions.None:
							FaceAnimatorLeft.Active = false;
							FaceAnimatorRight.Active = false;
							break;
						case Seriph.Positions.Left:
							FaceAnimatorLeft.Active = true;
							FaceAnimatorRight.Active = false;
							break;
						case Seriph.Positions.Right:
							FaceAnimatorLeft.Active = false;
							FaceAnimatorRight.Active = true;
							break;
						case Seriph.Positions.Both:
							FaceAnimatorLeft.Active = true;
							FaceAnimatorRight.Active = true;
							break;
					}
				}
			}
		}
	}

	void Start()
	{
		isSleeping = false;
		isStarted = false;

		if (SuperSceneManager.Singleton.CurrentState != "Tutorial")
		{
			Next();
			isStarted = true;
		}
		else
		{
			isSleeping = true;
		}
	}

	void Update()
	{
		if (SongPlayer.Singleton != null)
		{
			if (SongPlayer.Singleton.IsPlaying && !isStarted && SongPlayer.Singleton.Time >= 0)
			{
				Next();
				isStarted = true;
			}
		}
	}

	void Next()
	{
		Debug.Log("Seriph #" + Environment.Story.SeriphPosition + " out of " + Environment.Story.Scenario.Seriphs.Length);
		if (Environment.Story.SeriphPosition < Environment.Story.Scenario.Seriphs.Length)
		{
			var s = Environment.Story.Scenario.Seriphs[Environment.Story.SeriphPosition];
			Seriph = s;
			if (s.Duration != null) StartCoroutine(SleepCoroutine((int)s.Duration));

			Environment.Story.SeriphPosition++;
		}
		else { OnEnd.Invoke(); }

	}

	public IEnumerator SleepCoroutine(float time)
	{
		isSleeping = true;
		yield return new WaitForSeconds(time);
		isSleeping = false;
		Next();
	}

	public void OnTouched()
	{
		if (!isSleeping) Next();
	}
}
