﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

[RequireComponent(typeof(RectTransform))]
[RequireComponent(typeof(CanvasGroup))]
public class MessageViewAnimator : MonoBehaviour {
    RectTransform _rTransform;
    RectTransform RectTransform
    {
        get
        {
            if (_rTransform == null) _rTransform = GetComponent<RectTransform>();
            return _rTransform;
        }
    }

    CanvasGroup _canvasGroup;
    CanvasGroup CanvasGroup
    {
        get
        {
            if (_canvasGroup == null) _canvasGroup = GetComponent<CanvasGroup>();
            return _canvasGroup;
        }
    }

    bool _active;
    public bool Active
    {
        get { return _active; }
        set
        {
            _active = value;
            RectTransform.DOAnchorPos(value ? Vector2.zero : new Vector2(0, -100), 0.5f);
            CanvasGroup.DOFade(value ? 1 : 0, 0.5f);
        }
    }
}
