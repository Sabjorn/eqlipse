﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;
using EQLIPSE;

[RequireComponent(typeof(RectTransform))]
[RequireComponent(typeof(Image))]
public class FaceAnimator : MonoBehaviour
{
    public Vector2 AnchorPositionActive, AnchorPositionInactive;

    RectTransform _rTransform;
    RectTransform RectTransform
    {
        get
        {
            if (_rTransform == null) _rTransform = GetComponent<RectTransform>();
            return _rTransform;
        }
    }
    Image _image;
    Image Image
    {
        get
        {
            if (_image == null) _image = GetComponent<Image>();
            return _image;
        }
    }

    bool _active;
    public bool Active
    {
        get { return _active; }
        set
        {
            _active = value;
            RectTransform.DOAnchorPos(value ? AnchorPositionActive : AnchorPositionInactive, 0.2f);
			if (Face != "NULL")
			{
				Image.DOFade(value ? 1 : 0.5f, 0.2f);
			}
			else
			{
				Image.DOFade(0f, 0.2f);
			}
        }
    }

    string _face;
    public string Face
    {
        get { return _face; }
        set
        {
            _face = value;
            if (value == "NULL")
            {
                Active = false;
                Image.DOFade(0, 0.2f);
            }
            else if (Environment.Story.FaceSpritesDictionary.ContainsKey(value))
            {
                Active = Active;
                Image.sprite = Environment.Story.FaceSpritesDictionary[value][Emotion];
            }
            else
            {
                Active = Active;
                Image.sprite = Environment.Story.FaceSpritesDictionary[value][Seriph.Emotions.Normal];
            }
        }
    }

    Seriph.Emotions _emotion;
    public Seriph.Emotions Emotion
    {
        get { return _emotion; }
        set
        {
            _emotion = value;
            if (Environment.Story.FaceSpritesDictionary[Face].ContainsKey(value))
            {
                Image.sprite = Environment.Story.FaceSpritesDictionary[Face][value];
            }
        }
    }

	void Awake()
	{
		Face = "NULL";
	}
}
