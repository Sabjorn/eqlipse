﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using JetBrains.Annotations;
using UnityEngine;

namespace EQLIPSE.Option
{
	public class SelectorBehaviour : MonoBehaviour
	{
		[SerializeField] private Transform Contents;

		private string showingEntry;

		void Awake()
		{
			showingEntry = null;
		}

		public void Show(string entry)
		{
			if (showingEntry != null)
			{
				Contents.Find(showingEntry).GetComponent<IOptionEntry>().Hide();
			}

			showingEntry = entry;

			var _gameObject = Contents.Find(showingEntry);
			if (_gameObject == null)
			{
				Debug.LogWarning("GameObjectが見つかりません: " + showingEntry);
				showingEntry = null;
				return;
			}

			var component = _gameObject.GetComponent<IOptionEntry>();
			if (component == null)
			{
				Debug.LogWarning("IOptionEntryを実装したMonoBehaviourが見つかりません: " + showingEntry);
				showingEntry = null;
				return;
			}

			component.Show();
		}
	}

	public interface IOptionEntry
	{
		void Show();
		void Hide();
	}
}