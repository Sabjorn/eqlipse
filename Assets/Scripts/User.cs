﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using DG.Tweening.Core.Easing;
using EQLIPSE.SuperScene;
using EQLIPSE.Utility;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Utilities;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace EQLIPSE
{
	public static class User
	{
		public static string Uid, Name;
		public static float Rate
		{
			get
			{
				var baseScore = PlayDatas
					.GroupBy(e => new Tuple<string, Chart.Difficulties>(e.Title, e.Difficulty))
					.Select(group =>
					{
						var title = @group.Key.Item1;
						var difficulty = @group.Key.Item2;

						if (Environment.Songs.Any(s => s.Title == title))
						{
							var song = Environment.Songs.First(s => s.Title == title);
							float constant = song.LevelDictionary[difficulty];
							if (song.ConstDictionary.ContainsKey(difficulty))
								constant = song.ConstDictionary[difficulty];

							float rate;
							if (@group.Any(data => data.IsFormerlyClear))
							{
								var score = @group.Where(data => data.IsFormerlyClear)
									.Max(e => e.Score);
								rate = Mathf.Max((score >= 600000 ? (score - 600000) / 50000f : (score - 600000) / 25000f) + constant, 0);

								if (score >= 995000) rate += 1.0f;
								else if (score >= 990000) rate += 0.6f;
								else if (score >= 980000) rate += 0.3f;
							}
							else rate = 0;

							return rate;
						}
						else return 0;
					})
					.OrderByDescending(rate => rate)
					.Take(20)
					.Sum();

				var fullComboScore = PlayDatas
					.Where(e => e.IsFullCombo)
					.GroupBy(e => new Tuple<string, Chart.Difficulties>(e.Title, e.Difficulty))
					.Select(group =>
					{
						var first = @group.First();
						var difficulty = first.Difficulty;

						var song = Environment.Songs.First(s => s.Title == first.Title);
						float constant = song.LevelDictionary[difficulty];
						if (song.ConstDictionary.ContainsKey(difficulty))
							constant = song.ConstDictionary[difficulty];

						var rate = Mathf.Max(Mathf.Pow(2, constant) / 51200f, 0.001f);

						return rate;
					})
					.Sum();

				return baseScore + fullComboScore;
			}
		}

		public static float HiSpeed = 2.0f;
		public static int JudgeOffset = 0;
		public static int AudioOffset = 0;
		public static int DisplayingJudgeOffset { get { return JudgeOffset - 6; } }
		public static int LaneSize = 0;

		public static bool SimpleEffect = false;
		public static bool AdjustAudio = false;

		public static int Orientation = 0;

		public static List<Record> Records;
		public static Dictionary<string, Chart.Difficulties?> UnlockedSongs = new Dictionary<string, Chart.Difficulties?>();
		public static List<string> RivalUids;

		public static List<PlayData> PlayDatas = new List<PlayData>();

		public static class Twitter
		{
			public static string AccessToken;
			public static string AccessSecret;
			public static string Id;
			public static string Name;

			public static bool HasAccount
			{
				get
				{
					return !String.IsNullOrEmpty(Id) && !String.IsNullOrEmpty(AccessToken) && !String.IsNullOrEmpty(AccessSecret);
				}
			}

			public static IObservable<Unit> FetchProfileObservable()
			{
				return Observable.Defer(() =>
					{
						if (String.IsNullOrEmpty(AccessToken) || String.IsNullOrEmpty(AccessSecret))
							return Observable.Throw<string>(new System.Exception("Twitterのトークンが取得されていません。"));
						return ObservableWWW.Get(Environment.WwwApiHost + "/twitter/verify?oauth_token=" + AccessToken +
												 "&oauth_token_secret=" + AccessSecret);
					})
					.Do(response =>
					{
						var jObj = JObject.Parse(response);
						Id = (string)jObj["id"];
						Name = (string)jObj["screen_name"];
					})
					.AsUnitObservable();
			}

			public static IEnumerator FetchAccessTokenWizard(IObserver<Unit> observer)
			{
				// リクエストトークン取得
				WWW wwwRequestToken = new WWW(Environment.WwwApiHost + "/twitter/request/true");
				using (Modal.LoadingLock())
				{
					yield return wwwRequestToken;
				}
				if (!String.IsNullOrEmpty(wwwRequestToken.error))
				{
					observer.OnError(new System.Exception(wwwRequestToken.error));
					observer.OnCompleted();

					yield break;
				}
				var requestToken = (string)JToken.Parse(wwwRequestToken.text)["oauth_token"];

				// アクセストークン取得
				yield return Modal.InfoDialogObservable("ブラウザで確認画面が開きます。ログイン後に表示されるPINコードを記録してください。").StartAsCoroutine();

				Application.OpenURL(Environment.WwwApiHost + "/twitter/authenticate?oauth_token=" + requestToken);

				string pin = "";
				yield return
					Modal.InputDialogObservable("表示されたPINコードを入力してください。", InputField.ContentType.IntegerNumber).StartAsCoroutine(str =>
					{
						pin = str;
					});

				WWW wwwAccessToken = new WWW(Environment.WwwApiHost + "/twitter/access?oauth_token=" + requestToken + "&oauth_verifier=" + pin);
				using (Modal.LoadingLock())
				{
					yield return wwwAccessToken;
				}
				if (!String.IsNullOrEmpty(wwwAccessToken.error))
				{
					observer.OnError(new System.Exception(wwwAccessToken.error));
					observer.OnCompleted();

					yield break;
				}

				AccessToken = (string)JToken.Parse(wwwAccessToken.text)["oauth_token"];
				AccessSecret = (string)JToken.Parse(wwwAccessToken.text)["oauth_token_secret"];

				observer.OnNext(Unit.Default);
				observer.OnCompleted();
			}

			public static IEnumerator LinkWizard()
			{
				// Twitterログイン、アクセストークン・シークレット取得
				yield return Observable.FromCoroutine<Unit>(FetchAccessTokenWizard).ToYieldInstruction();

				// Twitterプロフィール取得
				yield return FetchProfileObservable().ToYieldInstruction();

				// Twitterアカウント連携試行
				var linkInst = LinkObservable().ToYieldInstruction();
				yield return linkInst;

				if (linkInst.HasError)
				{
					var wwwError = linkInst.Error as WWWErrorException;
					if (wwwError != null && wwwError.StatusCode == HttpStatusCode.Conflict)
					{
						// Twitterアカウントが重複

						var selection = false;
						yield return
							Modal.YesNoDialogObservable(
									"既に同じTwitter IDで登録されているユーザーが見つかりました。\n現在のユーザーの記録をすべてこのユーザーへ移管し、現在のユーザーを削除しますがよろしいですか？")
								.StartAsCoroutine(b =>
								{
									selection = b;
								});
						if (!selection) yield break;

						linkInst = LinkObservable(true).ToYieldInstruction();
						yield return linkInst;
					}
				}

				Save();
			}

			public static IObservable<string> LinkObservable(bool confirmed = false)
			{
				WWWForm form = new WWWForm();
				form.AddField("uid", Uid);
				form.AddField("twitter_id", Id);
				form.AddField("twitter_token", AccessToken);
				form.AddField("twitter_secret", AccessSecret);
				return ObservableWWW.Post(Environment.WwwApiHost + "/users/link_twitter2/" + confirmed, form)
					.Do(response =>
					{
						var token = JToken.Parse(response);
						ParseJToken(token);
					});
			}

			public static IObservable<string> UploadStatusWithPictureObservable(string status, string base64)
			{
				var form = new WWWForm();
				form.AddField("status", status);
				form.AddField("media_data", base64);

				return
					ObservableWWW.Post(
						Environment.WwwApiHost + "/twitter/share_result?oauth_token=" + AccessToken + "&oauth_token_secret=" + AccessSecret,
						form);
			}

			public static IObservable<Unit> FetchUserDataObservable()
			{
				if (!HasAccount) return Observable.Throw<Unit>(new System.Exception("Twitterアカウントが連携されていません"));
				return ObservableWWW.Get(Environment.WwwApiHost + "/users/view?twitter_id=" + Id)
					.Do(response =>
					{
						ParseJToken(JToken.Parse(response));
					}).AsUnitObservable();
			}
		}

		public static IObservable<Unit> FetchUserDataObservable()
		{
			return ObservableWWW.Get(Environment.WwwApiHost + "/users/view/" + Uid)
				.Do(response =>
				{
					ParseJToken(JToken.Parse(response));
				}).AsUnitObservable();
		}

		public static IObservable<Unit> FetchRecordsObservable()
		{
			return ObservableWWW.Get(Environment.WwwApiHost + "/users/records/" + Uid)
				.Do(response =>
				{
					var jObj = JToken.Parse(response);

					Records.Clear();
					foreach (var e in jObj)
					{
						var r = Record.JTokenToRecord(e);

						Records.Add(r);
					}
				})
				.DoOnError(Debug.LogError).AsUnitObservable();
		}

		public static IObservable<Unit> FetchPlayDataObservable()
		{
			return ObservableWWW.Get(Environment.WwwApiHost + "/scores/play_data/" + Uid)
				.Do(response =>
				{
					PlayDatas = new List<PlayData>();

					var token = JToken.Parse(response);
					foreach (var e in token)
					{
						PlayDatas.Add(PlayData.FromJToken(e));
					}

					Debug.Log("プレイデータ数: " + PlayDatas.Count);
				})
				.AsUnitObservable();
		}

		public static void EnqueueUserData()
		{
			WWWPostQueue.Enqueue(new WWWPostEntity()
			{
				Url = Environment.WwwApiHost + "/users/update/",
				Data = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(new
				{
					uid = Uid,
					name = Name,
					twitter_id = Twitter.Id ?? "",
					twitter_token = Twitter.AccessToken ?? "",
					twitter_secret = Twitter.AccessSecret ?? "",
					hispeed = HiSpeed,
					offset = JudgeOffset,
					audio_offset = AudioOffset,
					unlocked = JsonConvert.SerializeObject(UnlockedSongs),
					rivals = JsonConvert.SerializeObject(RivalUids),
					rate = Rate,
					lane_size = LaneSize,
					simple_effect = SimpleEffect ? 1 : 0
				})),
				Headers = new Dictionary<string, string>()
				{
					{"Content-Type", "application/json; charset=utf-8"}
				}
			});
		}

		public static void ParseJToken(JToken jToken)
		{
			AotHelper.EnsureDictionary<string, Chart.Difficulties?>();

			var unlocked = new Dictionary<string, Chart.Difficulties?>();
			try
			{
				unlocked = jToken["unlocked"].ToObject<Dictionary<string, Chart.Difficulties?>>();
				unlocked = unlocked ?? new Dictionary<string, Chart.Difficulties?>();
			}
			catch (System.Exception e)
			{
				Debug.LogWarning("解禁曲解析失敗\n" + e);
			}

			List<string> rivals = new List<string>();
			try
			{
				rivals.AddRange(jToken["rivals"].Select(token => token.ToString()));
			}
			catch (System.Exception e)
			{
				Debug.LogWarning("ライバル解析失敗\n" + e);
			}

			Uid = (string)jToken["uid"];
			Name = (string)jToken["name"];
			Twitter.Id = (string)jToken["twitter_id"];
			Twitter.AccessToken = (string)jToken["twitter_token"];
			Twitter.AccessSecret = (string)jToken["twitter_secret"];
			HiSpeed = (float)jToken["hispeed"];
			JudgeOffset = (int)jToken["offset"];
			AudioOffset = (int)(jToken["audio_offset"] ?? 0);
			UnlockedSongs = unlocked;
			RivalUids = rivals;
			LaneSize = (int)(jToken["lane_size"] ?? 0);
			SimpleEffect = (bool)(jToken["simple_effect"] ?? false);
		}

		public static void Load()
		{
			if (PlayerPrefs.HasKey("UID"))
			{
				Uid = PlayerPrefs.GetString("UID");
				Name = PlayerPrefs.GetString("Name");
				HiSpeed = PlayerPrefs.GetFloat("HiSpeed");
				JudgeOffset = PlayerPrefs.GetInt("JudgeOffset");
				AudioOffset = PlayerPrefs.GetInt("AudioOffset");
				LaneSize = PlayerPrefs.GetInt("LaneSize");

				SimpleEffect = PlayerPrefs.GetInt("SimpleEffect") == 1;
				AdjustAudio = PlayerPrefs.GetInt("AdjustAudio") == 1;

				Orientation = PlayerPrefs.GetInt("Orientation", 0);

				// 解禁状況
				Dictionary<string, Chart.Difficulties?> unlocked;
				try
				{
					unlocked = JToken.Parse(PlayerPrefs.GetString("Unlocked")).ToObject<Dictionary<string, Chart.Difficulties?>>();
				}
				catch (System.Exception e)
				{
					Debug.LogWarning("解禁曲読み込み失敗\n" + e);
					unlocked = new Dictionary<string, Chart.Difficulties?>();
				}
				UnlockedSongs = unlocked;

				// プレイデータ
				List<PlayData> playDatas;
				try
				{
					playDatas = JToken.Parse(PlayerPrefs.GetString("PlayDatas")).ToObject<List<PlayData>>();
				}
				catch (System.Exception e)
				{
					Debug.LogWarning("プレイデータ読み込み失敗\n" + e);
					playDatas = new List<PlayData>();
				}
				PlayDatas = playDatas;

				// ライバル
				List<string> rivals;
				try
				{
					rivals = JToken.Parse(PlayerPrefs.GetString("Rivals")).ToObject<List<string>>();
				}
				catch (System.Exception e)
				{
					Debug.LogWarning("ライバルデータ読み込み失敗\n" + e);
					rivals = new List<string>();
				}
				RivalUids = rivals;

				Twitter.AccessToken = PlayerPrefs.GetString("Twitter Access Token");
				Twitter.AccessSecret = PlayerPrefs.GetString("Twitter Access Secret");
				Twitter.Id = PlayerPrefs.GetString("Twitter ID");
				Twitter.Name = PlayerPrefs.GetString("Twitter Screen Name");
			}
			else
			{
				Clear();
			}
		}

		public static void Clear()
		{
			Uid = null;
			Name = null;
			PlayDatas = new List<PlayData>();
			Records = null;
			UnlockedSongs = null;
			RivalUids = null;
			HiSpeed = 2.0f;
			JudgeOffset = 0;
			AudioOffset = 0;
			LaneSize = 0;
			Orientation = 0;

			SimpleEffect = false;
			AdjustAudio = false;

			Twitter.AccessToken = null;
			Twitter.AccessSecret = null;
			Twitter.Id = null;
			Twitter.Name = null;
		}

		public static void Save()
		{
			PlayerPrefs.SetString("UID", Uid);
			PlayerPrefs.SetString("Name", Name);
			PlayerPrefs.SetFloat("HiSpeed", HiSpeed);
			PlayerPrefs.SetInt("JudgeOffset", JudgeOffset);
			PlayerPrefs.SetInt("AudioOffset", AudioOffset);
			PlayerPrefs.SetInt("LaneSize", LaneSize);
			PlayerPrefs.SetString("PlayDatas", JsonConvert.SerializeObject(PlayDatas));
			PlayerPrefs.SetString("Unlocked", JsonConvert.SerializeObject(UnlockedSongs));
			PlayerPrefs.SetString("Rivals", JsonConvert.SerializeObject(RivalUids));

			PlayerPrefs.SetInt("SimpleEffect", SimpleEffect ? 1 : 0);
			PlayerPrefs.SetInt("AdjustAudio", AdjustAudio ? 1 : 0);
			PlayerPrefs.SetInt("Orientation", Orientation);

			PlayerPrefs.SetString("Twitter Access Token", Twitter.AccessToken);
			PlayerPrefs.SetString("Twitter Access Secret", Twitter.AccessSecret);
			PlayerPrefs.SetString("Twitter ID", Twitter.Id);
			PlayerPrefs.SetString("Twitter Screen Name", Twitter.Name);

			PlayerPrefs.Save();
		}

		public static void Evaluate(PlayData played = null)
		{
			foreach (var song in Environment.Songs.Where(s => s.UnlockConditions.Any()))
			{
				bool flag = true;
				bool playingFlag = false;
				foreach (var e in song.UnlockConditions)
				{
					switch (e.Item1)
					{
						case UnlockCondition.Rate:
							flag &= User.Rate >= Single.Parse(e.Item2);
							break;
						case UnlockCondition.Cleared:
							{
								var data = e.Item2.Split('|');
								flag &= User.PlayDatas.Any(
									r => r.Title == data[0]
										 && (int)r.Difficulty >= (int)Enum.Parse(typeof(Chart.Difficulties), data[1], true)
										 && r.IsFormerlyClear);
							}
							break;
						case UnlockCondition.Unlocked:
							flag &= User.UnlockedSongs.ContainsKey(e.Item2);
							break;
						case UnlockCondition.Playing:
							if (played == null)
								break;
							{
								var data = e.Item2.Split('|');
								playingFlag |= played.IsClear && played.Title == data[0] && played.Difficulty ==
										(Chart.Difficulties)Enum.Parse(typeof(Chart.Difficulties), data[1], true);
							}
							break;
					}
					if (!flag) break;
				}

				if (flag && playingFlag)
				{
					if (!User.UnlockedSongs.ContainsKey(song.Title)) User.UnlockedSongs.Add(song.Title, null);
					Debug.Log("チャレンジ解禁: " + song.Title);
				}
			}
		}

		public static bool HasChartUnlock(string title, Chart.Difficulties difficulty)
		{
			bool flag = false;
			foreach (var song in Environment.Songs.Where(s => s.UnlockConditions.Any()))
			{
				if (UnlockedSongs.ContainsKey(song.Title)) continue;

				bool rate = true, unlocked = true, cleared = true, playing = false;

				foreach (var e in song.UnlockConditions)
				{
					switch (e.Item1)
					{
						case UnlockCondition.Rate:
							if (User.Rate < Single.Parse(e.Item2)) rate = false;
							break;
						case UnlockCondition.Playing:
							{
								var strs = e.Item2.Split('|');
								var _title = strs[0];
								var _diff = (Chart.Difficulties)Enum.Parse(typeof(Chart.Difficulties), strs[1], true);

								if (title == _title && difficulty == _diff)
									playing = true;
							}
							break;
						case UnlockCondition.Unlocked:
							{
								var _title = e.Item2;
								if (!User.UnlockedSongs.ContainsKey(_title)) unlocked = false;
							}
							break;
						case UnlockCondition.Cleared:
							{
								var data = e.Item2.Split('|');
								cleared &= User.PlayDatas.Any(
									r => r.Title == data[0]
										 && (int)r.Difficulty >= (int)Enum.Parse(typeof(Chart.Difficulties), data[1], true)
										 && r.IsFormerlyClear);
							}
							break;
					}
				}

				flag |= rate && unlocked && cleared && playing;
				if (flag) break;
			}

			return flag;
		}
	}
}