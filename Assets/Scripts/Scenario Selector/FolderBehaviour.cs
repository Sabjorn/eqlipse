﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FolderBehaviour : MonoBehaviour
{
    [SerializeField]
    Text textFolderName;

    string _folderName;
    public string FolderName
    {
        get { return _folderName; }
        set
        {
            _folderName = value;
            textFolderName.text = value;
        }
    }

    public Transform ScenarioTransform;
}
