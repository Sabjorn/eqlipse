﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using EQLIPSE;

public class ScenarioPanelsController : MonoBehaviour
{
    public GameObject FolderPrefab, ScenarioPrefab;
    public RectTransform FoldersTransform;

    public void GeneratePanels()
    {
        foreach (var e in Environment.Scenarios)
        {
            var go = Instantiate<GameObject>(FolderPrefab);
            go.transform.SetParent(FoldersTransform, false);

            var folderComponent = go.GetComponent<FolderBehaviour>();
            folderComponent.FolderName = e.Key;

            foreach (var s in e.Value)
            {
                var go_s = Instantiate(ScenarioPrefab);
                go_s.transform.SetParent(folderComponent.ScenarioTransform, false);

                var scenarioComponent = go_s.GetComponent<ScenarioPanelBehaviour>();
                scenarioComponent.Scenario = s;
                scenarioComponent.ScenarioName = s.Title;
            }
        }
    }
}
