﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using EQLIPSE;

public class ScenarioPanelBehaviour : MonoBehaviour
{
    [SerializeField]
    Text textScenarioName;

    string _scenarioName;
    public string ScenarioName
    {
        get { return _scenarioName; }
        set
        {
            _scenarioName = value;
            textScenarioName.text = value;
        }
    }

    public Scenario Scenario;

    public void Tap()
    {
        Environment.Story.Scenario = Scenario;
        SceneTransitioner.Singleton.Next();
    }
}
