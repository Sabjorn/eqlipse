﻿using System;
using System.IO;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using EQLIPSE.Utility;
using UnityEngine;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UniRx;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace EQLIPSE
{
	public static class Environment
	{
		public static Song[] Songs;
		public static Dictionary<string, List<Scenario>> Scenarios;

		public static Dictionary<Song, bool[]> FreeAvailableSongs
		{
			get
			{
				return Environment.Songs
					.OrderBy(s => s.Title)
					.Where(s => s.Title != "Tutorial" && (!s.UnlockConditions.Any() ||
														  (User.UnlockedSongs.ContainsKey(s.Title) &&
														   User.UnlockedSongs[s.Title] != null)))
					.ToDictionary(s => s, s =>
					{
						var bools = new bool[3];
						if (!s.UnlockConditions.Any())
						{
							bools = new[] { true, true, true };
						}
						else
						{
							for (int i = 0; i < 3; i++)
							{
								var unlockedSong = User.UnlockedSongs[s.Title];
								if (unlockedSong != null) bools[i] = i <= (int)unlockedSong;
							}
						}
						return bools;
					});
			}
		}

		public static Dictionary<Song, bool[]> ChallengeAvailableSongs
		{
			get
			{
				// DEBUG!!
				//return Environment.Songs
				//	.OrderBy(s => s.Title)
				//	.Where(s => s.Title != "Tutorial" && s.UnlockConditions.Any())
				//	.ToDictionary(s => s, s => new[] { true, true, true });
				return Environment.Songs
					.OrderBy(s => s.Title)
					.Where(s => s.Title != "Tutorial" && User.UnlockedSongs.ContainsKey(s.Title) &&
								User.UnlockedSongs[s.Title] != Chart.Difficulties.Hard)
					.ToDictionary(s => s, s =>
					{
						var unlockedSong = User.UnlockedSongs[s.Title];
						if (unlockedSong == null) return new bool[] { true, true, true };

						var bools = new bool[3];
						for (int i = 0; i < 3; i++)
						{
							bools[i] = i > (int)unlockedSong;
						}
						return bools;
					});
			}
		}

		public static bool EnableTouchParticle = true;

		public static class Option
		{
			public static void HSNoob()
			{
				User.HiSpeed = 2.0f;
			}
			public static void HSPro()
			{
				User.HiSpeed = 3.0f;
			}
		}
		public static class Selector
		{
			public class InfoEntity
			{
				public List<Tuple<string, int>> TopRankers;
			}

			public static Dictionary<Song, bool[]> AvailableSongs;

			public static Dictionary<Tuple<string, Chart.Difficulties>, InfoEntity> Infos = new Dictionary<Tuple<string, Chart.Difficulties>, InfoEntity>();

			public static IObservable<Unit> FetchInfoObservable()
			{
				return ObservableWWW.Get(WwwApiHost + "/charts/info")
					.Do(response =>
					{
						var d = JToken.Parse(response);

						Infos = d.Select(token =>
						{
							var key = new Tuple<string, Chart.Difficulties>((string)token["title"],
								(Chart.Difficulties)(int)token["difficulty"]);

							var value = new InfoEntity()
							{
								TopRankers = token["scores"].Select(e => new Tuple<string, int>((string)e["user"]["name"], (int)e["score"]))
									.ToList()
							};
							var entity = new KeyValuePair<Tuple<string, Chart.Difficulties>, InfoEntity>(key, value);

							return entity;
						}).ToDictionary(kv => kv.Key, kv => kv.Value);
					})
					.DoOnError(Debug.LogError).AsUnitObservable();
			}
		}
		public static class Player
		{
			public static Song SelectedSong;
			public static Chart.Difficulties SelectedDifficulty = Chart.Difficulties.Easy;

			public static Chart LoadedChart = null;

			public static Difficulty.Gauges Gauge = Difficulty.Gauges.Normal;

			public static bool IsAutoPlay = false;
			public static bool IsSimplePre = false;
			public static bool IsChallenge = false;

			public static PlayData PlayData = new PlayData
			{
				Title = "ウルトラマン拉致",
				Difficulty = Chart.Difficulties.Normal,
				Gauge = 0.9f,
				JudgeCounter = new Dictionary<Judges, int>()
				{
					{Judges.Perfect,114 },
					{Judges.Great,514 },
					{Judges.Good,364 },
					{Judges.Miss,1919 },
					{Judges.SlidePerfect,893 },
					{Judges.SlideGreat,810 },
					{Judges.SlideGood,187 },
					{Judges.SlideMiss,931 }
				},
				MaxCombo = 4545
			};

			public static void EnqueuePlayData()
			{
				var form = new WWWForm();
				form.AddField("uid", User.Uid);
				form.AddField("title", PlayData.Title);
				form.AddField("difficulty", (int)PlayData.Difficulty);
				form.AddField("gauge_type", (int)PlayData.GaugeType);
				form.AddField("is_cleared", PlayData.IsClear ? 1 : 0);
				form.AddField("score", PlayData.Score);
				form.AddField("brilliant",
					PlayData.JudgeCounter[Judges.Perfect] + PlayData.JudgeCounter[Judges.SlidePerfect]);
				form.AddField("great",
					PlayData.JudgeCounter[Judges.Great] + PlayData.JudgeCounter[Judges.SlideGreat]);
				form.AddField("good",
					PlayData.JudgeCounter[Judges.Good] + PlayData.JudgeCounter[Judges.SlideGood]);
				form.AddField("miss",
					PlayData.JudgeCounter[Judges.Miss] + PlayData.JudgeCounter[Judges.SlideMiss]);
				form.AddField("none",
					PlayData.JudgeCounter[Judges.None]);

				WWWPostQueue.Enqueue(new WWWPostEntity
				{
					Url = WwwApiHost + "/scores/register",
					Data = form.data,
					Headers = new Dictionary<string, string>()
				});
			}
		}

		public static class Result
		{
			public static List<Tuple<string, Chart.Difficulties?>> UnlockedSongs = new List<Tuple<string, Chart.Difficulties?>>();

			public static bool IsNewRecord = false;
		}
		public static class Story
		{
			public static Scenario Scenario;
			public static int SeriphPosition = 0;

			public static Dictionary<string, Dictionary<Seriph.Emotions, Sprite>> FaceSpritesDictionary;
			public static Dictionary<string, Sprite> BGImagesDictionary;

			public static IEnumerator LoadScenarios()
			{
				WWW wwwScenarioDictionary = new WWW(ScenarioListUrl);
				yield return wwwScenarioDictionary;

				var ScenarioDictionary = JObject.Parse(wwwScenarioDictionary.text).ToObject<Dictionary<string, List<string>>>();
				wwwScenarioDictionary.Dispose();

				Scenarios = new Dictionary<string, List<Scenario>>();
				foreach (var e in ScenarioDictionary)
				{
					var scenarios = new List<Scenario>();

					foreach (var l in e.Value)
					{
						WWW wwwScenario = new WWW(ScenariosUrl + "/" + e.Key + "/" + l + ".csv");
						yield return wwwScenario;

						Scenario s = Scenario.Parse(Encoding.Unicode.GetString(wwwScenario.bytes));
						scenarios.Add(s);
					}

					Scenarios.Add(e.Key, scenarios);
				}

				Debug.Log("シナリオ読み込み完了: " + Scenarios.Count + "個");
			}

			public static IEnumerator LoadFacialImages()
			{
				WWW wwwFaceDictionary = new WWW(FacialImageDictionaryUrl);
				yield return wwwFaceDictionary;

				var FaceDictionary = JObject.Parse(wwwFaceDictionary.text).ToObject<Dictionary<string, Dictionary<Seriph.Emotions, string>>>();
				wwwFaceDictionary.Dispose();

				FaceSpritesDictionary = new Dictionary<string, Dictionary<Seriph.Emotions, Sprite>>();
				foreach (var folder in FaceDictionary)
				{
					FaceSpritesDictionary.Add(folder.Key, new Dictionary<Seriph.Emotions, Sprite>());
					foreach (var emo_file in folder.Value)
					{
						WWW wwwImage = new WWW(FacialImagesUrl + "/" + folder.Key + "/" + emo_file.Value);
						yield return wwwImage;

						Texture2D tex = wwwImage.texture;
						FaceSpritesDictionary[folder.Key].Add(emo_file.Key, Sprite.Create(tex, new Rect(Vector2.zero, new Vector2(tex.width, tex.height)), Vector2.zero));
					}
				}

				Debug.Log("顔画像読み込み完了: " + FaceSpritesDictionary.Count + "種");
			}

			public static IEnumerator LoadBGImages()
			{
				WWW wwwBGImageList = new WWW(BgImageListUrl);
				yield return wwwBGImageList;

				var BGImageList = JContainer.Parse(wwwBGImageList.text).ToObject<List<string>>();
				wwwBGImageList.Dispose();

				BGImagesDictionary = new Dictionary<string, Sprite>();
				foreach (var filename in BGImageList)
				{
					WWW wwwImage = new WWW(BgImagesUrl + "/" + filename);
					yield return wwwImage;

					Texture2D tex = wwwImage.texture;
					BGImagesDictionary.Add(filename, Sprite.Create(tex, new Rect(Vector2.zero, new Vector2(tex.width, tex.height)), Vector2.zero));
				}

				Debug.Log("背景画像読み込み完了: " + BGImagesDictionary.Count + "枚");
			}
		}

		public static class MainMenu
		{
			public static List<string> Messages = new List<string>();

			public static IObservable<Unit> LoadMessages()
			{
				return ObservableWWW.Get(Environment.MenuMessagesUrl)
					.Do(response =>
					{
						Messages.Clear();

						string temp = "";
						using (StringReader sr = new StringReader(response))
						{
							while (sr.Peek() != -1)
							{
								var str = sr.ReadLine();
								if (string.IsNullOrEmpty(str))
								{
									if (!string.IsNullOrEmpty(temp)) Messages.Add(temp);
									temp = "";
								}
								else
								{
									if (!string.IsNullOrEmpty(temp)) temp += "\n";
									temp += str;
								}
							}
							if (!string.IsNullOrEmpty(temp)) Messages.Add(temp);
						}

						// Logging
						var builder = new StringBuilder();
						builder.AppendLine("メッセージ読み込み完了:");
						foreach (var message in Messages)
						{
							builder.AppendFormat("\t{0}\n\n", message);
						}
						builder.AppendFormat("全{0}セリフ", Messages.Count);

						Debug.Log(builder);
					}).AsUnitObservable();
			}
		}

		public static IEnumerator LoadSongs()
		{
			WWW wwwList = new WWW(SongListUrl);
			yield return wwwList;

			Song[] songList;

			using (StringReader reader = new StringReader(wwwList.text)) { songList = JsonConvert.DeserializeObject<Song[]>(reader.ReadToEnd()); }
			wwwList.Dispose();

			foreach (Song s in songList)
			{
				WWW wwwTex = new WWW(SongsUrl + "/" + s.Folder + "/graphic_small.jpg");
				yield return wwwTex;
				s.Cover = string.IsNullOrEmpty(wwwTex.error) ? wwwTex.texture : null;
				wwwTex.Dispose();

				WWW wwwPreview = new WWW(SongsUrl + "/" + s.Folder + "/demo.mp3");
				yield return wwwPreview;

				var clip = wwwPreview.GetAudioClip(false, true);
				if (clip == null)
				{
					wwwPreview = new WWW(SongsUrl + "/" + s.Folder + "/demo.ogg");
					yield return wwwPreview;

					clip = wwwPreview.GetAudioClip(false, true);
				}

				s.Preview = clip;
				wwwPreview.Dispose();
			}

			Songs = songList;

			// Logging
			var builder = new StringBuilder();
			builder.AppendLine("曲読み込み完了:");
			foreach (var song in Songs)
			{
				builder.AppendFormat("\t{0}\n", song.Title);
			}
			builder.AppendFormat("全{0}曲", Songs.Length);

			Debug.Log(builder);
		}

		public static string SongsPath { get { return Application.streamingAssetsPath + "/Songs"; } }
		public static string SongListPath { get { return SongsPath + "/SongList.json"; } }
		public static string SongsUrl { get { return (Application.platform != RuntimePlatform.Android ? "file://" : "") + SongsPath; } }
		public static string SongListUrl { get { return SongsUrl + "/SongList.json"; } }

		public static string ScenariosPath { get { return Application.streamingAssetsPath + "/Scenarios"; } }
		public static string ScenarioListPath { get { return ScenariosPath + "/ScenarioList.json"; } }
		public static string ScenariosUrl { get { return (Application.platform != RuntimePlatform.Android ? "file://" : "") + ScenariosPath; } }
		public static string ScenarioListUrl { get { return ScenariosUrl + "/ScenarioList.json"; } }

		public static string FacialImagesPath { get { return Application.streamingAssetsPath + "/Images/Faces"; } }
		public static string FacialImageDictionaryPath { get { return FacialImagesPath + "/FacialImageDictionary.json"; } }
		public static string FacialImagesUrl { get { return (Application.platform != RuntimePlatform.Android ? "file://" : "") + FacialImagesPath; } }
		public static string FacialImageDictionaryUrl { get { return FacialImagesUrl + "/FacialImageDictionary.json"; } }

		public static string BgImagesPath { get { return Application.streamingAssetsPath + "/Images/BGs"; } }
		public static string BgImageListPath { get { return BgImagesPath + "/BGImageList.json"; } }
		public static string BgImagesUrl { get { return (Application.platform != RuntimePlatform.Android ? "file://" : "") + BgImagesPath; } }
		public static string BgImageListUrl { get { return BgImagesUrl + "/BGImageList.json"; } }

		public static string MenuMessagesUrl { get { return Application.streamingAssetsPath + "/Messages.txt"; } }

		public static string WwwQueuePath { get { return Application.persistentDataPath + "/queue.bin"; } }

		public static string WwwApiHost { get { return "https://qpic.sakura.ne.jp/eqlipse/index.php/api"; } }

		public static Dictionary<Chart.Difficulties, string> DifficultyFilenameDictionary
		{
			get
			{
				return new Dictionary<Chart.Difficulties, string>
				{
					{ Chart.Difficulties.Easy, "file1" },
					{ Chart.Difficulties.Normal, "file2" },
					{ Chart.Difficulties.Hard, "file3" }
				};
			}
		}

		public static string UnlockConditionFilename = "conditions.txt";

		public static string ConsumerKey = "0NTahvH74fD9t1SHq7Sn8szRX";
		public static string ConsumerSecret = "yzZ0YWQH1IrPp4jLBdlDc3he417A7TYmSIcKAM5OgHT7PNMW1L";
	}


#if UNITY_EDITOR
	public static class EditorMenu
	{
		[MenuItem("EQLIPSE/譜面リスト生成")]
		public static void GenerateSongList()
		{
			EditorUtility.DisplayProgressBar("譜面リスト生成中・・・", "譜面リスト生成を開始", 0f);

			List<Song> Songs = new List<Song>();

			string[] folders = Directory.GetDirectories(Environment.SongsPath + "/")
				.Select(Path.GetFileName)
				.ToArray();

			int count = 0;
			Songs.AddRange(folders.Select(folder =>
			{
				Song song = new Song() { Folder = folder };

				foreach (Chart.Difficulties difficulty in Enum.GetValues(typeof(Chart.Difficulties)))
				{
					string path = Environment.SongsPath + "/" + folder + "/" + Environment.DifficultyFilenameDictionary[difficulty] + ".eql";
					if (File.Exists(path))
					{
						using (StreamReader reader = new StreamReader(path))
						{
							Chart chart = new Chart();
							chart.Parse(reader.ReadToEnd());

							if (string.IsNullOrEmpty(song.Title)) song.Title = chart.Title;
							if (string.IsNullOrEmpty(song.Artist)) song.Artist = chart.Artist;
							if (string.IsNullOrEmpty(song.Genre)) song.Genre = chart.Genre;
							song.BPM = chart.BpmStandard;
							song.IsExtra = chart.IsExtra;
							song.LevelDictionary[difficulty] = chart.Level;
							song.ConstDictionary[difficulty] = chart.Const;
						}
						if (!song.LevelDictionary.ContainsKey(difficulty)) song.LevelDictionary[difficulty] = -1;
					}
				}

				string conditionsPath = Environment.SongsPath + "/" + folder + "/" + Environment.UnlockConditionFilename;

				if (File.Exists(conditionsPath))
				{
					using (StreamReader reader = new StreamReader(conditionsPath))
					{
						song.ParseConditions(reader.ReadToEnd());
					}
				}

				count++;
				EditorUtility.DisplayProgressBar("譜面リスト更新", "生成: " + song.Title, (float)count / folders.Length);
				return song;
			}));

			using (StreamWriter writer = new StreamWriter(Environment.SongListPath))
			{
				writer.Write(JsonConvert.SerializeObject(Songs));
			}

			var json = JToken.FromObject(Songs.SelectMany(
				s => from Chart.Difficulties diff in s.LevelDictionary.Keys
					 select new
					 {
						 title = s.Title,
						 difficulty = (int)diff,
						 level = s.LevelDictionary[diff],
						 constant = s.ConstDictionary[diff]
					 })
			);

			var headers = new Dictionary<string, string>()
			{
				{"Content-Type", "application/json"}
			};

			EditorUtility.DisplayProgressBar("譜面リスト更新", "送信中", 1f);
			ObservableWWW.Post(Environment.WwwApiHost + "/charts/update", System.Text.Encoding.UTF8.GetBytes(json.ToString()), headers)
				.Subscribe(_ =>
				{
					EditorUtility.ClearProgressBar();
				}, Debug.LogError);
		}

		[MenuItem("EQLIPSE/シナリオリスト生成")]
		public static void GenerateScenarioList()
		{
			string[] folders = Directory.GetDirectories(Environment.ScenariosPath + "/");

			Dictionary<string, List<string>> scenarioes = new Dictionary<string, List<string>>();
			foreach (var folder in folders)
			{
				scenarioes.Add(Path.GetFileName(folder), Directory.GetFiles(folder).Where(f => Path.GetExtension(f) == ".csv").Select(s => Path.GetFileNameWithoutExtension(s)).ToList());
			}

			using (StreamWriter sr = new StreamWriter(Environment.ScenarioListPath))
			{
				sr.Write(JsonConvert.SerializeObject(scenarioes));
			}

			Debug.Log("シナリオリストの生成に成功しました。");
		}

		[MenuItem("EQLIPSE/顔画像リスト生成")]
		public static void GenerateFacialImageDictionary()
		{
			Dictionary<string, Dictionary<Seriph.Emotions, string>> FaceDictionary = new Dictionary<string, Dictionary<Seriph.Emotions, string>>();

			string[] folders = Directory.GetDirectories(Environment.FacialImagesPath + "/");
			foreach (var folder in folders)
			{
				string[] images = Directory.GetFiles(folder).Where(f => Path.GetExtension(f) == ".png" || Path.GetExtension(f) == ".jpg").ToArray();
				foreach (Seriph.Emotions emotion in Enum.GetValues(typeof(Seriph.Emotions)))
				{
					if (images.Any(f => Path.GetFileNameWithoutExtension(f) == emotion.ToString()))
					{
						string imagePath = images.First(f => Path.GetFileNameWithoutExtension(f) == emotion.ToString());
						if (!string.IsNullOrEmpty(imagePath))
						{
							if (!FaceDictionary.ContainsKey(Path.GetFileName(folder))) FaceDictionary.Add(Path.GetFileName(folder), new Dictionary<Seriph.Emotions, string>());
							FaceDictionary[Path.GetFileName(folder)].Add(emotion, Path.GetFileName(imagePath));
						}
					}
				}
			}

			var jObj = JObject.FromObject(FaceDictionary);
			using (StreamWriter sr = new StreamWriter(Environment.FacialImageDictionaryPath))
			{
				sr.Write(jObj);
			}

			Debug.Log("顔画像リストの生成に成功しました。");
		}

		[MenuItem("EQLIPSE/背景画像リスト生成")]
		public static void GenerateBGImageList()
		{
			List<string> BGList = Directory.GetFiles(Environment.BgImagesPath)
				.Where(f => Path.GetExtension(f) == ".png" || Path.GetExtension(f) == ".jpg")
				.Select(Path.GetFileName)
				.ToList();
			var jObj = JContainer.FromObject(BGList);
			using (StreamWriter sw = new StreamWriter(Environment.BgImageListPath))
			{
				sw.Write(jObj);
			}

			Debug.Log("背景画像リストの生成に成功しました。");
		}
	}
#endif
}