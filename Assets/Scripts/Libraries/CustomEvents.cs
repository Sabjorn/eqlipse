﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using EQLIPSE;

[System.Serializable]
public class FloatEvent : UnityEvent<float> { }

[System.Serializable]
public class DoubleEvent : UnityEvent<double> { }

[System.Serializable]
public class IntEvent : UnityEvent<int> { }

[System.Serializable]
public class StringEvent : UnityEvent<string> { }

[System.Serializable]
public class TextureEvent : UnityEvent<Texture> { }

[System.Serializable]
public class RationalEvent : UnityEvent<Rational> { }

[System.Serializable]
public class SongPathEvent : UnityEvent<string, string> { }

[System.Serializable]
public class JudgeEvent : UnityEvent<Judges> { }

[System.Serializable]
public class JudgeIntEvent : UnityEvent<Judges, int> { }

[System.Serializable]
public class JudgeFloatEvent : UnityEvent<Judges, float> { }

[System.Serializable]
public class LaneInputEvent : UnityEvent<bool, int> { }

[System.Serializable]
public class JudgeInfoEvent : UnityEvent<JudgeInfo> { }