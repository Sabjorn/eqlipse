﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using EQLIPSE.Player;
using UnityEngine;
using UniRx;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace EQLIPSE
{
	public enum Judges
	{
		None,
		Perfect,
		Great,
		Good,
		Miss,
		SlidePerfect,
		SlideGreat,
		SlideGood,
		SlideMiss
	}

	public enum UnlockCondition
	{
		Rate, Unlocked, Cleared, Playing
	}

	public class JudgeInfo
	{
		public enum NoteTypes
		{
			Normal,
			Charge,
			Slide,
			Tap
		}
		public Note Note;
		public Judges Judge;

		public NoteTypes NoteType = NoteTypes.Normal;

		public float DeltaTime = 0;
		public float DeltaPosition = 0;

		public float Position = 0;
	}

	public abstract class Note
	{
		public bool IsFingered = false;
	};

	public class NormalNote : Note, IComparable<NormalNote>, JudgeElement
	{
		public Rational Beat;

		public NormalNote(Rational beat) { Beat = beat; }

		public int CompareTo(NormalNote n) { return Beat.CompareTo(n.Beat); }
	}

	public class ChargeNote : Note, IComparable<ChargeNote>, JudgeElement
	{
		public Rational[] Beat;

		public ChargeNote(Rational beatFrom, Rational beatTo) { Beat = new Rational[2] { beatFrom, beatTo }; }
		public int CompareTo(ChargeNote n) { return Beat[0].CompareTo(n.Beat[0]); }
	}

	public class TapNote : Note, IComparable<TapNote>, JudgeElement
	{
		public Rational Beat;
		public float Value;

		public TapNote(Rational beat, float value)
		{
			Beat = beat;
			Value = value;
		}

		public int CompareTo(TapNote n) { return Beat.CompareTo(n.Beat); }
	}

	public class SlideNote : Note, IComparable<SlideNote>
	{
		public KeyValuePair<Rational, float>[] Keys;

		public Rational BeatFrom { get { return Keys[0].Key; } }
		public Rational BeatTo { get { return Keys[1].Key; } }
		public float ValueFrom { get { return Keys[0].Value; } }
		public float ValueTo { get { return Keys[1].Value; } }

		public int NumCombos { get { return (int)((double)(BeatTo - BeatFrom) * 2 + 0.5); } }

		public SlideNote(KeyValuePair<Rational, float> From, KeyValuePair<Rational, float> To)
		{
			Keys = new KeyValuePair<Rational, float>[2];
			Keys[0] = From;
			Keys[1] = To;
		}
		public int CompareTo(SlideNote n) { return BeatFrom.CompareTo(n.BeatFrom); }
	}

	public class Chart
	{
		public Song Song;
		public string Title = "New Song", Genre = "Non Genre", Artist = "Somebody";
		public double Offset;
		public string WaveFileName = "dummy.wav";
		public string FolderName = "New Song", FileName = "file1.eql";
		public Rational BpmStandard = 120;
		public Difficulties Difficulty = Difficulties.Easy;
		public int Level = -1;
		public float Const = -1;
		public bool IsExtra = false;
		public AudioClip Audio;

		// X: beats, Y: seconds
		public PiecewiseFunction BeatBySecond = new PiecewiseFunction() { Derivatives = new Pair<Rational, Rational>[] { new Pair<Rational, Rational> { Item1 = 0, Item2 = new Rational(1, 2) } } };
		// X: measures, Y: beats
		public PiecewiseFunction MeasureByBeat = new PiecewiseFunction() { Derivatives = new Pair<Rational, Rational>[] { new Pair<Rational, Rational> { Item1 = 0, Item2 = 4 } } };

		public List<NormalNote>[] Notes = new List<NormalNote>[4]{
			new List<NormalNote>(),	//note1
			new List<NormalNote>(),	//note2
			new List<NormalNote>(),	//note3
			new List<NormalNote>()	//note4
		};
		public List<ChargeNote>[] ChargeNotes = new List<ChargeNote>[4]{
			new List<ChargeNote>(),
			new List<ChargeNote>(),
			new List<ChargeNote>(),
			new List<ChargeNote>()
		};
		public List<SlideNote> SlideNotes = new List<SlideNote>();
		public List<TapNote> TapNotes = new List<TapNote>();

		public List<Pair<Rational, bool>> AutoplayCommands = new List<Pair<Rational, bool>>();

		int _numNormalNotes = 0, _numChargeNotes = 0, _numSlideNotes = 0, _numTapNotes = 0, _numMaxCombo = 0;

		bool _isLoaded = false;
		public bool IsLoaded { get { return _isLoaded; } }

		public int NumAllNotes { get { return _numNormalNotes + _numChargeNotes + _numSlideNotes + _numTapNotes; } }

		public int NumNormalNotes { get { return _numNormalNotes; } }

		public int NumChargeNotes { get { return _numChargeNotes; } }

		public int NumSlideNotes { get { return _numSlideNotes; } }

		public int NumTapNotes { get { return _numTapNotes; } }

		public int NumMaxCombo { get { return _numMaxCombo; } }

		public double BeatLength
		{
			get
			{
				double beatNoteMax = 0;

				for (int i = 0; i < 4; i++)
				{
					double[] beats = Notes[i].Select(n => (double)n.Beat).ToArray();
					if (beats.Length > 0)
					{
						beatNoteMax = Math.Max(beatNoteMax, beats.Max());
					}
				}

				double beatChargeNoteMax = 0;

				for (int i = 0; i < 4; i++)
				{
					double[] beats = ChargeNotes[i].Select(n => (double)n.Beat[1]).ToArray();
					if (beats.Length > 0)
					{
						beatChargeNoteMax = Math.Max(beatChargeNoteMax, beats.Max());
					}
				}

				double beatSlideNoteMax = 0;

				double[] Sbeats = SlideNotes.Select(n => (double)n.BeatTo).ToArray();
				if (Sbeats.Length > 0)
				{
					beatSlideNoteMax = Sbeats.Max();
				}

				double beatTapNoteMax = 0;

				double[] Tbeats = TapNotes.Select(n => (double)n.Beat).ToArray();
				if (Tbeats.Length > 0)
				{
					beatTapNoteMax = Tbeats.Max();
				}

				return Math.Max(beatNoteMax, Math.Max(beatChargeNoteMax, Math.Max(beatSlideNoteMax, beatTapNoteMax)));
			}
		}

		public double TimeLength { get { return ToTime(BeatLength); } }

		public enum Difficulties { Easy = 0, Normal = 1, Hard = 2 }

		public Chart()
		{
			_isLoaded = false;
		}

		public void Parse(string data)
		{
			#region Reading Headers
			using (StringReader sReader = new StringReader(data))
			{
				while (sReader.Peek() != -1)
				{
					string line = sReader.ReadLine();
					if (line.Length > 0)
					{
						if (line.First() == '#')
						{
							string[] lineData = line.TrimStart('#').Split(':');

							switch (lineData[0])
							{
								case "TITLE":
									Title = lineData[1].Replace('_', ' ');
									break;
								case "ARTIST":
									Artist = lineData[1].Replace('_', ' ');
									break;
								case "GENRE":
									Genre = lineData[1].Replace('_', ' ');
									break;
								case "WAVE":
									WaveFileName = lineData[1];
									break;
								case "BPM":
									{
										double b;
										double.TryParse(lineData[1], out b);
										BpmStandard = b.ToRational(10);
										break;
									}
								case "OFFSET":
									Offset = float.Parse(lineData[1]);
									break;
								case "D":   // ** DEPRECATED **
									Level = int.Parse(lineData[1]);
									break;
								case "DIFFICULTY":
									Difficulty = (Difficulties)Enum.Parse(typeof(Difficulties), lineData[1]);
									break;
								case "LEVEL":
									Level = int.Parse(lineData[1]);
									break;
								case "CONST":
									Const = float.Parse(lineData[1]);
									break;
								case "EXTRA":
									IsExtra = true;
									break;
							}
						}
					}
				}
			}
			#endregion

			#region Reading Measures
			List<Pair<Rational, Rational>> meters = new List<Pair<Rational, Rational>>();
			using (StringReader sReader = new StringReader(data))
			{
				while (sReader.Peek() != -1)
				{
					string line = sReader.ReadLine();
					if (line.Length > 0)
					{
						if (line.First() == '#')
						{
							string[] lineData = line.TrimStart('#').Split(':');

							int measure;
							if (int.TryParse(lineData[0], out measure))
							{
								string[] command = lineData[1].Split(',');
								if (command[0] == "M")
								{
									Rational meter = 0;
									meter = Rational.Parse(command[1]);

									meters.Add(new Pair<Rational, Rational>(measure, meter));
								}
							}
						}
					}
				}
			}

			if (meters.Count == 0 || meters[0].Item1 != 0) throw new System.Exception("At least one Meter Command should be at measure 0!");

			//MeterSeq.Meters = meters;
			MeasureByBeat = new PiecewiseFunction()
			{
				Derivatives = meters.ToArray()
			};
			#endregion

			#region Reading Others
			List<Pair<Rational, Rational>> bpmData = new List<Pair<Rational, Rational>>();

			List<KeyValuePair<Rational, int>> sliderData = new List<KeyValuePair<Rational, int>>();
			List<KeyValuePair<Rational, int>>[] chargeData = new List<KeyValuePair<Rational, int>>[4]{
				new List<KeyValuePair<Rational, int>>(),
				new List<KeyValuePair<Rational, int>>(),
				new List<KeyValuePair<Rational, int>>(),
				new List<KeyValuePair<Rational, int>>()
			};

			List<KeyValuePair<Rational, int>> sliderDataFingered = new List<KeyValuePair<Rational, int>>();
			List<KeyValuePair<Rational, int>>[] chargeDataFingered = new List<KeyValuePair<Rational, int>>[4]{
				new List<KeyValuePair<Rational, int>>(),
				new List<KeyValuePair<Rational, int>>(),
				new List<KeyValuePair<Rational, int>>(),
				new List<KeyValuePair<Rational, int>>()
			};

			using (StringReader sReader = new StringReader(data))
			{
				while (sReader.Peek() != -1)
				{
					string line = sReader.ReadLine();
					if (line.Length > 0)
					{
						if (line.First() == '#')
						{
							string[] lineData = line.TrimStart('#').Split(':');

							// #( data[0] ):( data[1] )

							int measure;
							if (int.TryParse(lineData[0], out measure))
							{
								//Reading commands
								Rational refBeat = MeasureByBeat.YAt(measure);

								string[] command = lineData[1].Split(',');

								// #( bar ):( command[0] ),( command[1] ),( command[2] )

								switch (command[0])
								{
									case "N":   // GradNormal Note
										{
											int denom = command[1].Length;
											for (int i = 0; i < denom; i++)
											{
												int n = int.Parse(command[1][i].ToString());
												if (n == 0) continue;
												else
												{
													Notes[n - 1].Add(new NormalNote(refBeat + (MeasureByBeat.DYDXAt(measure) * new Rational(i, denom))));
												}
											}
											break;
										}
									case "S":   // Slide Note
										{
											int denom = command[1].Length;
											for (int i = 0; i < denom; i++)
											{
												int n = int.Parse(command[1][i].ToString());
												if (n == 0) continue;
												else
												{
													sliderData.Add(new KeyValuePair<Rational, int>(refBeat + (MeasureByBeat.DYDXAt(measure) * new Rational(i, denom)), n));
												}
											}
											break;
										}
									case "ST":  // Tap Note
										{
											int denom = command[1].Length;
											for (int i = 0; i < denom; i++)
											{
												int n = int.Parse(command[1][i].ToString());
												if (n == 0) continue;
												else
												{
													TapNotes.Add(new TapNote(refBeat + (MeasureByBeat.DYDXAt(measure) * new Rational(i, denom)), (n - 1) / 8.0f));
												}
											}
											break;
										}
									case "C":   // Charge Note
										{
											int denom = command[1].Length;
											for (int i = 0; i < denom; i++)
											{
												int n = int.Parse(command[1][i].ToString());
												if (n == 0) continue;
												else
												{
													chargeData[n - 1].Add(new KeyValuePair<Rational, int>(refBeat + (MeasureByBeat.DYDXAt(measure) * new Rational(i, denom)), n));
												}
											}
											break;
										}
									case "BPM": // BPM Change
										bpmData.Add(new Pair<Rational, Rational>(refBeat + Rational.Parse(command[2]), 60 / double.Parse(command[1]).ToRational(10)));
										break;
									case "B":   // ** DEPRECATED **
										bpmData.Add(new Pair<Rational, Rational>(refBeat, 60 / double.Parse(command[1]).ToRational(10)));
										break;
									case "AUTO":    // Autoplay Change
										if (command.Length == 2)
										{
											AutoplayCommands.Add(new Pair<Rational, bool>(refBeat, bool.Parse(command[1])));
										}
										else if (command.Length == 3)
										{
											AutoplayCommands.Add(new Pair<Rational, bool>(refBeat + Rational.Parse(command[2]), bool.Parse(command[1])));
										}
										break;
								}
								continue;
							}
						}
						else { continue; }
					}
				}
			}

			// Sorting
			for (int i = 0; i < 4; i++)
			{
				Notes[i].Sort((n1, n2) => n1.Beat.CompareTo(n2.Beat));
			}
			TapNotes.Sort((n1, n2) => n1.Beat.CompareTo(n2.Beat));

			if (bpmData.Count > 0)
			{
				BeatBySecond = new PiecewiseFunction() { Derivatives = bpmData.ToArray() };
			}
			else
			{
				BeatBySecond = new PiecewiseFunction()
				{
					Derivatives = new Pair<Rational, Rational>[] {
						new Pair<Rational, Rational>() {
							Item1=0,Item2=60/BpmStandard
						}
					}
				};
			}

			sliderData.Sort((l, r) => l.Key.CompareTo(r.Key));
			foreach (int i in Enumerable.Range(0, sliderData.Count / 2))
			{
				SlideNotes.Add(
					new SlideNote(
						new KeyValuePair<Rational, float>(sliderData[i * 2].Key, (sliderData[i * 2].Value - 1) / 8.0f),
						new KeyValuePair<Rational, float>(sliderData[i * 2 + 1].Key, (sliderData[i * 2 + 1].Value - 1) / 8.0f)
					)
				);
			}
			sliderDataFingered.Sort((l, r) => l.Key.CompareTo(r.Key));
			foreach (int i in Enumerable.Range(0, sliderDataFingered.Count / 2))
			{
				SlideNotes.Add(
					new SlideNote(
							new KeyValuePair<Rational, float>(sliderDataFingered[i * 2].Key, (sliderDataFingered[i * 2].Value - 1) / 8.0f),
							new KeyValuePair<Rational, float>(sliderDataFingered[i * 2 + 1].Key, (sliderDataFingered[i * 2 + 1].Value - 1) / 8.0f)
						)
					{ IsFingered = true }
				);
			}
			SlideNotes.Sort((n1, n2) => n1.BeatFrom.CompareTo(n2.BeatFrom));

			for (int i = 0; i < 4; i++)
			{
				chargeData[i].Sort((l, r) => { return Math.Sign((float)(l.Key - r.Key)); });
				foreach (int j in Enumerable.Range(0, chargeData[i].Count / 2))
				{
					ChargeNotes[i].Add(
						new ChargeNote(
							chargeData[i][j * 2].Key, chargeData[i][j * 2 + 1].Key
						)
					);
				}
				chargeDataFingered[i].Sort((l, r) => { return Math.Sign((float)(l.Key - r.Key)); });
				foreach (int j in Enumerable.Range(0, chargeDataFingered[i].Count / 2))
				{
					ChargeNotes[i].Add(
						new ChargeNote(
								chargeDataFingered[i][j * 2].Key, chargeDataFingered[i][j * 2 + 1].Key
							)
						{ IsFingered = true }
					);
					ChargeNotes[i].Sort((n1, n2) => n1.Beat[0].CompareTo(n2.Beat[0]));
				}
			}
			#endregion

			_numNormalNotes = Notes[0].Count + Notes[1].Count + Notes[2].Count + Notes[3].Count;
			_numChargeNotes = ChargeNotes[0].Count + ChargeNotes[1].Count + ChargeNotes[2].Count + ChargeNotes[3].Count;
			_numSlideNotes = SlideNotes.Count;
			_numTapNotes = TapNotes.Count;

			_numMaxCombo = NumNormalNotes + NumChargeNotes + NumTapNotes + SlideNotes.Select(sn => sn.NumCombos).Sum();

			_isLoaded = true;
		}

		public void SaveSong(StreamWriter writer)
		{
			#region Writing Headers
			writer.WriteLine("#TITLE:" + Title);
			writer.WriteLine("#ARTIST:" + Artist);
			writer.WriteLine("#GENRE:" + Genre);
			writer.WriteLine("#DIFFICULTY:" + Difficulty.ToString());
			writer.WriteLine("#LEVEL:" + Level);
			writer.WriteLine("#BPM:" + BpmStandard);
			writer.WriteLine("#WAVE:" + WaveFileName);
			writer.WriteLine("#OFFSET:" + Offset);
			#endregion

			#region Preparing Writings
			List<Pair<Rational, int>> notesWritingList = new List<Pair<Rational, int>>();
			for (int i = 0; i < 4; i++) { notesWritingList.AddRange(Notes[i].Select(n => new Pair<Rational, int>(n.Beat, i + 1))); }

			List<Pair<Rational, int>> chargeNotesWritingList = new List<Pair<Rational, int>>();
			for (int i = 0; i < 4; i++)
			{
				chargeNotesWritingList.AddRange(ChargeNotes[i].Select(n => new Pair<Rational, int>(n.Beat[0], i + 1)));
				chargeNotesWritingList.AddRange(ChargeNotes[i].Select(n => new Pair<Rational, int>(n.Beat[1], i + 1)));
			}

			List<Pair<Rational, int>> slideNotesWritingList = new List<Pair<Rational, int>>();
			slideNotesWritingList.AddRange(SlideNotes.Select(sn => new Pair<Rational, int>(sn.BeatFrom, (int)Math.Round(sn.ValueFrom * 8 + 1))));
			slideNotesWritingList.AddRange(SlideNotes.Select(sn => new Pair<Rational, int>(sn.BeatTo, (int)Math.Round(sn.ValueTo * 8 + 1))));

			List<Pair<Rational, int>> tapNotesWritingList = new List<Pair<Rational, int>>();
			tapNotesWritingList.AddRange(TapNotes.Select(st => new Pair<Rational, int>(st.Beat, (int)Math.Round(st.Value * 8 + 1))));
			#endregion

			Func<List<Pair<Rational, int>>, string[]> converter = list =>
			{
				List<string> ret = new List<string>();

				List<Pair<Rational, int>> listCarry = new List<Pair<Rational, int>>();
				while (list.Count > 0)
				{
					long commonDenom = Rational.CommonDenom(list.Select(n => n.Item1).ToArray());

					string line = "";
					for (int i = 0; i < commonDenom; i++)
					{
						List<Pair<Rational, int>> listHit = list.FindAll(n => n.Item1.NumIfDenomIs(commonDenom) == i);
						if (listHit.Count == 0)
						{
							line += "0";
						}
						else if (listHit.Count >= 1)
						{
							line += listHit[0].Item2;
							if (listHit.Count > 1) { listCarry.AddRange(listHit.Skip(1)); }
						}
					}
					ret.Add(line);

					list = new List<Pair<Rational, int>>(listCarry);
					listCarry.Clear();
				}

				return ret.ToArray();
			};

			Rational measure = 0;
			Rational beatFrom, beatTo;
			while (true)
			{
				beatFrom = MeasureByBeat.YAt(measure);
				beatTo = MeasureByBeat.YAt(measure + 1);

				if (beatFrom > BeatLength) break;

				//Writing Notes
				List<Pair<Rational, int>> currentNotesList = notesWritingList
					.Where(n => { return n.Item1 >= beatFrom && n.Item1 < beatTo; })
					.Select(n => new Pair<Rational, int>(MeasureByBeat.XAt(n.Item1) - measure, n.Item2)).ToList();

				string[] lines = converter(currentNotesList);
				foreach (string line in lines)
				{
					writer.WriteLine("#" + (int)measure + ":N," + line);
				}


				//Writing Charge Notes
				List<Pair<Rational, int>> currentChargeNotesList = chargeNotesWritingList
					.Where(n => { return n.Item1 >= beatFrom && n.Item1 < beatTo; })
					.Select(n => new Pair<Rational, int>(MeasureByBeat.XAt(n.Item1) - measure, n.Item2)).ToList();

				lines = converter(currentChargeNotesList);
				foreach (string line in lines)
				{
					writer.WriteLine("#" + (int)measure + ":C," + line);
				}


				//Writing Slider Notes
				List<Pair<Rational, int>> currentSlideNotesList = slideNotesWritingList
					.Where(n => n.Item1 >= beatFrom && n.Item1 < beatTo)
					.Select(n => new Pair<Rational, int>(MeasureByBeat.XAt(n.Item1) - measure, n.Item2)).ToList();

				lines = converter(currentSlideNotesList);
				foreach (string line in lines)
				{
					writer.WriteLine("#" + (int)measure + ":S," + line);
				}

				//Writing Tap Notes
				List<Pair<Rational, int>> currentTapNotesList = tapNotesWritingList
					.Where(n => { return n.Item1 >= beatFrom && n.Item1 < beatTo; })
					.Select(n => new Pair<Rational, int>(MeasureByBeat.XAt(n.Item1) - measure, n.Item2)).ToList();

				lines = converter(currentTapNotesList);
				foreach (string line in lines)
				{
					writer.WriteLine("#" + (int)measure + ":ST," + line);
				}

				//Writing BPM
				var currentBPMList = BeatBySecond.Derivatives
					.Where(b => beatFrom <= b.Item1 && b.Item1 < beatTo).ToList();
				foreach (var b in currentBPMList)
				{
					writer.WriteLine("#" + (int)measure + ":BPM," + (60.0 / b.Item2).ToString() + "," + (b.Item1 - beatFrom).ToString());
				}

				//Writing Meters
				if (MeasureByBeat.Derivatives.Where(m => m.Item1 == measure).Any())
				{
					writer.WriteLine("#" + (int)measure + ":M," + MeasureByBeat.Derivatives.Where(m => m.Item1 == measure).First().Item2);
				}

				//Writing AutoCommand
				List<Pair<Rational, bool>> currentAudoCommands = AutoplayCommands
					.Where(n => { return n.Item1 >= beatFrom && n.Item1 < beatTo; }).ToList();
				foreach (var e in currentAudoCommands)
				{
					writer.WriteLine("#" + (int)measure + ":Auto," + e.Item2.ToString() + "," + (e.Item1 - beatFrom).ToString());
				}

				measure++;
			}
		}

		public double ToBeat(double time) { return BeatBySecond.XAt(time - Offset); }
		public double ToTime(double beatTime) { return BeatBySecond.YAt(beatTime) + Offset; }

		public Rational MeasureToBeat(Rational measure) { return MeasureByBeat.YAt(measure); }
		public double MeasureToBeat(double measure) { return MeasureByBeat.YAt(measure.ToRational(120)); }

		public Rational BeatToMeasure(Rational beat) { return MeasureByBeat.XAt(beat); }
		public double BeatToMeasure(double beat) { return MeasureByBeat.XAt(beat.ToRational(30)); }

		public override string ToString()
		{
			return Title + " [" + Difficulty + ": " + Level + "]";
		}
	}

	public class PiecewiseFunction
	{
		SortedList<Rational, Rational> X, Y, DYDX;

		public Pair<Rational, Rational>[] Derivatives
		{
			get { return X.Select((e, i) => new Pair<Rational, Rational>(e.Value, DYDX.Values[i])).ToArray(); }
			set
			{
				var val = value.Distinct().ToArray();
				Array.Sort(val, (e1, e2) => Math.Sign(e1.Item1 - e2.Item1));


				if (val.Any(e => e.Item2 < 0)) throw new System.Exception("all DY/DX must be non-negative!");

				X = new SortedList<Rational, Rational>();
				Y = new SortedList<Rational, Rational>();
				DYDX = new SortedList<Rational, Rational>();

				Rational y = 0;
				for (int i = 0; i < val.Count(); i++)
				{
					X.Add(val[i].Item1, val[i].Item1);
					Y.Add(y, y);
					DYDX.Add(val[i].Item1, val[i].Item2);
					if (i + 1 < val.Count())
					{
						y += (val[i + 1].Item1 - val[i].Item1) * val[i].Item2;
						y.Approximate(100000);
					}
				}
			}
		}

		public PiecewiseFunction()
		{
			X = new SortedList<Rational, Rational>();
			Y = new SortedList<Rational, Rational>();
			DYDX = new SortedList<Rational, Rational>();
		}

		public Rational YAt(Rational x)
		{
			bool[] founds = new bool[2];
			int i = X.NearestIndices(x, out founds)[0];

			return Y.Values[i] + (x - X.Values[i]) * DYDX.Values[i];
		}
		public double YAt(double x)
		{
			bool[] founds = new bool[2];
			int i = X.NearestIndices(x.ToRational(1000), out founds)[0];

			return Y.Values[i] + (x - X.Values[i]) * DYDX.Values[i];
		}

		public Rational XAt(Rational y)
		{
			bool[] founds = new bool[2];
			int i = Y.NearestIndices(y, out founds)[0];

			return X.Values[i] + (y - Y.Values[i]) / DYDX.Values[i];
		}
		public double XAt(double y)
		{
			bool[] founds = new bool[2];
			int i = Y.NearestIndices(y.ToRational(1000), out founds)[0];

			return X.Values[i] + (y - Y.Values[i]) / DYDX.Values[i];
		}

		public Rational DYDXAt(Rational x)
		{
			bool[] founds = new bool[2];
			int i = X.NearestIndices(x, out founds)[0];

			return DYDX.Values[i];
		}
		public Rational DYDXAt(double x)
		{
			bool[] founds = new bool[2];
			int i = X.NearestIndices(x.ToRational(1000), out founds)[0];

			return DYDX.Values[i];
		}
	}
}