﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UniRx;
using Newtonsoft.Json.Linq;

namespace EQLIPSE
{
	[Serializable]
	public class Song
	{
		public string Title = "";
		public string Artist = "";
		public string Genre = "";
		public double BPM = -1;
		public bool IsExtra = false;
		[NonSerialized]
		public Texture2D Cover = null;
		[NonSerialized]
		public AudioClip Preview = null;

		public string Folder = "";
		public Dictionary<Chart.Difficulties, int> LevelDictionary = new Dictionary<Chart.Difficulties, int>();
		public Dictionary<Chart.Difficulties, float> ConstDictionary = new Dictionary<Chart.Difficulties, float>();

		public List<Pair<UnlockCondition, string>> UnlockConditions = new List<Pair<UnlockCondition, string>>();
		public Difficulty.Gauges ChallengeGauge = Difficulty.Gauges.Challenge3;

		public void ParseConditions(string data)
		{
			UnlockConditions = new List<Pair<UnlockCondition, string>>();
			using (StringReader reader = new StringReader(data))
			{
				while (reader.Peek() != -1)
				{
					var line = reader.ReadLine();

					if (!line.StartsWith("#")) continue;

					var lineData = line.TrimStart('#').Split(':');
					switch (lineData[0])
					{
						case "PLAYING":
							UnlockConditions.Add(new Pair<UnlockCondition, string>(UnlockCondition.Playing, lineData[1]));
							break;
						case "RATE":
							UnlockConditions.Add(new Pair<UnlockCondition, string>(UnlockCondition.Rate, lineData[1]));
							break;
						case "UNLOCKED":
							UnlockConditions.Add(new Pair<UnlockCondition, string>(UnlockCondition.Unlocked, lineData[1]));
							break;
						case "CLEARED":
							UnlockConditions.Add(new Pair<UnlockCondition, string>(UnlockCondition.Cleared, lineData[1]));
							break;
						case "GAUGE":
							ChallengeGauge = (Difficulty.Gauges)Enum.Parse(typeof(Difficulty.Gauges), lineData[1]);
							Debug.Log("Gauge: " + ChallengeGauge);
							break;
					}
				}
			}
		}
	}
}