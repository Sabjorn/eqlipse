﻿using System;
using System.Linq;
using UnityEngine;

namespace EQLIPSE
{
	public static partial class Extensions
	{
		public static Rational ToRational(this int self)
		{
			return new Rational(self, 1);
		}
		public static Rational ToRational(this double self, int maxDenom)
		{
			return new Rational((int)Math.Round(self * maxDenom), maxDenom);
		}
		public static Rational ToRational(this float self, int maxDenom)
		{
			return new Rational((int)Math.Round(self * maxDenom), maxDenom);
		}
	}
}

public struct Rational : IComparable<Rational>
{
	public long Numerator, Denominator;

	public static Rational Infinity { get { return new Rational(1, 0); } }
	public static Rational NegativeInfinity { get { return new Rational(-1, 0); } }

	public Rational(long num, long denom)
	{
		Numerator = num; Denominator = denom;
		Reduce();
	}

	static long gcd(long x, long y)
	{
		long temp1 = x, temp2 = y, temp3;
		while (Math.Abs(temp1) % Math.Abs(temp2) != 0)
		{
			temp3 = temp1 % temp2;
			temp1 = temp2;
			temp2 = temp3;
		}

		return Math.Abs(temp2);
	}
	static long gcd(params long[] nums)
	{
		if (nums.Length < 2)
		{
			return nums[0];
		}
		if (nums.Length != 2)
		{
			return gcd(nums.First(), gcd(nums.Skip(1).ToArray()));
		}
		else
		{
			return gcd(nums[0], nums[1]);
		}
	}

	static long lcm(long x, long y)
	{
		return x * y / gcd(x, y);
	}
	static long lcm(params long[] nums)
	{
		if (nums.Length < 2)
		{
			return nums[0];
		}
		if (nums.Length != 2)
		{
			return lcm(nums.First(), lcm(nums.Skip(1).ToArray()));
		}
		else
		{
			return lcm(nums[0], nums[1]);
		}
	}

	public void Reduce()
	{
		if (Denominator == 0)
		{
			Numerator = System.Math.Sign(Numerator);
		}
		else
		{
			long d = gcd(Numerator, Denominator);

			Numerator /= d; Denominator /= d;
		}
	}

	public static Rational operator +(Rational lhs, Rational rhs)
	{
		long denom = lcm(lhs.Denominator, rhs.Denominator);

		return new Rational(lhs.Numerator * denom / lhs.Denominator + rhs.Numerator * denom / rhs.Denominator, denom);
	}
	public static Rational operator -(Rational lhs, Rational rhs)
	{
		long denom = lcm(lhs.Denominator, rhs.Denominator);

		return new Rational(lhs.Numerator * denom / lhs.Denominator - rhs.Numerator * denom / rhs.Denominator, denom);
	}
	public static Rational operator *(Rational lhs, Rational rhs)
	{
		return new Rational(lhs.Numerator * rhs.Numerator, lhs.Denominator * rhs.Denominator);
	}
	public static Rational operator /(Rational lhs, Rational rhs)
	{
		return new Rational(lhs.Numerator * rhs.Denominator, lhs.Denominator * rhs.Numerator);
	}

	public static Rational operator ++(Rational r) { return r + 1; }
	public static Rational operator --(Rational r) { return r - 1; }

	public static bool operator ==(Rational lhs, Rational rhs)
	{
		Rational lr = lhs;
		lr.Reduce();

		Rational rr = rhs;
		rr.Reduce();

		return lr.Denominator == rr.Denominator && lr.Numerator == rr.Numerator;
	}
	public static bool operator !=(Rational lhs, Rational rhs) { return !(lhs == rhs); }

	public override bool Equals(object obj) { return base.Equals(obj); }
	public override int GetHashCode() { return base.GetHashCode(); }

	public static bool operator <(Rational lhs, Rational rhs) { return (lhs - rhs).Numerator < 0; }
	public static bool operator <=(Rational lhs, Rational rhs) { return (lhs - rhs).Numerator <= 0; }
	public static bool operator >(Rational lhs, Rational rhs) { return (lhs - rhs).Numerator > 0; }
	public static bool operator >=(Rational lhs, Rational rhs) { return (lhs - rhs).Numerator >= 0; }

	public static long CommonDenom(Rational[] nums) { return lcm(nums.Select(n => n.Denominator).ToArray()); }
	public long NumIfDenomIs(long denom) { return Numerator * (denom / Denominator); }

	public void Approximate(long maxDenom)
	{
		if (Denominator > maxDenom)
		{
			float scale = (float)maxDenom / Denominator;
			Denominator = (long)(Denominator * scale);
			Numerator = (long)(Numerator * scale);
		}
	}

	public override string ToString() { return "" + Numerator + "/" + Denominator; }

	public static implicit operator float(Rational r) { return (float)r.Numerator / r.Denominator; }
	public static implicit operator double(Rational r) { return (double)r.Numerator / r.Denominator; }
	public static explicit operator long(Rational r) { return r.Numerator / r.Denominator; }

	public static implicit operator Rational(int i) { return new Rational(i, 1); }

	public static bool TryParse(string s, out Rational result)
	{
		result = new Rational();

		string[] splitted = s.Split('/');
		if (splitted.Length < 2)
		{
			int i = 0;
			if (!int.TryParse(s, out i)) { return false; }

			result = i;
			return true;
		}

		int num = 0;
		if (!int.TryParse(splitted[0], out num)) return false;

		int denom = 0;
		if (!int.TryParse(splitted[1], out denom)) return false;

		result = new Rational(num, denom);
		return true;
	}
	public static Rational Parse(string s)
	{
		string[] splitted = s.Split('/');
		if (splitted.Length < 2) { return int.Parse(s); }

		int num = int.Parse(splitted[0]);
		int denom = int.Parse(splitted[1]);

		return new Rational(num, denom);
	}

	public int CompareTo(Rational other)
	{
		Rational d = this - other;
		return d.Numerator > 0 ? 1 : (d.Numerator < 0 ? -1 : 0);
	}
}