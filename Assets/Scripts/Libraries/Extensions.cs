﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using UniRx;
using UnityEngine;

namespace EQLIPSE
{
	public static partial class Extensions
	{
		public static string RemoveWhitespace(this string self)
		{
			return new string(self.Where(c => !char.IsWhiteSpace(c)).ToArray());
		}

		public static int MaxIndex<TSource>(this IEnumerable<TSource> self, Func<TSource, double> selector)
		{
			int i_max = 0;
			for (int i = 0; i < self.Count(); i++)
			{
				i_max = (selector(self.ElementAt(i)) > selector(self.ElementAt(i_max))) ? i : i_max;
			}
			return i_max;
		}
		public static int MinIndex<TSource>(this IEnumerable<TSource> self, Func<TSource, double> selector)
		{
			int i_min = 0;
			for (int i = 0; i < self.Count(); i++)
			{
				i_min = (selector(self.ElementAt(i)) < selector(self.ElementAt(i_min))) ? i : i_min;
			}
			return i_min;
		}

		public static TSource FindMax<TSource>(this IEnumerable<TSource> self, Func<TSource, double> selector)
		{
			if (self.Count() == 0) return default(TSource);
			return self.ElementAt(self.MaxIndex(selector));
		}
		public static TSource FindMin<TSource>(this IEnumerable<TSource> self, Func<TSource, double> selector)
		{
			if (self.Count() == 0) return default(TSource);
			return self.ElementAt(self.MinIndex(selector));
		}

		public static int NearestIndex<TSource>(this List<TSource> self, double target, Func<TSource, double> selector)
		{
			return self.MinIndex(c => Math.Abs(selector(c) - target));
		}
		public static TSource Nearest<TSource>(this IEnumerable<TSource> self, double target, Func<TSource, double> selector)
		{
			if (self.Count() == 0) return default(TSource);
			return self.ElementAt(self.MinIndex(c => Math.Abs(selector(c) - target)));
		}
		public static int NearestLessIndex<TSource>(this List<TSource> self, double target, Func<TSource, double> selector)
		{
			int i = self.MinIndex(c => Math.Abs(selector(c) - target));
			if (target < selector(self.ElementAt(i)))
			{
				return i - 1;
			}
			return i;
		}
		public static TSource NearestLess<TSource>(this IEnumerable<TSource> self, double target, Func<TSource, double> selector)
		{
			if (self.Count() == 0) return default(TSource);
			return self.Where(e => selector(e) <= target).FindMin(c => Math.Abs(selector(c) - target));
		}
		public static TSource NearestLess<TSource>(this IEnumerable<TSource> self, TSource target, Comparison<TSource> comparison = null)
		{
			if (self.Count() == 0) return default(TSource);

			if (comparison == null) comparison = Comparer<TSource>.Default.Compare;

			IEnumerable<TSource> candidates = self.Where(t => comparison(t, target) <= 0);
			if (candidates.Count() == 0) return default(TSource);

			TSource candidate = candidates.ElementAt(0);
			for (int i = 0; i < candidates.Count(); i++)
			{
				if (comparison(candidates.ElementAt(i), candidate) > 0) candidate = candidates.ElementAt(i);
			}

			return candidate;
		}
		public static TSource NearestMore<TSource>(this IEnumerable<TSource> self, TSource target, Comparison<TSource> comparison = null)
		{
			if (self.Count() == 0) return default(TSource);

			if (comparison == null) comparison = Comparer<TSource>.Default.Compare;

			IEnumerable<TSource> candidates = self.Where(t => comparison(t, target) >= 0);
			if (candidates.Count() == 0) return default(TSource);

			TSource candidate = candidates.ElementAt(0);
			for (int i = 0; i < candidates.Count(); i++)
			{
				if (comparison(candidates.ElementAt(i), candidate) < 0) candidate = candidates.ElementAt(i);
			}

			return candidate;
		}

		public static int NearestMoreIndex<TSource>(this List<TSource> self, double target, Func<TSource, double> selector)
		{
			var t = self.Where(c => selector(c) >= target).Min(c => Math.Abs(selector(c) - target));
			return self.FindIndex(c => selector(c) - target == t);
		}

		static int?[] nearestBothSorted<TSource, TComparable>(this IEnumerable<TSource> self, TComparable origin, Func<TSource, TComparable> selector) where TComparable : IComparable<TComparable>
		{
			int n = self.Count();

			if (n == 0) return new int?[] { null, null };
			if (n == 1) return origin.CompareTo(selector(self.ElementAt(0))) < 0 ? new int?[] { null, 0 } : new int?[] { 0, null };

			int id_l = 0, id_r = n - 1;

			if (origin.CompareTo(selector(self.ElementAt(id_l))) < 0) return new int?[] { null, id_l };
			if (origin.CompareTo(selector(self.ElementAt(id_r))) > 0) return new int?[] { id_r, null };

			int id_m;

			while (id_r - id_l > 1)
			{
				if (origin.CompareTo(selector(self.ElementAt(id_l))) == 0)
					return new int?[] { id_l, id_l };
				if (origin.CompareTo(selector(self.ElementAt(id_r))) == 0)
					return new int?[] { id_r, id_r };

				id_m = (id_r + id_l) / 2;
				TSource m = self.ElementAt(id_m);

				if (origin.CompareTo(selector(m)) > 0)
				{
					id_l = id_m;
				}
				else if (origin.CompareTo(selector(m)) < 0)
				{
					id_r = id_m;
				}
				else
				{
					return new int?[] { id_m, id_m };
				}
			}

			return new int?[] { id_l, id_r };
		}

		public static TSource NearestLessSorted<TSource, TComparable>(this IEnumerable<TSource> self, TComparable origin, Func<TSource, TComparable> selector, out bool found) where TComparable : IComparable<TComparable>
		{
			int? i = self.nearestBothSorted(origin, selector)[0];
			if (i == null)
			{
				found = false;
				return default(TSource);
			}
			found = true;
			return self.ElementAt((int)i);
		}


		public static int[] NearestIndices<TKey, TValue>(this SortedList<TKey, TValue> self, TKey target, out bool[] founds)
		{
			if (self.Count == 0)
			{
				founds = new bool[2] { false, false };
				return new int[2] { 0, 0 };
			}

			if (self.ContainsKey(target))
			{
				founds = new bool[2] { true, true };
				return new int[2] { self.IndexOfKey(target), self.IndexOfKey(target) };
			}

			int pvtL = 0, pvtR = self.Count - 1;

			if (self.Comparer.Compare(target, self.Keys[pvtL]) < 0)
			{
				founds = new bool[2] { false, true };
				return new int[2] { 0, pvtL };
			}
			if (self.Comparer.Compare(self.Keys[pvtR], target) < 0)
			{
				founds = new bool[2] { true, false };
				return new int[2] { pvtR, 0 };
			}

			founds = new bool[2] { true, true };

			while (pvtR - pvtL > 1)
			{
				int pvtM = (pvtL + pvtR) / 2;
				int comp = self.Comparer.Compare(self.Keys[pvtM], target);
				if (comp < 0) pvtL = pvtM;
				else pvtR = pvtM;
			}

			return new int[2] { pvtL, pvtR };
		}
		public static TKey[] NearestKeys<TKey, TValue>(this SortedList<TKey, TValue> self, TKey target, out bool[] founds)
		{
			var inds = self.NearestIndices(target, out founds);
			return new TKey[2] {
				self.Count > 0 ? self.Keys[inds[0]] : default(TKey),
				self.Count > 0 ? self.Keys[inds[1]] : default(TKey)
			};
		}
		public static TValue[] NearestValues<TKey, TValue>(this SortedList<TKey, TValue> self, TKey target, out bool[] founds)
		{
			var inds = self.NearestIndices(target, out founds);
			return new TValue[2] {
				self.Count > 0 ? self.Values[inds[0]] : default(TValue),
				self.Count > 0 ? self.Values[inds[1]] : default(TValue)
			};
		}

		public static double Variance(this IEnumerable<float> self)
		{
			double vari = 0;
			double avg = self.Average();

			for (int i = 0; i < self.Count(); i++)
			{
				double d = (self.ElementAt(i) - avg);
				vari += d * d;
			}
			vari /= self.Count();

			return vari;
		}
		public static double CoVariance(this IEnumerable<float> self, IEnumerable<float> counterpart)
		{
			double pAvg = 0f;
			for (int i = 0; i < self.Count(); i++)
			{
				pAvg += self.ElementAt(i) * counterpart.ElementAt(i);
			}
			pAvg /= self.Count();

			return pAvg - self.Average() * counterpart.Average();
		}

		public static IObservable<T2> Using<T, T2>(this IObservable<T> self, Func<T, IDisposable> resourceFactory, Func<T, IDisposable, IObservable<T2>> observableFactory)
		{
			var obsTuple = self.Select(t => new Tuple<T, IDisposable>(t, resourceFactory(t))).Publish().RefCount();
			var obsT2 = obsTuple.SelectMany(tuple => observableFactory(tuple.Item1, tuple.Item2));

			return obsT2.Zip(obsTuple, (t2, tuple) => new Tuple<T2, IDisposable>(t2, tuple.Item2))
				.Select(tuple =>
				{
					tuple.Item2.Dispose();
					return tuple.Item1;
				});
		}

	}

	public static class ObservableEx
	{
		public static IObservable<T> Using<T>(IDisposable resource, IObservable<T> completionObservable)
		{
			return Using(resource, r => completionObservable);
		}
		public static IObservable<T> Using<T, TDisposable>(TDisposable resource, Func<TDisposable, IObservable<T>> completionObservableBuilder) where TDisposable : IDisposable
		{
			return completionObservableBuilder(resource)
				.Single()
				.Do((_) => resource.Dispose())
				.DoOnError((e) => resource.Dispose());
		}
	}
}