﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using EQLIPSE;

public class Difficulty
{
	public enum Gauges
	{
		Normal, Hard, Challenge1, Challenge2, Challenge3, Challenge4, Challenge5
	}

	public static Dictionary<Gauges, Dictionary<Judges, UniRx.Tuple<float, bool>>> GaugeDictionaries =
		new Dictionary<Gauges, Dictionary<Judges, UniRx.Tuple<float, bool>>>
		{
			{
				Gauges.Normal, new Dictionary<Judges, UniRx.Tuple<float, bool>>
				{
					{Judges.Perfect, new UniRx.Tuple<float, bool>(3.34f, false)}, // TTL 334%
					{Judges.Great, new UniRx.Tuple<float, bool>(1.67f, false)}, // TTL 167%
					{Judges.Good, new UniRx.Tuple<float, bool>(0f, false)}, // TTL 0%
					{Judges.Miss, new UniRx.Tuple<float, bool>(-0.035f, true)}, // ABS -3.5%
					{Judges.SlidePerfect, new UniRx.Tuple<float, bool>(2f, false)}, // TTL 200%
					{Judges.SlideGreat, new UniRx.Tuple<float, bool>(1f, false)}, // TTL 100%
					{Judges.SlideGood, new UniRx.Tuple<float, bool>(0f, false)}, // TTL 0%
					{Judges.SlideMiss, new UniRx.Tuple<float, bool>(-0.02f, true)} // ABS -2%
				}
			},
			{
				Gauges.Hard, new Dictionary<Judges, UniRx.Tuple<float, bool>>
				{
					{Judges.Perfect, new UniRx.Tuple<float, bool>(0.002f, true)}, // ABS 0.2%
					{Judges.Great, new UniRx.Tuple<float, bool>(0.001f, true)}, // ABS 0.1%
					{Judges.Good, new UniRx.Tuple<float, bool>(0f, false)}, // TTL 0%
					{Judges.Miss, new UniRx.Tuple<float, bool>(-0.05f, true)}, // ABS -5%
					{Judges.SlidePerfect, new UniRx.Tuple<float, bool>(0.001f, true)}, // ABS 0.1%
					{Judges.SlideGreat, new UniRx.Tuple<float, bool>(0.0005f, true)}, // ABS 0.05%
					{Judges.SlideGood, new UniRx.Tuple<float, bool>(0f, false)}, // TTL 0%
					{Judges.SlideMiss, new UniRx.Tuple<float, bool>(-0.05f, true)} // ABS -5%
				}
			},
			{
				Gauges.Challenge1, new Dictionary<Judges, UniRx.Tuple<float, bool>>
				{
					{Judges.Perfect, new UniRx.Tuple<float, bool>(2f, false)}, // TTL 200%
					{Judges.Great, new UniRx.Tuple<float, bool>(1f, false)}, // TTL 100%
					{Judges.Good, new UniRx.Tuple<float, bool>(0f, false)}, // TTL 0%
					{Judges.Miss, new UniRx.Tuple<float, bool>(-0.03f, true)}, // ABS -3%
					{Judges.SlidePerfect, new UniRx.Tuple<float, bool>(0.6f, false)}, // TTL 60%
					{Judges.SlideGreat, new UniRx.Tuple<float, bool>(0.3f, false)}, // TTL 30%
					{Judges.SlideGood, new UniRx.Tuple<float, bool>(0f, false)}, // TTL 0%
					{Judges.SlideMiss, new UniRx.Tuple<float, bool>(-0.01f, true)} // ABS -1%
				}
			},
			{
				Gauges.Challenge2, new Dictionary<Judges, UniRx.Tuple<float, bool>>
				{
					{Judges.Perfect, new UniRx.Tuple<float, bool>(0.008f, true)}, // ABS 0.8%
					{Judges.Great, new UniRx.Tuple<float, bool>(0.005f, true)}, // ABS 0.5%
					{Judges.Good, new UniRx.Tuple<float, bool>(0f, false)}, // TTL 0%
					{Judges.Miss, new UniRx.Tuple<float, bool>(-0.03f, true)}, // ABS -3%
					{Judges.SlidePerfect, new UniRx.Tuple<float, bool>(0.001f, true)}, // ABS 0.1%
					{Judges.SlideGreat, new UniRx.Tuple<float, bool>(0.0005f, true)}, // ABS 0.05%
					{Judges.SlideGood, new UniRx.Tuple<float, bool>(0f, false)}, // TTL 0%
					{Judges.SlideMiss, new UniRx.Tuple<float, bool>(-0.03f, true)} // ABS -3%
				}
			},
			{
				Gauges.Challenge3, new Dictionary<Judges, UniRx.Tuple<float, bool>>
				{
					{Judges.Perfect, new UniRx.Tuple<float, bool>(0.005f, true)}, // ABS 0.5%
					{Judges.Great, new UniRx.Tuple<float, bool>(0.002f, true)}, // ABS 0.2%
					{Judges.Good, new UniRx.Tuple<float, bool>(0f, false)}, // TTL 0%
					{Judges.Miss, new UniRx.Tuple<float, bool>(-0.05f, true)}, // ABS -5%
					{Judges.SlidePerfect, new UniRx.Tuple<float, bool>(0.001f, true)}, // ABS 0.1%
					{Judges.SlideGreat, new UniRx.Tuple<float, bool>(0.0005f, true)}, // ABS 0.05%
					{Judges.SlideGood, new UniRx.Tuple<float, bool>(0f, false)}, // TTL 0%
					{Judges.SlideMiss, new UniRx.Tuple<float, bool>(-0.05f, true)} // ABS -5%
				}
			},
			{
				Gauges.Challenge4, new Dictionary<Judges, UniRx.Tuple<float, bool>>
				{
					{Judges.Perfect, new UniRx.Tuple<float, bool>(0.003f, true)}, // ABS 0.3%
					{Judges.Great, new UniRx.Tuple<float, bool>(0.002f, true)}, // ABS 0.2%
					{Judges.Good, new UniRx.Tuple<float, bool>(0f, false)}, // TTL 0%
					{Judges.Miss, new UniRx.Tuple<float, bool>(-0.07f, true)}, // ABS -7%
					{Judges.SlidePerfect, new UniRx.Tuple<float, bool>(0.001f, true)}, // ABS 0.1%
					{Judges.SlideGreat, new UniRx.Tuple<float, bool>(0.0005f, true)}, // ABS 0.05%
					{Judges.SlideGood, new UniRx.Tuple<float, bool>(0f, false)}, // TTL 0%
					{Judges.SlideMiss, new UniRx.Tuple<float, bool>(-0.07f, true)} // ABS -7%
				}
			},
			{
				Gauges.Challenge5, new Dictionary<Judges, UniRx.Tuple<float, bool>>
				{
					{Judges.Perfect, new UniRx.Tuple<float, bool>(0.001f, true)}, // ABS 0.1%
					{Judges.Great, new UniRx.Tuple<float, bool>(0.0005f, true)}, // ABS 0.05%
					{Judges.Good, new UniRx.Tuple<float, bool>(0f, false)}, // TTL 0%
					{Judges.Miss, new UniRx.Tuple<float, bool>(-0.05f, true)}, // ABS -5%
					{Judges.SlidePerfect, new UniRx.Tuple<float, bool>(0.0005f, true)}, // ABS 0.05%
					{Judges.SlideGreat, new UniRx.Tuple<float, bool>(0.00025f, true)}, // ABS 0.025%
					{Judges.SlideGood, new UniRx.Tuple<float, bool>(0f, false)}, // TTL 0%
					{Judges.SlideMiss, new UniRx.Tuple<float, bool>(-0.05f, true)} // ABS -5%
				}
			}
		};

	public Dictionary<Judges, float> TotalDictionary, ScoreDictionary, TimeJudgeDictionary, SliderJudgeDictionary;

	public static Difficulty Normal = new Difficulty()
	{
		TotalDictionary = new Dictionary<Judges, float>
		{
			{Judges.Perfect,3.34f},  //334%
			{Judges.Great,1.67f},   //167%
			{Judges.Good,0f},       //0%
			{Judges.Miss,-0.025f},
			{Judges.SlidePerfect,2f}, //200%
			{Judges.SlideGreat,1f},   //100%
			{Judges.SlideGood,0f},      //0%
			{Judges.SlideMiss,-0.01f},
		},
		ScoreDictionary = new Dictionary<Judges, float>
		{
			{Judges.Perfect,1f},
			{Judges.Great,0.7f},
			{Judges.Good,0.4f},
			{Judges.Miss,0f},
			{Judges.SlidePerfect,1f},
			{Judges.SlideGreat,0.8f},
			{Judges.SlideGood,0.6f},
			{Judges.SlideMiss,0f},
		},
		TimeJudgeDictionary = new Dictionary<Judges, float>
		{
			{Judges.Perfect,0.05f},
			{Judges.Great,0.1f},
			{Judges.Good,0.15f}
		},
		SliderJudgeDictionary = new Dictionary<Judges, float>
		{
			{Judges.Perfect,0.15f},
			{Judges.Great,0.3f},
			{Judges.Good,0.5f}
		}
	};
}