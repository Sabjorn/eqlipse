﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EQLIPSE
{
	public static class Scoring
	{
		static Dictionary<Judges, double> rawDictionary = new Dictionary<Judges, double>
		{
			{Judges.Perfect,1.0 },
			{Judges.Great,0.7 },
			{Judges.Good,0.4 },
			{Judges.Miss,0 },
			{Judges.None,0 },
			{Judges.SlidePerfect,1 },
			{Judges.SlideGreat,0.7 },
			{Judges.SlideGood,0.4 },
			{Judges.SlideMiss,0 }
		};

		public static int ScoreFromCounter(Dictionary<Judges, int> counter, int theoreticalMaxCombo)
		{
			double rawScore = counter.Select(entity => rawDictionary[entity.Key] * entity.Value).Sum();

			theoreticalMaxCombo = theoreticalMaxCombo != 0 ? theoreticalMaxCombo : 1;

			return (int)Math.Ceiling(1000000 * rawScore / theoreticalMaxCombo);
		}

		public static Ranks RankFromScore(int score)
		{
			if (score >= 995000)
				return Ranks.Spp;
			if (score >= 990000)
				return Ranks.Sp;
			if (score >= 980000)
				return Ranks.S;
			if (score >= 950000)
				return Ranks.Ap;
			if (score >= 900000)
				return Ranks.A;
			if (score >= 850000)
				return Ranks.Bp;
			if (score >= 800000)
				return Ranks.B;
			if (score >= 700000)
				return Ranks.C;
			if (score >= 600000)
				return Ranks.D;
			return Ranks.F;
		}

		public static string RankName(this Ranks rank)
		{
			switch (rank)
			{
				case Ranks.Spp:
					return "S++";
				case Ranks.Sp:
					return "S+";
				case Ranks.S:
					return "S";
				case Ranks.Ap:
					return "A+";
				case Ranks.A:
					return "A";
				case Ranks.Bp:
					return "B+";
				case Ranks.B:
					return "B";
				case Ranks.C:
					return "C";
				case Ranks.D:
					return "D";
				case Ranks.F:
					return "F";
				default:
					return "None";
			}
		}
	}

	public enum Ranks
	{
		Spp, Sp, S, Ap, A, Bp, B, C, D, F
	}
}