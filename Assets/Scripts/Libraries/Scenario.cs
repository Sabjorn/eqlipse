﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using System.Text.RegularExpressions;

public class Scenario
{
	private static Regex regexp = new Regex(@"\t");

	public string Title = "New Scenario", Description = "Some Description";
    public Seriph[] Seriphs = new Seriph[] { };
    public int Length { get { return Seriphs.Length; } }

    public static Scenario Parse(string str)
    {
        Scenario retScenario = new Scenario();

        List<Seriph> seriphList = new List<Seriph>();
        using (StringReader sr = new StringReader(str))
        {
            while (sr.Peek() != -1)
            {
                string line = sr.ReadLine();
				if (line.Substring(0, 2) == "//")
				{
					continue;
				}
				regexp.Replace(line, "");

                string[] parameters = line.Split(',');

                if (parameters[0] == "HEADER")
                {
                    for (int i = 1; i < 2 && i < parameters.Length; i++)
                    {
                        string param = parameters[i];
                        switch (i)
                        {
                            case 1:
                                retScenario.Title = param;
                                break;
                            case 2:
                                retScenario.Description = param;
                                break;
                        }
                    }
                }
                else
                {
                    Seriph seriph = new Seriph();
                    for (int i = 0; i < 13 && i < parameters.Length; i++)
                    {
                        string param = parameters[i];
                        switch (i)
                        {
                            case 0:     // セリフ主の名前
                                seriph.Name = param;
                                break;
                            case 1:     // セリフ本文
                                seriph.Body = param.Replace(';', '\n');
                                break;
                            case 2:     // 左に表示する顔
                                seriph.FaceLeft = param;
                                break;
                            case 3:     // 左の顔の表情
                                if (!string.IsNullOrEmpty(param))
                                    seriph.EmotionLeft = (Seriph.Emotions?)Enum.Parse(typeof(Seriph.Emotions), param);
                                break;
                            case 4:     // 右に表示する顔
                                seriph.FaceRight = param;
                                break;
                            case 5:     // 右の顔の表情
                                if (!string.IsNullOrEmpty(param))
                                    seriph.EmotionRight = (Seriph.Emotions?)Enum.Parse(typeof(Seriph.Emotions), param);
                                break;
                            case 6:     // フォーカスする顔選択
                                if (!string.IsNullOrEmpty(param))
                                    seriph.Position = (Seriph.Positions?)Enum.Parse(typeof(Seriph.Positions), param);
                                break;
                            case 7:     // 背景
                                seriph.Background = param;
                                break;
                            case 8:     // エフェクト
                                if (!string.IsNullOrEmpty(param))
                                    seriph.Effect = int.Parse(param);
                                break;
                            case 9:     // 曲移行時に選択する曲
                                seriph.Song = param;
                                break;
                            case 10:    // BGM
                                if (!string.IsNullOrEmpty(param))
                                    seriph.BGM = param;
                                break;
                            case 11:    // SE
                                if (!string.IsNullOrEmpty(param))
                                    seriph.SE = param;
                                break;
                            case 12:    // スリープ時間
                                if (!string.IsNullOrEmpty(param))
                                    seriph.Duration = float.Parse(param);
                                break;
                        }
                    }

                    seriphList.Add(seriph);
                }
            }
        }
        retScenario.Seriphs = seriphList.ToArray();

        return retScenario;
    }
}

public struct Seriph
{
    public enum Positions
    {
        None, Left, Right, Both
    }
    public enum Emotions
    {
        Normal, Happy, Angry, Sad
    }
    public string Name, Body, FaceLeft, FaceRight, Background, Song, BGM, SE;
    public Emotions? EmotionLeft, EmotionRight;
    public Positions? Position;
    public int? Effect;
    public float? Duration;

    public override string ToString()
    {
        return Name + "「" + Body + "」";
    }
}