﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using Newtonsoft.Json.Linq;
using UnityEngine;
using UnityEngine.Networking;

namespace EQLIPSE
{
	public class PlayData
	{
		public string Title;
		public Chart.Difficulties Difficulty;

		public float Gauge;
		public Difficulty.Gauges GaugeType;

		public bool IsClear
		{
			get
			{
				switch (GaugeType)
				{
					case global::Difficulty.Gauges.Normal:
						return Gauge >= 0.7f;
					default:
						return Gauge > 0f;
				}
			}
		}

		public bool IsFormerlyClear
		{
			get
			{
				return IsClear && (GaugeType == global::Difficulty.Gauges.Normal ||
				                   GaugeType == global::Difficulty.Gauges.Challenge5);
			}
		}

		public bool IsFullCombo
		{
			get
			{
				return JudgeCounter[Judges.Miss] == 0 && JudgeCounter[Judges.SlideMiss] == 0 && JudgeCounter[Judges.None] == 0;
			}
		}
		public Dictionary<Judges, int> JudgeCounter;
		public int MaxCombo;
		public int TheoreticalMaxCombo;

		public int Score { get { return Scoring.ScoreFromCounter(JudgeCounter, TheoreticalMaxCombo); } }
		public Ranks Rank { get { return Scoring.RankFromScore(Score); } }

		public static PlayData FromJToken(JToken token)
		{
			var data = new PlayData()
			{
				Title = (string)token["chart"]["title"],
				Difficulty = (Chart.Difficulties)(int)token["chart"]["difficulty"],
				Gauge = (bool)token["is_cleared"] ? 1 : 0,  // 再考が必要
				GaugeType = (Difficulty.Gauges)(int)token["gauge_type"],
				JudgeCounter = new Dictionary<Judges, int>()
				{
					{ Judges.Perfect, (int)token["brilliant"] },
					{ Judges.Great, (int)token["great"] },
					{ Judges.Good, (int)token["good"] },
					{ Judges.Miss, (int)token["miss"] },
					{ Judges.SlidePerfect, 0},
					{ Judges.SlideGreat, 0},
					{ Judges.SlideGood, 0},
					{ Judges.SlideMiss, 0},
					{ Judges.None, (int)token["none"]},
				},
				MaxCombo = 0,
				TheoreticalMaxCombo = (int)token["brilliant"] + (int)token["great"] + (int)token["good"] + (int)token["miss"] + (int)token["none"]
			};

			return data;
		}
	}
}