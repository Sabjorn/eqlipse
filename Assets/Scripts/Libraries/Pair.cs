﻿using System.Collections;

public struct Pair<T1, T2>
{
	public T1 Item1;
	public T2 Item2;

	public Pair(T1 item1, T2 item2)
	{
		Item1 = item1; Item2 = item2;
	}

	public override string ToString()
	{
		return "(" + Item1.ToString() + ", " + Item2.ToString() + ")";
	}
}