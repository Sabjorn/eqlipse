﻿using UnityEngine;

public abstract class SingletonMonoBehaviour<T> : MonoBehaviour where T : MonoBehaviour
{
    private static T singleton;
    public static T Singleton
    {
        get
        {
            if (singleton == null)
            {
                singleton = (T)FindObjectOfType(typeof(T));

                if (singleton == null)
                {
                    Debug.LogWarning(typeof(T) + " is null.");
                }

            }

            return singleton;
        }
    }
}