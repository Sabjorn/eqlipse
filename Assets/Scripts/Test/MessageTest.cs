﻿using System;
using System.Collections;
using System.Collections.Generic;
using EQLIPSE.Utility;
using UnityEngine;
using UniRx;

public class MessageTest : MonoBehaviour
{
	public void Message()
	{
		Observable.Interval(TimeSpan.FromSeconds(1)).Take(3)
			.SelectMany(_ => Modal.InfoDialogObservable("川の中に、石が、ある。\n拾いに、行く。" + _))
			.Subscribe();
	}
}
