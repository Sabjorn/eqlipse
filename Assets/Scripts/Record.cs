﻿using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using UniRx;
using UnityEngine;

namespace EQLIPSE
{
    public class Record
    {
        public string Title;
        public Chart.Difficulties Difficulty;
        public int Highscore;
        public Tuple<int, int> RankTuple;
        public bool IsCleared,IsFullCombo;

        public static Record JTokenToRecord(JToken jToken)
        {
            Record ret;
            try
            {
                ret = new Record()
                {
                    Title = (string)jToken["title"],
                    Difficulty =
                        (Chart.Difficulties)Enum.Parse(typeof(Chart.Difficulties), (string)jToken["difficulty"]),
                    Highscore = (int)jToken["highscore"],
                    RankTuple = new Tuple<int, int>((int)jToken["rank"][0], (int)jToken["rank"][1]),
	                IsCleared = (bool)jToken["cleared"],
	                IsFullCombo = (bool)jToken["full_combo"]
				};
            }
            catch (System.Exception)
            {
                Debug.LogWarning("プレイ記録解析失敗");
                ret = new Record();
            }

            return ret;
        }
    }
}