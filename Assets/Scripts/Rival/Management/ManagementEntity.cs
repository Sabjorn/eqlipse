﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using EQLIPSE.Option;
using EQLIPSE.Utility;
using Newtonsoft.Json.Linq;
using UnityEngine;
using UnityEngine.UI;
using UniRx;

namespace EQLIPSE.Rival
{
	public class ManagementEntity : MonoBehaviour, IOptionEntry
	{
		private CanvasGroup _canvasGroup;

		[Header("プレイヤー検索バー")]
		[SerializeField]
		private InputField NameInput;
		[SerializeField] private RectTransform PlayersParent;
		[SerializeField] private GameObject PlayerEntityPrefab;
		[SerializeField] private Text NotFoundText;
		private IDisposable PlayerFetchSubscription;

		[Header("ライバルバー")]
		[SerializeField]
		private RectTransform RivalsParent;
		[SerializeField] private GameObject RivalEntityPrefab;
		[SerializeField] private Text RivalNotFoundText;

		[Header("プレイヤー詳細")] [SerializeField] private Text NameText;
		[SerializeField] private Text RateText;
		[SerializeField] private CanvasGroup DetailCanvasGroup;
		[SerializeField] private Button AddRivalButton, RemoveRivalButton;
		private IDisposable RivalButtonSubscription;

		private List<GameObject> _playerEntities, _rivalEntities;

		void Start()
		{
			_canvasGroup = GetComponent<CanvasGroup>();

			_canvasGroup.blocksRaycasts = false;
			_canvasGroup.alpha = 0;

			_playerEntities = new List<GameObject>();
			_rivalEntities = new List<GameObject>();

			NotFoundText.color = new Color(1, 1, 1, 0);
			RivalNotFoundText.color = new Color(1, 1, 1, 0);

			DetailCanvasGroup.alpha = 0;
			DetailCanvasGroup.blocksRaycasts = false;

			NameInput.OnEndEditAsObservable()
				.Subscribe(OnRivalInput);

			RefreshRivals();
		}

		public void OnRivalInput(string input)
		{
			foreach (var go in _playerEntities)
			{
				Destroy(go);
			}
			_playerEntities.Clear();

			NotFoundText.DOFade(0f, 0.2f);

			ObservableWWW.Get(Environment.WwwApiHost + "/users/search?name=" + input)
				.Select(JToken.Parse)
				.Subscribe(RefreshList);
		}

		public void RefreshList(JToken userList)
		{
			foreach (var e in userList)
			{
				var go = Instantiate(PlayerEntityPrefab, PlayersParent);
				_playerEntities.Add(go);

				var component = go.GetComponent<PlayerEntity>();
				component.Text = e["name"].ToString();
				component.Uid = e["uid"].ToString();

				component.Button.OnClickAsObservable()
					.Subscribe(_ => OnRivalButton(component));
			}

			if (!userList.Any())
			{
				NotFoundText.DOFade(1f, 0.2f);
			}
		}

		public void OnRivalButton(PlayerEntity entity)
		{
			DetailCanvasGroup.DOFade(0, 0.2f);
			DetailCanvasGroup.blocksRaycasts = false;

			ObservableWWW.Get(Environment.WwwApiHost + "/users/view/" + entity.Uid)
				.Select(JToken.Parse)
				.Subscribe(detail =>
				{
					NameText.text = detail["name"].ToString();
					RateText.text = detail["rate"].ToString();

					DetailCanvasGroup.DOFade(1, 0.2f);
					DetailCanvasGroup.blocksRaycasts = true;

					if (RivalButtonSubscription != null) RivalButtonSubscription.Dispose();
					if (User.RivalUids.Contains(detail["uid"].ToString()))
					{
						// 既にライバルが登録済み

						/*
						RivalButtonSubscription = AddRivalButton.OnClickAsObservable()
							.SelectMany(_ =>
								ObservableEx.Using(Modal.LoadingLock(),
									ObservableWWW.Get(Environment.WwwApiHost + "/users/add_rival/" + User.Uid + "/" +
													  detail["uid"].ToString())))
							.Subscribe(response =>
							{
								RefreshRivals();
								Modal.InfoDialogObservable("ライバル登録が完了しました。").Subscribe();
							}, error =>
							{
								var message = JToken.Parse(((WWWErrorException)error).Text)["message"].ToString();
								Modal.InfoDialogObservable(message).Subscribe();
							});
						*/
					}
					else
					{
						// ライバル未登録

						if (Application.internetReachability != NetworkReachability.NotReachable)
						{
							User.RivalUids.Add(detail["uid"].ToString());

							User.EnqueueUserData();

							RivalButtonSubscription = AddRivalButton.OnClickAsObservable()
								.SelectMany(_ =>
									ObservableEx.Using(Modal.LoadingLock("登録中・・・"), WWWPostQueue.TryPostObservable()))
								.Subscribe(response =>
								{
									RefreshRivals();
									Modal.InfoDialogObservable("ライバル登録が完了しました。").Subscribe();
								}, error =>
								{
									var message = JToken.Parse(((WWWErrorException) error).Text)["message"].ToString();
									Modal.InfoDialogObservable(message).Subscribe();
								});
						}
					}
				}).AddTo(entity.gameObject);
		}

		public void RefreshRivals()
		{
			Observable.Defer(() =>
				{
					foreach (var go in _rivalEntities)
					{
						Destroy(go);
					}
					_rivalEntities.Clear();

					RivalNotFoundText.DOFade(0f, 0.2f);

					return Observable.ReturnUnit();
				})
				.SelectMany(_ => ObservableWWW.Get(Environment.WwwApiHost + "/users/view/" + User.Uid + "?rivals=true"))
				.Select(JToken.Parse)
				.Subscribe(response =>
				{
					foreach (var e in response["rivals"])
					{
						var go = Instantiate(RivalEntityPrefab, RivalsParent);
						_rivalEntities.Add(go);

						var component = go.GetComponent<PlayerEntity>();
						component.Text = e["name"].ToString();
						component.Uid = e["uid"].ToString();
					}

					if (!response["rivals"].Any())
					{
						NotFoundText.DOFade(1f, 0.2f);
					}
				});
		}

		public void Show()
		{
			_canvasGroup.blocksRaycasts = true;
			_canvasGroup.DOFade(1, 0.2f);
		}

		public void Hide()
		{
			_canvasGroup.blocksRaycasts = true;
			_canvasGroup.DOFade(0f, 0.2f);
		}
	}
}