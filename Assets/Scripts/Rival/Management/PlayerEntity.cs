﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace EQLIPSE.Rival
{
	public class PlayerEntity : MonoBehaviour
	{
		[SerializeField] private Button _Button;
		[SerializeField] private Text _Text;

		public Button Button
		{
			get { return _Button; }
			set { _Button = value; }
		}

		public string Text
		{
			get { return _Text.text; }
			set { _Text.text = value; }
		}

		public string Uid { get; set; }
	}
}